import React, { useEffect } from "react";
import { hash } from "immutable";
import { Graph } from "./Graph";
import { dfs } from "./dfs";
import { asFunctor, Functor, Invalid, Valid } from "./Functor";
import { isBlank } from "../utils";

function findEffect(effects, field) {
  const effect = effects.find(effect => effect.computedField === field);
  if (!effect) throw new Error(`Couldn't find effect for field named "${field.config.name}"`);
  return effect;
}

export function useComputedValuesEffect(form, effects) {
  const reversedGraph = makeGraph(effects).withReversedEdges();
  const sourceNodes = reversedGraph.nodes();
  sourceNodes.forEach(sourceNode => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
      if (!sourceNode.field.isFocused && !isBlank(sourceNode.field.innerField.value)) {
        dfs({
          graph: reversedGraph,
          sourceNode,

          callback(currentNode) {
            if (!currentNode.equals(sourceNode)) {
              const fieldToUpdate = currentNode.field;
              const effect = findEffect(effects, fieldToUpdate);
              const fn = effect.formula;
              const args = effect.requiredFields.map(asFunctor);
              const computed = Functor.map(fn)(...args);

              if (computed instanceof Valid) {
                fieldToUpdate.setValue(computed.value);
              } else if (computed instanceof Invalid) {
                fieldToUpdate.resetValue();
              }
            }
          }

        });
      } // eslint-disable-next-line

    }, [sourceNode.field.isFocused]);
  });
}

class FieldNode {
  constructor(field) {
    this.field = field;
  }

  equals(other) {
    if (!(other instanceof FieldNode)) return false;
    return this.field.config.name === other.field.config.name;
  }

  hashCode() {
    return hash(this.field.config.name);
  }

}

function makeGraph(effects) {
  let graph = Graph.empty();
  effects.forEach(effect => {
    const u = new FieldNode(effect.computedField);
    effect.requiredFields.forEach(requiredField => {
      const v = new FieldNode(requiredField);
      graph.addEdge(u, v);
    });
  });
  return graph;
}
//# sourceMappingURL=useComputedValuesEffect.js.map