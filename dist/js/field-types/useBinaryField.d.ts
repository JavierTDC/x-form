import { RefObject } from "react";
import { Field, FieldConfig } from "../useField";
import { InputProps } from "./useTextField";
export interface BinaryFieldConfig extends FieldConfig<boolean> {
    readonly inputProps?: Partial<InputProps>;
}
export interface BinaryField extends Field<boolean> {
    config: BinaryFieldConfig;
    inputProps: InputProps;
    inputRef: RefObject<HTMLInputElement>;
}
export declare function useBinaryField(config: BinaryFieldConfig, internal: {
    resetExtraState(): void;
}): BinaryField;
