(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.XFormContext = exports.defaultXFormConfig = undefined;
  const defaultXFormConfig = exports.defaultXFormConfig = {
    strings: {
      requiredField: "The field is required",
      optional: "Optional",
      invalidForm: "There are fields with errors",
      blankSelectField: "--- Select an option ---",
      invalidNumber: "The number is invalid",
      negativeNumber: "The number can't be negative",
      numberTooSmall: minValue => `Must be at least ${minValue}`,
      numberTooBig: maxValue => `Must be at most ${maxValue}`,
      tooManyDecimals: "Too many decimals",
      shouldBeInteger: "Must be an integer",
      cantBeZero: "Can't be zero",
      invalidDate: "Invalid date",
      tooYoung: minAge => `Must be older than ${minAge}`
    }
  };
  const XFormContext = exports.XFormContext = (0, _react.createContext)({});
});
//# sourceMappingURL=XFormContext.js.map