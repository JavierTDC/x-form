import { Blank, Result, Valid } from '../Result';
import { FieldConfig } from './FieldConfig';
import React, { Dispatch, ReactElement, RefObject, SetStateAction, useRef, useState } from 'react';
import { FormThemeContext } from '../FormTheme';
import { InputProps } from '../field-types/useTextField';
import { useXState, XState } from '../useXState';
import { Parser } from './FieldBuilder';

/** Used internally by `Form` to trigger the "on submit" event on its fields. */
export const ON_SUBMIT = Symbol('ON_SUBMIT');

export interface IField<S, T> extends StatefulField<S, T> {
  /** The `config` object that you used to build this field. */
  readonly config: FieldConfig<S, T>;

  /** Ref to container div. Used to scroll the field into view when submitting
   * with invalid values. */
  readonly ref: RefObject<HTMLDivElement>;

  /** Event handler internally triggered by the `Form` containing this field. */
  [ON_SUBMIT](): void;

  /** The result after parsing and validation. This can come in three shapes:
   * - `Valid(value)`
   * - `Invalid(message)`
   * - `Blank()` */
  result: Result<T>;

  /** True if the field has a `Valid` result, i.e.
   * the input value passes all validation rules and therefore the field has a
   * valid value that can be submitted with the form. */
  isValid: boolean;

  /** Gets the field value assuming the field has a valid value. If the field
   * is blank or invalid, this will throw. */
  unwrapValue(): T;

  /** Render this field with the current theme. */
  render(): ReactElement;

  readonly xType: string;

  // readonly inputProps: InputProps;
}

export interface IFieldLogic<S, T> {
  /** Allows to see what would happen if the field parser were used with the
   * specified input. */
  parse(input: S): Result<T>;

  /** Allows to see what would happen if the field validator were used with the
   * specified value. */
  validate(value: T): Result<T>;

  /** Allows to see what would happen if the field formatter were used with the
   * specified value. */
  format(value: T): S;
}

export abstract class FieldLogic<S, T> implements IFieldLogic<S, T> {
  protected constructor(readonly config: FieldConfig<S, T>) {}

  abstract readonly blankInput: S;

  /** Example: for a text field, a string input value consisting of just
   * whitespace is considered blank and therefore fails validation
   * if the field is required. */
  abstract isBlank(input: S): boolean;

  /** The inverse of the `parse` function. Used when setting the field input
   * from a valid value. */
  abstract defaultFormatter(value: T): S;

  abstract defaultParser(input: S): Result<T>;

  defaultValidator(value: T): Result<T> {
    return Valid(value);
  }

  parse    = this.config.parse    ?? this.defaultParser    ;
  validate = this.config.validate ?? this.defaultValidator ;
  format   = this.config.format   ?? this.defaultFormatter ;

  get initialInput(): S {
    if (this.config.defaultInput != null) {
      return this.config.defaultInput;
    }

    if (this.config.defaultValue != null) {
      return this.format(this.config.defaultValue);
    }

    return this.blankInput;
  }
}

const CACHED_RESULT = Symbol('CACHED_RESULT');

interface StatefulField<S, T> {
  /** Gets the input value of the field, a value that generally correspond to
   * the string value typed by the user in an `<input>` tag. */
  readonly input: S;

  /** Set the field input value that generally corresponds to a string typed
   * by the user in an `<input>` tag.
   *
   * For example, for a `NumberField` you can
   * do `field.setInput("42")` or even leave the field with an invalid value
   * with `field.setInput("hello")`.
   *
   * This also accepts passing a function instead of a new value, in the same
   * way as can be done on `React.useState`.
   * @see Field#setValue */
  readonly setInput: Dispatch<SetStateAction<S>>;

  /** Set the field input value to something that after parsing is equal to the
   * specified value. For example, for a `NumberField` you can do
   * `field.setValue(42)` directly.
   * @see Field#setInput */
  setValue(newValue: T): void;

  /** Resets the field to its default value (generally blank) and all extra
   * state associated to the field (e.g. whether the field has been blurred). */
  reset(): void;

  /** The initial input value of this field, generally an empty string.
   * @see Field#input */
  readonly initialInput: S;

  /** Gets the latest input value set using `setInput`, without needing to wait
   * for React to propagate the state change to the rendering components. */
  getLatestInput(): S;

  /** True if the field is part of a form that has been submitted. */
  readonly hasBeenSubmitted: boolean;

  readonly isFocused: boolean;
}

class StatefulFieldMixin<S, T> implements StatefulField<S, T> {
  constructor(readonly logic: FieldLogic<S, T>,
              readonly state: FieldState<S>) {}

  get input(): S {
    return this.state.inputState.reactiveValue;
  }

  setInput(action: SetStateAction<S>): void {
    this.state.inputState.setValue(action);
  }

  setValue(newValue: T): void {
    this.setInput(this.logic.format(newValue));
  }

  reset(): void {
    this.state.reset();
  }

  get initialInput(): S {
    return this.state.inputState.initialValue;
  }

  getLatestInput(): S {
    return this.state.inputState.getLatestValue();
  }

  get hasBeenSubmitted(): boolean {
    return this.state.hasBeenSubmitted;
  }

  get isFocused(): boolean {
    return this.state.isFocused;
  }
}

export abstract class Field<S, T> extends StatefulFieldMixin<S, T>
                                  implements IField<S, T> {
  protected constructor(readonly config: FieldConfig<S, T>,
                        readonly logic: FieldLogic<S, T>,
                        readonly state: FieldState<S>,
                        readonly ref: RefObject<HTMLDivElement>) {
    super(logic, state);
  }

  [ON_SUBMIT](): void {
    this.state.setHasBeenSubmitted(true);
  }

  private [CACHED_RESULT]: Result<T> | null = null;

  get result(): Result<T> {
    this[CACHED_RESULT] = this[CACHED_RESULT] ?? this.getResultSlow();
    return this[CACHED_RESULT]!;
  }

  private getResultSlow(): Result<T> {
    return this.logic.parse(this.input).chain(this.logic.validate);
  }

  get isValid(): boolean {
    return this.result instanceof Valid;
  }

  unwrapValue(): T {
    return this.result.unwrap();
  }

  abstract readonly xType: string;

  render(): ReactElement {
    if (this.config.render != null) {
      return this.config.render(this);
    }

    return (
        <FormThemeContext.Consumer>
          {formTheme => formTheme.render[this.xType]?.(this)}
        </FormThemeContext.Consumer>
    );
  }
}

export interface FieldState<S> {
  inputState: XState<S>;
  // extraState: XState<X>;
  isFocused: boolean;
  setIsFocused: Dispatch<SetStateAction<boolean>>;
  hasBeenBlurred: boolean;
  setHasBeenBlurred: Dispatch<SetStateAction<boolean>>;
  hasBeenSubmitted: boolean;
  setHasBeenSubmitted: Dispatch<SetStateAction<boolean>>;
  reset(): void;
}

export function useFieldState<S, X>(
    initialInput: S,
): FieldState<S> {
  const inputState = useXState(initialInput);
  // const extraState = useXState(initialExtra);
  const [isFocused, setIsFocused] = useState(false);
  const [hasBeenBlurred, setHasBeenBlurred] = useState(false);
  const [hasBeenSubmitted, setHasBeenSubmitted] = useState(false);

  function reset(): void {
    setHasBeenSubmitted(false);
    setHasBeenBlurred(false);
    inputState.resetValue();
    // extraState.resetValue();
  }

  return {
    inputState,
    // extraState,
    isFocused,
    setIsFocused,
    hasBeenBlurred,
    setHasBeenBlurred,
    hasBeenSubmitted,
    setHasBeenSubmitted,
    reset,
  };
}

/** The error message shown to the dev when a field doesn't have a render
 * function in the `FormTheme` implementation. */
function MissingThemeRender({ field }: {
  field: Field<any, any>;
}) {
  return (
      <p>
        Missing render function for field of type {field.xType} in
        FormTheme implementation
      </p>
  );
}