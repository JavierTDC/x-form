import { FieldConfig } from './FieldConfig';
import { Field, FieldLogic, FieldState, useFieldState } from './Field';
import { ChangeEvent, Dispatch, RefObject, SetStateAction, useRef, useState, FocusEvent } from 'react';
import { useXState, XState } from '../useXState';
import { isBlank } from '../utils';
import { Result, Valid } from '../Result';
import { InputProps } from '../field-types/useTextField';
import { FieldBuilder } from './FieldBuilder';

export interface TextFieldConfig extends FieldConfig<string, string> {
  readonly inputProps?: InputProps;
}

class TextFieldLogic extends FieldLogic<string, string> {
  constructor(readonly config: TextFieldConfig) {
    super(config);
  }

  readonly blankInput = "";

  isBlank(input: string): boolean {
    return isBlank(input);
  }

  defaultParser(input: string): Result<string> {
    return Valid(input);
  }

  defaultFormatter(value: string): string {
    return value;
  }
}

export class TextFieldBuilder extends FieldBuilder<string, string> {
  constructor(readonly config: TextFieldConfig) {
    super(config);
  }

  protected new(config: TextFieldConfig): TextFieldBuilder {
    return new TextFieldBuilder(config);
  }

  buildFieldLogic(): FieldLogic<string, string> {
    return new TextFieldLogic(this.config);
  }

  buildFromState(name: string, state: FieldState<string>): TextField {
    const config = this.config;
    const logic = this.buildFieldLogic();
    const ref = useRef<HTMLDivElement>(null);
    const inputRef = useRef<HTMLInputElement>(null);
    return new TextField(config, logic, state, ref, inputRef);
  }

  private getInputProps(name: string, state: FieldState<string>): InputProps {
    const config = this.config;

    function onChange(event: ChangeEvent<HTMLInputElement>): void {
      state.inputState.setValue(event.target.value);
      config.inputProps?.onChange?.(event);
    }

    function onFocus(event: FocusEvent<HTMLInputElement>): void {
      state.setIsFocused(true);
      config.inputProps?.onFocus?.(event);
    }

    function onBlur(event: FocusEvent<HTMLInputElement>): void {
      state.setIsFocused(false);
      state.setHasBeenBlurred(true);
      config.inputProps?.onBlur?.(event);
    }

    return {
      type: 'text',
      id: name,
      name,
      ...this.config.inputProps,
      value: state.inputState.reactiveValue,
      onChange,
      onFocus,
      onBlur,
    };
  }
}

export class TextField extends Field<string, string> {
  xType = "TextField";

  constructor(readonly config: TextFieldConfig,
              readonly logic: FieldLogic<string, string>,
              readonly state: FieldState<string>,
              readonly ref: RefObject<HTMLDivElement>,
              readonly inputRef: RefObject<HTMLInputElement>) {
    super(config, logic, state, ref);
  }
}