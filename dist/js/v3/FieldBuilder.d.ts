import { FieldConfig } from './FieldConfig';
import { Field, FieldLogic, FieldState } from './Field';
import { Result } from '../Result';
import { RecursivePartial } from '../XFormContext';
export declare abstract class FieldBuilder<S, T> {
    readonly config: FieldConfig<S, T>;
    protected constructor(config: FieldConfig<S, T>);
    protected abstract new(config: FieldConfig<S, T>): FieldBuilder<S, T>;
    abstract buildFromState(name: string, state: FieldState<S>): Field<S, T>;
    validateConfig<S, T>(config: FieldConfig<S, T>): void;
    with(config: RecursivePartial<FieldConfig<S, T>>): FieldBuilder<S, T>;
    abstract buildFieldLogic(): FieldLogic<S, T>;
    build(name: string): Field<S, T>;
    private defaultValidator;
}
export declare type Formatter<S, T> = (value: T) => S;
export declare type Parser<S, T> = (input: S) => Result<T>;
export declare type Validator<T> = (value: T) => Result<T>;
