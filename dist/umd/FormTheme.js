(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "./useXGlobalConfig", "auto-bind"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("./useXGlobalConfig"), require("auto-bind"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.useXGlobalConfig, global.autoBind);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _useXGlobalConfig, _autoBind) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.FormThemeContext = exports.DefaultFormTheme = undefined;

  var _react2 = _interopRequireDefault(_react);

  var _autoBind2 = _interopRequireDefault(_autoBind);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  class DefaultFormTheme {
    constructor(config) {
      (0, _autoBind2.default)(this);
      this.config = config;
      this.render = {
        Text: this.Field,
        Checkbox: this.Field,
        Number: this.Field,
        Date: this.Field,
        Select: this.Select,
        Radio: this.Radio
      };
    }

    Field(field) {
      return /*#__PURE__*/_react2.default.createElement(this.FieldContainer, {
        field: field
      }, /*#__PURE__*/_react2.default.createElement(this.Label, {
        field: field
      }), /*#__PURE__*/_react2.default.createElement(this.Input, {
        field: field
      }), /*#__PURE__*/_react2.default.createElement(this.Error, {
        field: field
      }));
    }

    Select(field) {
      return /*#__PURE__*/_react2.default.createElement(this.FieldContainer, {
        field: field
      }, /*#__PURE__*/_react2.default.createElement(this.Label, {
        field: field
      }), /*#__PURE__*/_react2.default.createElement(this.SelectContainer, {
        field: field
      }, field.options.map(option => /*#__PURE__*/_react2.default.createElement("option", _extends({
        key: option.value,
        className: this.config?.classNames?.option
      }, option.props), option.label))), /*#__PURE__*/_react2.default.createElement(this.Error, {
        field: field
      }));
    }

    Radio(field) {
      return /*#__PURE__*/_react2.default.createElement(this.FieldContainer, {
        field: field
      }, /*#__PURE__*/_react2.default.createElement(this.Label, {
        field: field
      }), /*#__PURE__*/_react2.default.createElement(this.RadioOptions, {
        field: field
      }), /*#__PURE__*/_react2.default.createElement(this.Error, {
        field: field
      }));
    }

    RadioOptions(props) {
      const field = props.field;
      return /*#__PURE__*/_react2.default.createElement(_react2.default.Fragment, null, field.options.map(option => /*#__PURE__*/_react2.default.createElement(this.RadioOptionContainer, {
        field: field,
        key: option.value
      }, /*#__PURE__*/_react2.default.createElement(this.RadioOption, {
        field: field,
        option: option
      }))));
    }

    RadioOptionContainer(props) {
      return /*#__PURE__*/_react2.default.createElement("span", {
        className: this.config?.classNames?.radioOption
      }, props.children);
    }

    RadioOption(props) {
      const {
        label,
        inputProps
      } = props.option;
      return /*#__PURE__*/_react2.default.createElement(_react2.default.Fragment, null, /*#__PURE__*/_react2.default.createElement("input", _extends({
        className: this.config?.classNames?.input
      }, inputProps)), /*#__PURE__*/_react2.default.createElement("label", {
        className: this.config?.classNames?.label,
        htmlFor: inputProps.id
      }, label));
    }

    FieldContainer(props) {
      return /*#__PURE__*/_react2.default.createElement("div", {
        className: this.config?.classNames?.field,
        ref: props.field.ref
      }, props.children);
    }

    SelectContainer(props) {
      return /*#__PURE__*/_react2.default.createElement("select", _extends({
        className: this.config?.classNames?.select
      }, props.field.inputProps), props.children);
    }

    Label(props) {
      return /*#__PURE__*/_react2.default.createElement(this.LabelContainer, {
        field: props.field
      }, props.field.config.label, props.field.config.label && props.field.config.optional && /*#__PURE__*/_react2.default.createElement(this.Optional, {
        field: props.field
      }));
    }

    LabelContainer(props) {
      return /*#__PURE__*/_react2.default.createElement("label", {
        className: this.config?.classNames?.label,
        htmlFor: props.field.inputProps?.id ?? props.field.config.name
      }, props.children);
    }

    Optional(_props) {
      const xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
      return /*#__PURE__*/_react2.default.createElement(_react2.default.Fragment, null, " (", xGlobalConfig.strings.optional, ")");
    }

    Input(props) {
      return /*#__PURE__*/_react2.default.createElement("input", _extends({
        className: this.config?.classNames?.input
      }, props.field.inputProps, {
        ref: props.field.inputRef
      }));
    }

    Error(props) {
      return /*#__PURE__*/_react2.default.createElement(_react2.default.Fragment, null, props.field.shouldShowError && /*#__PURE__*/_react2.default.createElement(this.TextError, {
        field: props.field
      }));
    }

    TextError(props) {
      return /*#__PURE__*/_react2.default.createElement("p", {
        style: {
          color: 'red'
        }
      }, props.field.validationResult);
    }

  }

  exports.DefaultFormTheme = DefaultFormTheme;
  const FormThemeContext = exports.FormThemeContext = (0, _react.createContext)(new DefaultFormTheme());
});
//# sourceMappingURL=FormTheme.js.map