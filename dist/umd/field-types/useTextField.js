(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./useHtmlInput"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./useHtmlInput"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.useHtmlInput);
    global.undefined = mod.exports;
  }
})(this, function (exports, _useHtmlInput) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useTextField = useTextField;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  // the type of the props of <input/>
  function useTextField(config) {
    return (0, _useHtmlInput.useHtmlInput)(_objectSpread(_objectSpread({}, config), {}, {
      xType: config.xType ?? "Text"
    }), {
      resetExtraState: () => {}
    });
  }
});
//# sourceMappingURL=useTextField.js.map