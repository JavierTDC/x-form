# @tdc-cl/x-form

## Installation
`yarn add @tdc-cl/x-form`

## Basic usage
```typescript jsx
type Credentials = {
  user: string;
  pass: string;
};

function MyLoginForm() {
  const form = useForm({
    fields: {
      user: useTextField({
        name: "user",
        label: "Username",
      }),
      pass: useTextField({
        name: "pass",
        label: "Password",
      })
    },

    async onSubmitWithValidValues(values: Credentials): Promise<void> {
      await fetch(".../api/login", {
        body: JSON.stringify({
          user: values.user,
          pass: values.pass,
        })
      })
    }
  });

  const { user, pass } = form.config.fields;

  return (
      <form>
        {user.render()}
        {pass.render()}
        <button onClick={form.submit}>
          Log in
        </button>
      </form>
  );
}
```