"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSelectField = useSelectField;

var _useHtmlInput = require("./useHtmlInput");

var _useXGlobalConfig = require("../useXGlobalConfig");

var _lodash = _interopRequireDefault(require("lodash"));

var _usePreviousStateEffect = require("../usePreviousStateEffect");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function useSelectField(config) {
  var _config$blankOptionLa, _config$xType;

  var xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
  var blankOptions = config.defaultValue ? [] : [{
    value: "",
    label: (_config$blankOptionLa = config.blankOptionLabel) !== null && _config$blankOptionLa !== void 0 ? _config$blankOptionLa : xGlobalConfig.strings.blankSelectField
  }];
  var field = (0, _useHtmlInput.useHtmlInput)(_objectSpread(_objectSpread({}, config), {}, {
    xType: (_config$xType = config.xType) !== null && _config$xType !== void 0 ? _config$xType : "Select"
  }), {
    resetExtraState: function resetExtraState() {}
  }); // if the selected value disappears from the options, reset the value

  var currentOptions = config.options;
  (0, _usePreviousStateEffect.usePreviousStateEffect)(function (prevOptions) {
    var value = field.getLatestValue();

    var valueIn = function valueIn(options) {
      return options.some(_lodash.default.matches({
        value: value
      }));
    };

    if (valueIn(prevOptions) && !valueIn(currentOptions)) {
      field.resetValue();
    }
  }, currentOptions);
  return Object.assign(field, {
    config: config,
    options: [].concat(blankOptions, _toConsumableArray(config.options)).map(function (option) {
      return _objectSpread(_objectSpread({}, option), {}, {
        props: {
          value: option.value
        }
      });
    })
  });
}
//# sourceMappingURL=useSelectField.js.map