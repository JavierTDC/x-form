import React, {createContext, PropsWithChildren, ReactElement} from "react";
import {Field} from "./useField";
import {useXGlobalConfig} from "./useXGlobalConfig";
import {SelectField} from "./field-types/useSelectField";
import autoBind from "auto-bind";
import { InputProps, TextField } from './field-types/useTextField';
import { RadioField } from './field-types/useRadioField';
import { EnumOption } from './field-types/useEnumField';

type RenderFunction<F extends Field<any>> = (field: F) => ReactElement;

export interface FormThemeConfig {
    readonly classNames?: {
        readonly field?: string;
        readonly label?: string;
        readonly input?: string;
        readonly select?: string;
        readonly option?: string;
        readonly radioOption?: string;
    };
}

export interface FormTheme {
    readonly config?: FormThemeConfig;
    readonly render: {
        [fieldType: string]: RenderFunction<any> | undefined;
    };
}

export type Props = PropsWithChildren<{
    readonly field: Field<any>;
}>;

export class DefaultFormTheme implements FormTheme {
    readonly config?: FormThemeConfig;
    readonly render: {
        [fieldType: string]: RenderFunction<any> | undefined;
    };

    constructor(config?: FormThemeConfig) {
        autoBind(this);
        this.config = config;
        this.render = {
            Text: this.Field,
            Checkbox: this.Field,
            Number: this.Field,
            Date: this.Field,
            Select: this.Select,
            Radio: this.Radio,
        };
    }

    Field(field: Field<any>): ReactElement {
        return (
            <this.FieldContainer field={field}>
                <this.Label field={field}/>
                <this.Input field={field}/>
                <this.Error field={field}/>
            </this.FieldContainer>
        );
    }

    Select(field: SelectField): ReactElement {
        return (
            <this.FieldContainer field={field}>
                <this.Label field={field}/>
                <this.SelectContainer field={field}>
                    {field.options.map(option => (
                        <option
                            key={option.value}
                            className={this.config?.classNames?.option}
                            {...option.props}
                        >
                            {option.label}
                        </option>
                    ))}
                </this.SelectContainer>
                <this.Error field={field}/>
            </this.FieldContainer>
        );
    }

    Radio(field: RadioField): ReactElement {
        return (
            <this.FieldContainer field={field}>
                <this.Label field={field}/>
                <this.RadioOptions field={field}/>
                <this.Error field={field}/>
            </this.FieldContainer>
        );
    }

    RadioOptions(props: Props): ReactElement {
        const field = props.field as RadioField;
        return (
            <>
                {field.options.map(option => (
                    <this.RadioOptionContainer field={field} key={option.value}>
                        <this.RadioOption field={field} option={option}/>
                    </this.RadioOptionContainer>
                ))}
            </>
        );
    }

    RadioOptionContainer(props: Props): ReactElement {
        return (
            <span className={this.config?.classNames?.radioOption}>
                {props.children}
            </span>
        );
    }

    RadioOption(props: Props & {
        option: EnumOption & {inputProps: InputProps},
    }): ReactElement {
        const { label, inputProps } = props.option;
        return (
            <>
                <input className={this.config?.classNames?.input}
                       {...inputProps}/>
                <label className={this.config?.classNames?.label}
                       htmlFor={inputProps.id}>{label}</label>
            </>
        );
    }

    FieldContainer(props: Props): ReactElement {
        return (
            <div
                className={this.config?.classNames?.field}
                ref={props.field.ref}
            >
                {props.children}
            </div>
        );
    }

    SelectContainer(props: Props): ReactElement {
        return (
            <select
                className={this.config?.classNames?.select}
                {...(props.field as any).inputProps}
            >
                {props.children}
            </select>
        );
    }

    Label(props: Props): ReactElement {
        return (
            <this.LabelContainer field={props.field}>
                {props.field.config.label}

                {props.field.config.label && props.field.config.optional &&
                <this.Optional field={props.field}/>}
            </this.LabelContainer>
        )
    }

    LabelContainer(props: Props): ReactElement {
        return (
            <label
                className={this.config?.classNames?.label}
                htmlFor={
                    (props.field as TextField).inputProps?.id
                        ?? props.field.config.name
                }
            >
                {props.children}
            </label>
        )
    }

    Optional(_props: Props): ReactElement {
        const xGlobalConfig = useXGlobalConfig();
        return <> ({xGlobalConfig.strings.optional})</>;
    }

    Input(props: Props): ReactElement {
        return (
            <input
                className={this.config?.classNames?.input}
                {...(props.field as any).inputProps}
                ref={(props.field as any).inputRef}
            />
        );
    }

    Error(props: Props): ReactElement {
        return (
            <>
                {props.field.shouldShowError &&
                <this.TextError field={props.field}/>}
            </>
        );
    }

    TextError(props: Props): ReactElement {
        return (
            <p style={{ color: 'red' }}>
                {props.field.validationResult}
            </p>
        );
    }
}

export const FormThemeContext = createContext<FormTheme>(new DefaultFormTheme());