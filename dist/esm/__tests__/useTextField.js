import { useTextField } from '../field-types/useTextField';
import { testBlankField, testInvalidField, testValidField } from '../test-utils/optional';
const name = 'test';

function validate(value) {
  if (!value.includes('@')) return 'Invalid email';
  return 'OK';
}

function validateRedundantly(value) {
  if (value.trim().length === 0) return 'Required field';
  return validate(value);
}

const invalidValue = 'not an email';
const validValue = 'test@example.com';
const hooks = {
  optional: () => useTextField({
    name,
    optional: true,
    validate: validateRedundantly
  }),
  required: () => useTextField({
    name,
    validate
  })
};
describe('useTextField', () => {
  testBlankField('optional', hooks.optional, true);
  testBlankField('required', hooks.required, false);
  testInvalidField('optional', hooks.optional, invalidValue);
  testInvalidField('required', hooks.required, invalidValue);
  testValidField('optional', hooks.optional, validValue);
  testValidField('required', hooks.required, validValue);
});
//# sourceMappingURL=useTextField.js.map