import React, {ReactElement, RefObject, useCallback, useMemo, useRef, useState} from "react";
import {FormThemeContext} from "./FormTheme";
import {XState} from "./useXState";
import { useXGlobalConfig } from './useXGlobalConfig';

export interface FieldConfig<V> {
  readonly name: string;
  readonly label?: string | ReactElement;
  readonly optional?: boolean;
  readonly disabled?: boolean;
  readonly defaultValue?: V;
  validate?(value: V): string;  // custom validate function, should return "OK" if the value is valid
  shouldValidate?(field: PartialField<V>): boolean;
  render?(field: Field<V>): ReactElement;  // custom render function

  /** Allows you to specify a custom field type.
   *
   * If your field is instantiated with `useFooField`,
   * your xType should correspond to `"Foo"`.
   *
   * Currently used for FormTheme, but you can also use it to identify the type
   * of a field for custom logic. */
  readonly xType?: string;

  /** Allows you to override the error message when a required field has not
    * been filled. */
  readonly requiredFieldMessage?: string;

  /** If true, the field won't be submitted */
  readonly ignored?: boolean;

  /** If true, the field won't be rendered. */
  readonly hidden?: boolean;

  /** If true, the field will be ignored and hidden. */
  readonly removed?: boolean;
}

function validateFieldConfig(config: FieldConfig<any>): void {
  if (config.ignored != null && config.removed != null) {
    throw new Error(
          `'ignored' and 'removed' are specified at the same time in the field`
        + ` "${config.name}". Removed fields are already ignored by definition,`
        + ` so this configuration is at best redundant and at worst wrong.`
    );
  }

  if (config.hidden != null && config.removed != null) {
    throw new Error(
        `'hidden' and 'removed' are specified at the same time in the field`
        + ` "${config.name}". Removed fields are already hidden by definition,`
        + ` so this configuration is at best redundant and at worst wrong.`
    );
  }
}

export interface PartialField<V> extends XState<V> {
  // meta
  readonly xType: string;
  readonly config: FieldConfig<V>;  // the config you passed to use****Field
  readonly ref: RefObject<HTMLDivElement>;  // ref to container div

  // state
  // ...XState<V>;  // using inheritance to simulate this
  reset(): void;

  // submit
  readonly hasBeenSubmitted: boolean;
  onSubmit(): void;  // executed by Form
}

export interface Field<V> extends PartialField<V> {
  // validator
  readonly validationResult: string;
  readonly valid: boolean;
  readonly shouldValidate: boolean;
  readonly shouldShowError: boolean;
  validate(value: V): string;

  // render
  render(): ReactElement;
}

export function useField<V>(
    config: FieldConfig<V>,
    internal: XState<V> & {
      resetExtraState(): void;

      /** Example: for a text field, a string value consisting of just
        * whitespace is considered blank and therefore fails validation
        * if the field is required. */
      isBlank(value: V): boolean;
    },
): Field<V> {
  validateFieldConfig(config);

  const xGlobalConfig = useXGlobalConfig();
  const ref = useRef<HTMLDivElement>(null);

  // submit
  const [hasBeenSubmitted, setHasBeenSubmitted] = useState(false);
  const submitter = {
    hasBeenSubmitted,
    onSubmit(): void {
      setHasBeenSubmitted(true);
    }
  };

  // reset
  // eslint-disable-next-line react-hooks/exhaustive-deps
  function reset(): void {
    setHasBeenSubmitted(false);
    internal.resetValue();
    internal.resetExtraState();
  }

  const partialField: PartialField<V> = {
    config,
    ref,
    ...internal,
    ...submitter,
    reset,
    xType: config.xType ?? "Field",
  };

  /* VALIDATOR
   * --------- */

  const requiredFieldMessage = config.requiredFieldMessage
      ?? xGlobalConfig.strings.requiredField;

  const customValidator = config.validate;

  // minimize calls to validate function
  const validate = useCallback((value: V): string => {
    const isBlank = internal.isBlank(value);

    if (config.optional && isBlank) {
      return 'OK';
    }

    if (isBlank) {
      return requiredFieldMessage;
    }

    if (customValidator) {
      return customValidator(value);
    }

    return "OK";
  }, [config, internal.isBlank]);

  const validationResult = useMemo(() => {
    return validate(internal.value);
  }, [validate, internal.value]);

  const valid = validationResult === "OK";
  const shouldValidate = config.shouldValidate?.(partialField) ?? hasBeenSubmitted;
  const shouldShowError = shouldValidate && !valid;

  const validator = {
    validationResult,
    valid,
    shouldValidate,
    shouldShowError,
    validate,
  };

  // Removed fields are hidden by definition
  const hidden = config.hidden || config.removed;

  // render
  function render(): ReactElement {
    // Hidden fields are not rendered by definition
    if (hidden) {
      return null!;
    }

    return config.render?.(field) ?? (
        <FormThemeContext.Consumer>
          {formTheme => formTheme.render[field.xType]?.(field)
              ?? <MissingThemeRender field={field}/>}
        </FormThemeContext.Consumer>
    );
  }

  // those methods depend on the field instance to already exist
  const field = Object.assign(partialField, {
    ...validator,
    render,
  });

  return field;
}

const MissingThemeRender: React.FC<{
  field: Field<any>;
}> = (props) => {
  return (
      <p>
        Missing render function for field of type {props.field.xType} in
        FormTheme implementation
      </p>
  );
}