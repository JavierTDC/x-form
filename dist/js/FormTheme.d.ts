import React, { PropsWithChildren, ReactElement } from "react";
import { Field } from "./useField";
import { SelectField } from "./field-types/useSelectField";
import { InputProps } from './field-types/useTextField';
import { RadioField } from './field-types/useRadioField';
import { EnumOption } from './field-types/useEnumField';
declare type RenderFunction<F extends Field<any>> = (field: F) => ReactElement;
export interface FormThemeConfig {
    readonly classNames?: {
        readonly field?: string;
        readonly label?: string;
        readonly input?: string;
        readonly select?: string;
        readonly option?: string;
        readonly radioOption?: string;
    };
}
export interface FormTheme {
    readonly config?: FormThemeConfig;
    readonly render: {
        [fieldType: string]: RenderFunction<any> | undefined;
    };
}
export declare type Props = PropsWithChildren<{
    readonly field: Field<any>;
}>;
export declare class DefaultFormTheme implements FormTheme {
    readonly config?: FormThemeConfig;
    readonly render: {
        [fieldType: string]: RenderFunction<any> | undefined;
    };
    constructor(config?: FormThemeConfig);
    Field(field: Field<any>): ReactElement;
    Select(field: SelectField): ReactElement;
    Radio(field: RadioField): ReactElement;
    RadioOptions(props: Props): ReactElement;
    RadioOptionContainer(props: Props): ReactElement;
    RadioOption(props: Props & {
        option: EnumOption & {
            inputProps: InputProps;
        };
    }): ReactElement;
    FieldContainer(props: Props): ReactElement;
    SelectContainer(props: Props): ReactElement;
    Label(props: Props): ReactElement;
    LabelContainer(props: Props): ReactElement;
    Optional(_props: Props): ReactElement;
    Input(props: Props): ReactElement;
    Error(props: Props): ReactElement;
    TextError(props: Props): ReactElement;
}
export declare const FormThemeContext: React.Context<FormTheme>;
export {};
