import { Form } from "../useForm";
import { NumberField } from "../field-types/useNumberField";
declare type Effect = {
    computedField: NumberField;
    formula(...args: number[]): number;
    requiredFields: NumberField[];
};
export declare function useComputedValuesEffect(form: Form<any>, effects: Effect[]): void;
export {};
