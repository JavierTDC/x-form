(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "./Rut"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("./Rut"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.Rut);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _Rut) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.RutWithOnChange = exports.RutWithDottedFormat = exports.RutWithInitialState = exports.RutWithOnValid = undefined;

  var React = _interopRequireWildcard(_react);

  function _getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();

    _getRequireWildcardCache = function () {
      return cache;
    };

    return cache;
  }

  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    }

    if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
      return {
        default: obj
      };
    }

    var cache = _getRequireWildcardCache();

    if (cache && cache.has(obj)) {
      return cache.get(obj);
    }

    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

        if (desc && (desc.get || desc.set)) {
          Object.defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }

    newObj.default = obj;

    if (cache) {
      cache.set(obj, newObj);
    }

    return newObj;
  }

  exports.default = {
    title: 'Rut',
    component: _Rut.Rut
  };

  const Template = args => /*#__PURE__*/React.createElement(_Rut.Rut, args);

  const RutWithOnValid = exports.RutWithOnValid = Template.bind({});
  RutWithOnValid.args = {
    onValid: rut => alert('This RUT is valid: ' + rut)
  };
  const RutWithInitialState = exports.RutWithInitialState = Template.bind({});
  RutWithInitialState.args = {
    rut: '12.312.312-3'
  };
  const RutWithDottedFormat = exports.RutWithDottedFormat = Template.bind({});
  RutWithDottedFormat.args = {
    format: 'dashed'
  };
  const RutWithOnChange = exports.RutWithOnChange = Template.bind({});
  RutWithOnChange.args = {
    onRutChange: (rut, isValid) => {
      console.log('rut', rut, isValid);
    }
  };
});
//# sourceMappingURL=Rut.stories.js.map