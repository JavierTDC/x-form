function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { Valid } from '../Result';
import React, { useState } from 'react';
import { FormThemeContext } from '../FormTheme';
import { useXState } from '../useXState';

/** Used internally by `Form` to trigger the "on submit" event on its fields. */
export const ON_SUBMIT = Symbol('ON_SUBMIT');
export class FieldLogic {
  constructor(config) {
    _defineProperty(this, "parse", this.config.parse ?? this.defaultParser);

    _defineProperty(this, "validate", this.config.validate ?? this.defaultValidator);

    _defineProperty(this, "format", this.config.format ?? this.defaultFormatter);

    this.config = config;
  }

  defaultValidator(value) {
    return Valid(value);
  }

  get initialInput() {
    if (this.config.defaultInput != null) {
      return this.config.defaultInput;
    }

    if (this.config.defaultValue != null) {
      return this.format(this.config.defaultValue);
    }

    return this.blankInput;
  }

}
const CACHED_RESULT = Symbol('CACHED_RESULT');

class StatefulFieldMixin {
  constructor(logic, state) {
    this.logic = logic;
    this.state = state;
  }

  get input() {
    return this.state.inputState.reactiveValue;
  }

  setInput(action) {
    this.state.inputState.setValue(action);
  }

  setValue(newValue) {
    this.setInput(this.logic.format(newValue));
  }

  reset() {
    this.state.reset();
  }

  get initialInput() {
    return this.state.inputState.initialValue;
  }

  getLatestInput() {
    return this.state.inputState.getLatestValue();
  }

  get hasBeenSubmitted() {
    return this.state.hasBeenSubmitted;
  }

  get isFocused() {
    return this.state.isFocused;
  }

}

export class Field extends StatefulFieldMixin {
  constructor(config, logic, state, ref) {
    super(logic, state);

    _defineProperty(this, CACHED_RESULT, null);

    this.config = config;
    this.logic = logic;
    this.state = state;
    this.ref = ref;
  }

  [ON_SUBMIT]() {
    this.state.setHasBeenSubmitted(true);
  }

  get result() {
    this[CACHED_RESULT] = this[CACHED_RESULT] ?? this.getResultSlow();
    return this[CACHED_RESULT];
  }

  getResultSlow() {
    return this.logic.parse(this.input).chain(this.logic.validate);
  }

  get isValid() {
    return this.result instanceof Valid;
  }

  unwrapValue() {
    return this.result.unwrap();
  }

  render() {
    if (this.config.render != null) {
      return this.config.render(this);
    }

    return /*#__PURE__*/React.createElement(FormThemeContext.Consumer, null, formTheme => formTheme.render[this.xType]?.(this));
  }

}
export function useFieldState(initialInput) {
  const inputState = useXState(initialInput); // const extraState = useXState(initialExtra);

  const [isFocused, setIsFocused] = useState(false);
  const [hasBeenBlurred, setHasBeenBlurred] = useState(false);
  const [hasBeenSubmitted, setHasBeenSubmitted] = useState(false);

  function reset() {
    setHasBeenSubmitted(false);
    setHasBeenBlurred(false);
    inputState.resetValue(); // extraState.resetValue();
  }

  return {
    inputState,
    // extraState,
    isFocused,
    setIsFocused,
    hasBeenBlurred,
    setHasBeenBlurred,
    hasBeenSubmitted,
    setHasBeenSubmitted,
    reset
  };
}
/** The error message shown to the dev when a field doesn't have a render
 * function in the `FormTheme` implementation. */

function MissingThemeRender({
  field
}) {
  return /*#__PURE__*/React.createElement("p", null, "Missing render function for field of type ", field.xType, " in FormTheme implementation");
}
//# sourceMappingURL=Field.js.map