import { BinaryField, BinaryFieldConfig } from "./useBinaryField";
export interface CheckboxFieldConfig extends BinaryFieldConfig {
}
export interface CheckboxField extends BinaryField {
    config: CheckboxFieldConfig;
}
export declare function useCheckboxField(config: CheckboxFieldConfig): CheckboxField;
