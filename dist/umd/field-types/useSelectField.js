(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./useHtmlInput", "../useXGlobalConfig", "lodash", "../usePreviousStateEffect"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./useHtmlInput"), require("../useXGlobalConfig"), require("lodash"), require("../usePreviousStateEffect"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.useHtmlInput, global.useXGlobalConfig, global.lodash, global.usePreviousStateEffect);
    global.undefined = mod.exports;
  }
})(this, function (exports, _useHtmlInput, _useXGlobalConfig, _lodash, _usePreviousStateEffect) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useSelectField = useSelectField;

  var _lodash2 = _interopRequireDefault(_lodash);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  // the type of the props of <option/>
  function useSelectField(config) {
    const xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
    const blankOptions = config.defaultValue ? [] : [{
      value: "",
      label: config.blankOptionLabel ?? xGlobalConfig.strings.blankSelectField
    }];
    let field = (0, _useHtmlInput.useHtmlInput)(_objectSpread(_objectSpread({}, config), {}, {
      xType: config.xType ?? "Select"
    }), {
      resetExtraState: () => {}
    }); // if the selected value disappears from the options, reset the value

    const currentOptions = config.options;
    (0, _usePreviousStateEffect.usePreviousStateEffect)(prevOptions => {
      const value = field.getLatestValue();

      const valueIn = options => options.some(_lodash2.default.matches({
        value
      }));

      if (valueIn(prevOptions) && !valueIn(currentOptions)) {
        field.resetValue();
      }
    }, currentOptions);
    return Object.assign(field, {
      config,
      options: [...blankOptions, ...config.options].map(option => _objectSpread(_objectSpread({}, option), {}, {
        props: {
          value: option.value
        }
      }))
    });
  }
});
//# sourceMappingURL=useSelectField.js.map