import { DetailedHTMLProps, FormHTMLAttributes, ReactElement } from 'react';
import { Field } from "./useField";
export declare type FormValues = {
    readonly [fieldName: string]: any;
};
export declare type FormFields<T extends FormValues> = {
    [P in keyof T]: Field<T[P]>;
};
export declare type FormProps = DetailedHTMLProps<FormHTMLAttributes<HTMLFormElement>, HTMLFormElement>;
export interface FormConfig<T extends FormValues> {
    readonly fields: FormFields<T>;
    /** The props to be passed to the <form> element. */
    readonly props?: FormProps;
    onSubmitWithValidValues(values: T): Promise<void>;
    validate?(values: T): string;
    render?(fields: FormFields<T>): ReactElement;
}
export interface Form<T> {
    config: FormConfig<T>;
    /** A dictionary with each of the field objects corresponding to this form.
     *
     * Those field objects then allow you to do useful operations on each field.
     *
     * For example, if your form has a "firstName" field, then you can do
     * `form.fields.firstName.render()`, `form.fields.firstName.value`, etc.
     * */
    fields: FormFields<T>;
    /** The props to be passed to the <form> element. */
    props: FormProps;
    values: T;
    setValues(values: T): void;
    isValid: boolean;
    isSubmitting: boolean;
    submit(): Promise<void>;
    reset(): void;
    render(): ReactElement;
}
export declare function useForm<T extends FormValues>(config: FormConfig<T>): Form<T>;
