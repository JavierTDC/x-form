"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dfs = dfs;

var _immutable = require("immutable");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DfsState = /*#__PURE__*/function () {
  function DfsState(colors) {
    _classCallCheck(this, DfsState);

    this.colors = colors;
  }

  _createClass(DfsState, [{
    key: "set",
    value: function set(u, color) {
      this.colors = this.colors.set(u, color);
    }
  }, {
    key: "get",
    value: function get(u) {
      return this.colors.get(u, "unvisited");
    }
  }], [{
    key: "empty",
    value: function empty() {
      return new DfsState((0, _immutable.Map)());
    }
  }]);

  return DfsState;
}();

function dfs(params) {
  dfsRecursion({
    graph: params.graph,
    state: DfsState.empty(),
    currentNode: params.sourceNode,
    callback: params.callback
  });
}

function dfsRecursion(params) {
  var graph = params.graph,
      state = params.state,
      currentNode = params.currentNode,
      callback = params.callback;
  state.set(currentNode, "visiting");
  callback(currentNode);
  graph.neighbors(currentNode).forEach(function (neighNode) {
    if (state.get(neighNode) === "unvisited") {
      dfsRecursion(_objectSpread(_objectSpread({}, params), {}, {
        currentNode: neighNode
      }));
    }
  });
  state.set(currentNode, "visited");
}
//# sourceMappingURL=dfs.js.map