import _pt from "prop-types";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import * as React from 'react';
import { rut } from '@tdc-cl/utils';
export function Rut(_ref) {
  let {
    onValid,
    rut: initialState,
    format = 'dotted'
  } = _ref,
      props = _objectWithoutProperties(_ref, ["onValid", "rut", "format"]);

  const maxLength = React.useRef(format === 'dotted' ? 12 : 10);
  const [text, setText] = React.useState(setInitialState);
  React.useEffect(() => {
    if (onValid && rut.validate(text)) {
      onValid(text);
    }
  }, [onValid, text]);

  function setInitialState() {
    if (initialState) {
      const valid = rut.validate(initialState);
      if (valid) return initialState;
      console.error(`Error trying to use '${initialState}' as initial state. Please provide a valid and formatted RUT.`);
    }

    return '';
  }

  function onChange(event) {
    const value = rut.format(event.target.value, format);
    setText(value);

    if (props.onRutChange) {
      props.onRutChange(value, rut.validate(value));
    }
  }

  return /*#__PURE__*/React.createElement("input", _extends({
    type: "text",
    value: text,
    onChange: onChange,
    maxLength: maxLength.current
  }, props));
}
Rut.propTypes = {
  rut: _pt.string,
  format: _pt.oneOf(['dotted', 'dashed'])
};
//# sourceMappingURL=Rut.js.map