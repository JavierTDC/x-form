"use strict";

var _useNumberField = require("../field-types/useNumberField");

var _optional = require("../test-utils/optional");

var name = 'test';
var inputProps = {
  min: 0,
  max: 10
};
var invalidNumber = {
  toFixed: function toFixed() {
    return "42o";
  }
};
var outOfRangeNumber = inputProps.max + 1;
var validNumber = inputProps.min;
var hooks = {
  optional: function optional() {
    return (0, _useNumberField.useNumberField)({
      name: name,
      optional: true,
      inputProps: inputProps
    });
  },
  required: function required() {
    return (0, _useNumberField.useNumberField)({
      name: name,
      inputProps: inputProps
    });
  }
};
describe('useNumberField', function () {
  (0, _optional.testBlankField)('optional', hooks.optional, true);
  (0, _optional.testBlankField)('required', hooks.required, false);
  (0, _optional.testInvalidField)('optional', hooks.optional, invalidNumber);
  (0, _optional.testInvalidField)('required', hooks.required, invalidNumber);
  (0, _optional.testInvalidField)('optional', hooks.optional, outOfRangeNumber);
  (0, _optional.testInvalidField)('required', hooks.required, outOfRangeNumber);
  (0, _optional.testValidField)('optional', hooks.optional, validNumber);
  (0, _optional.testValidField)('required', hooks.required, validNumber);
});
//# sourceMappingURL=useNumberField.js.map