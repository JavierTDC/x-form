"use strict";

var _common = require("../test-utils/common");

var _useForm = require("../useForm");

var _useTextField = require("../field-types/useTextField");

var _reactHooks = require("@testing-library/react-hooks");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function useRegularField() {
  return (0, _useTextField.useTextField)({
    name: 'regular',
    defaultValue: 'reg'
  });
}

function useIgnoredField() {
  return (0, _useTextField.useTextField)({
    name: 'ignored',
    ignored: true,
    defaultValue: 'ign'
  });
}

function useHiddenField() {
  return (0, _useTextField.useTextField)({
    name: 'hidden',
    hidden: true,
    defaultValue: 'hid'
  });
}

function useIgnoredAndHiddenField() {
  return (0, _useTextField.useTextField)({
    name: 'ignoredAndHidden',
    ignored: true,
    hidden: true,
    defaultValue: 'iah'
  });
}

function useRemovedField() {
  return (0, _useTextField.useTextField)({
    name: 'removed',
    removed: true,
    defaultValue: 'rem'
  });
}

function aFormWhenSubmitted(onSubmit) {
  return function useTheForm() {
    return (0, _useForm.useForm)({
      fields: {
        regular: useRegularField(),
        ignored: useIgnoredField(),
        hidden: useHiddenField(),
        ignoredAndHidden: useIgnoredAndHiddenField(),
        removed: useRemovedField()
      },
      onSubmitWithValidValues: function onSubmitWithValidValues(values) {
        return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  onSubmit(values);

                case 1:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }))();
      }
    });
  };
}

describe('a regular field', function () {
  it('is submitted', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var form;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            form = (0, _common.getter)(aFormWhenSubmitted(function (values) {
              expect(values.regular).toBe('reg');
            }));
            _context2.next = 3;
            return (0, _reactHooks.act)(form().submit);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  })));
  it('is rendered', function () {
    var field = (0, _common.getter)(useRegularField);
    expect(field().render() != null).toBe(true);
  });
});
describe('a ignored field', function () {
  it('is not submitted', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    var form;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            form = (0, _common.getter)(aFormWhenSubmitted(function (values) {
              expect('ignored' in values).toBe(false);
            }));
            _context3.next = 3;
            return (0, _reactHooks.act)(form().submit);

          case 3:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  })));
  it('is rendered', function () {
    var field = (0, _common.getter)(useIgnoredField);
    expect(field().render() != null).toBe(true);
  });
});
describe('a hidden field', function () {
  it('is submitted', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
    var form;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            form = (0, _common.getter)(aFormWhenSubmitted(function (values) {
              expect(values.hidden).toBe('hid');
            }));
            _context4.next = 3;
            return (0, _reactHooks.act)(form().submit);

          case 3:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  })));
  it('is not rendered', function () {
    var field = (0, _common.getter)(useHiddenField);
    expect(field().render()).toBeNull();
  });
});
describe('an ignored and hidden field', function () {
  it('is not submitted', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
    var form;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            form = (0, _common.getter)(aFormWhenSubmitted(function (values) {
              expect('ignoredAndHidden' in values).toBe(false);
            }));
            _context5.next = 3;
            return (0, _reactHooks.act)(form().submit);

          case 3:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  })));
  it('is not rendered', function () {
    var field = (0, _common.getter)(useIgnoredAndHiddenField);
    expect(field().render()).toBeNull();
  });
});
describe('a removed field', function () {
  it('is not submitted', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
    var form;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            form = (0, _common.getter)(aFormWhenSubmitted(function (values) {
              expect('removed' in values).toBe(false);
            }));
            _context6.next = 3;
            return (0, _reactHooks.act)(form().submit);

          case 3:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  })));
  it('is not rendered', function () {
    var field = (0, _common.getter)(useRemovedField);
    expect(field().render()).toBeNull();
  });
});
//# sourceMappingURL=ignoredAndHidden.js.map