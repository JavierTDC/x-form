(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["../test-utils/common", "../useForm", "../field-types/useTextField", "@testing-library/react-hooks"], factory);
  } else if (typeof exports !== "undefined") {
    factory(require("../test-utils/common"), require("../useForm"), require("../field-types/useTextField"), require("@testing-library/react-hooks"));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.common, global.useForm, global.useTextField, global.reactHooks);
    global.undefined = mod.exports;
  }
})(this, function (_common, _useForm, _useTextField, _reactHooks) {
  "use strict";

  function useRegularField() {
    return (0, _useTextField.useTextField)({
      name: 'regular',
      defaultValue: 'reg'
    });
  }

  function useIgnoredField() {
    return (0, _useTextField.useTextField)({
      name: 'ignored',
      ignored: true,
      defaultValue: 'ign'
    });
  }

  function useHiddenField() {
    return (0, _useTextField.useTextField)({
      name: 'hidden',
      hidden: true,
      defaultValue: 'hid'
    });
  }

  function useIgnoredAndHiddenField() {
    return (0, _useTextField.useTextField)({
      name: 'ignoredAndHidden',
      ignored: true,
      hidden: true,
      defaultValue: 'iah'
    });
  }

  function useRemovedField() {
    return (0, _useTextField.useTextField)({
      name: 'removed',
      removed: true,
      defaultValue: 'rem'
    });
  }

  function aFormWhenSubmitted(onSubmit) {
    return function useTheForm() {
      return (0, _useForm.useForm)({
        fields: {
          regular: useRegularField(),
          ignored: useIgnoredField(),
          hidden: useHiddenField(),
          ignoredAndHidden: useIgnoredAndHiddenField(),
          removed: useRemovedField()
        },

        async onSubmitWithValidValues(values) {
          onSubmit(values);
        }

      });
    };
  }

  describe('a regular field', () => {
    it('is submitted', async () => {
      const form = (0, _common.getter)(aFormWhenSubmitted(values => {
        expect(values.regular).toBe('reg');
      }));
      await (0, _reactHooks.act)(form().submit);
    });
    it('is rendered', () => {
      const field = (0, _common.getter)(useRegularField);
      expect(field().render() != null).toBe(true);
    });
  });
  describe('a ignored field', () => {
    it('is not submitted', async () => {
      const form = (0, _common.getter)(aFormWhenSubmitted(values => {
        expect('ignored' in values).toBe(false);
      }));
      await (0, _reactHooks.act)(form().submit);
    });
    it('is rendered', () => {
      const field = (0, _common.getter)(useIgnoredField);
      expect(field().render() != null).toBe(true);
    });
  });
  describe('a hidden field', () => {
    it('is submitted', async () => {
      const form = (0, _common.getter)(aFormWhenSubmitted(values => {
        expect(values.hidden).toBe('hid');
      }));
      await (0, _reactHooks.act)(form().submit);
    });
    it('is not rendered', () => {
      const field = (0, _common.getter)(useHiddenField);
      expect(field().render()).toBeNull();
    });
  });
  describe('an ignored and hidden field', () => {
    it('is not submitted', async () => {
      const form = (0, _common.getter)(aFormWhenSubmitted(values => {
        expect('ignoredAndHidden' in values).toBe(false);
      }));
      await (0, _reactHooks.act)(form().submit);
    });
    it('is not rendered', () => {
      const field = (0, _common.getter)(useIgnoredAndHiddenField);
      expect(field().render()).toBeNull();
    });
  });
  describe('a removed field', () => {
    it('is not submitted', async () => {
      const form = (0, _common.getter)(aFormWhenSubmitted(values => {
        expect('removed' in values).toBe(false);
      }));
      await (0, _reactHooks.act)(form().submit);
    });
    it('is not rendered', () => {
      const field = (0, _common.getter)(useRemovedField);
      expect(field().render()).toBeNull();
    });
  });
});
//# sourceMappingURL=ignoredAndHidden.js.map