function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import _ from "lodash";
import { isBlank } from "../utils";
export class Functor {
  static map(fn) {
    return (...args) => {
      if (args.every(x => x instanceof Valid)) return new Valid(fn(..._.map(args, "value")));
      if (args.some(x => x instanceof Invalid)) return invalid;
      return blank;
    };
  }

}
export class Blank extends Functor {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "type", "Blank");
  }

}
export class Invalid extends Functor {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "type", "Invalid");
  }

}
export class Valid extends Functor {
  constructor(value) {
    super();

    _defineProperty(this, "type", "Valid");

    this.value = value;
  }

}
const blank = new Blank();
const invalid = new Invalid();
export function asFunctor(field) {
  const numberValue = field.getLatestValue();
  const stringValue = field.innerField.getLatestValue();
  const validate = field.innerField.validate;
  if (validate(stringValue) === "OK") return new Valid(numberValue);
  if (isBlank(stringValue)) return blank;
  return invalid;
}
//# sourceMappingURL=Functor.js.map