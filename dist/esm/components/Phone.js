import _pt from "prop-types";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import * as React from 'react';
import NumberFormat from 'react-number-format'; // +56 9 XXXX XXXX -> Móvil
// +56 2 XXXX XXXX -> Fijo RM
// +56 YY XXX XXXX -> Fijo Otras regiones

export function Phone(_ref) {
  let {
    format,
    withPrefix = true
  } = _ref,
      props = _objectWithoutProperties(_ref, ["format", "withPrefix"]);

  const mask = format === 'mobile' ? '#### ####' : format === 'rm' ? '#### ####' : '## ### ####';
  const prefix = format === 'mobile' ? '+56 9' : format === 'rm' ? '+56 2' : '+56';
  return /*#__PURE__*/React.createElement(NumberFormat, _extends({
    type: "text",
    format: `${prefix} ${mask}`,
    allowEmptyFormatting: true
  }, props));
}
Phone.propTypes = {
  format: _pt.oneOf(['mobile', 'rm', 'region']).isRequired,
  withPrefix: _pt.bool
};
//# sourceMappingURL=Phone.js.map