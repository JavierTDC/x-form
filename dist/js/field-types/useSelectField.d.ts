import { TextField } from './useTextField';
import { DetailedHTMLProps, OptionHTMLAttributes } from 'react';
import { EnumFieldConfig, EnumOption } from './useEnumField';
export declare type OptionProps = DetailedHTMLProps<OptionHTMLAttributes<HTMLOptionElement>, HTMLOptionElement>;
export interface SelectFieldConfig extends EnumFieldConfig {
    readonly blankOptionLabel?: string;
}
export interface SelectField extends TextField {
    readonly config: SelectFieldConfig;
    readonly options: Array<EnumOption & {
        props: OptionProps;
    }>;
}
export declare function useSelectField(config: SelectFieldConfig): SelectField;
