(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "lodash/merge", "./XFormContext"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("lodash/merge"), require("./XFormContext"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.merge, global.XFormContext);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _merge, _XFormContext) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useXGlobalConfig = useXGlobalConfig;

  var _react2 = _interopRequireDefault(_react);

  var _merge2 = _interopRequireDefault(_merge);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function useXGlobalConfig() {
    const context = _react2.default.useContext(_XFormContext.XFormContext);

    return (0, _merge2.default)({}, _XFormContext.defaultXFormConfig, context);
  }
});
//# sourceMappingURL=useXGlobalConfig.js.map