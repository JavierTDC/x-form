"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getter = getter;

var _reactHooks = require("@testing-library/react-hooks");

function getter(hook) {
  var _renderHook = (0, _reactHooks.renderHook)(hook),
      result = _renderHook.result;

  return function () {
    return result.current;
  };
}
//# sourceMappingURL=common.js.map