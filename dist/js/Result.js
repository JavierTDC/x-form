"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Blank = exports.Invalid = exports.Valid = exports.Result = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _immutable = _interopRequireDefault(require("immutable"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***
 *    ██████╗ ███████╗███████╗██╗   ██╗██╗  ████████╗
 *    ██╔══██╗██╔════╝██╔════╝██║   ██║██║  ╚══██╔══╝
 *    ██████╔╝█████╗  ███████╗██║   ██║██║     ██║
 *    ██╔══██╗██╔══╝  ╚════██║██║   ██║██║     ██║
 *    ██║  ██║███████╗███████║╚██████╔╝███████╗██║
 *    ╚═╝  ╚═╝╚══════╝╚══════╝ ╚═════╝ ╚══════╝╚═╝
 *
 */

/** The return type for `parse` and `validate` functions.
 *
 * `Result`s are chainable functors that can store the following results after
 * a field parsing and/or validation:
 * - a `Valid(value)`
 * - an `Invalid(message)` where `message` is the error message shown to the
 * user.
 * - a `Blank()` for fields that have not been filled. */
var Result = /*#__PURE__*/function () {
  function Result() {
    _classCallCheck(this, Result);
  }

  _createClass(Result, null, [{
    key: "fMap",

    /** Intuitively, it takes a function that doesn't work with `Result`s and
     * returns a new function that works with `Result` parameters and return type.
     *
     *
     * Example:
     * ```
     * const originalFn = (a, b, c) => a + b + c;
     * const newFn = Result.fMap(originalFn);
     * newFn(Valid(100), Valid(20), Valid(3))  // Valid(123)
     * newFn(Valid(100), Invalid("Oops"), Invalid("Boom"))  // Invalid("")
     * newFn(Valid(100), Invalid("Oops"), Blank())  // Invalid("")
     * newFn(Valid(100), Blank(), Blank())  // Blank()
     * ```
     *
     * Specifically, if `fn` is a function that receive N parameters of type `X`
     * and returns a value of type `Y`, `fMap` will return a new function that
     * receive N parameters of type `Result<X>` and returns a `Result<Y>`.
     *
     * Then the new function will work as follows:
     * - If all arguments are `Valid`, the result will be `Valid`.
     * - If there is any `Invalid` argument, the result will be `Invalid`.
     * - If there is any `Blank` argument (and no `Invalid`s), the result will be
     * `Blank`.
     * */
    value: function fMap(fn) {
      return function () {
        for (var _len = arguments.length, xs = new Array(_len), _key = 0; _key < _len; _key++) {
          xs[_key] = arguments[_key];
        }

        if (xs.every(function (x) {
          return x instanceof Valid;
        })) {
          var unwrappedValues = _lodash.default.map(xs, 'value');

          return Valid(fn.apply(void 0, _toConsumableArray(unwrappedValues)));
        }

        if (xs.some(function (x) {
          return x instanceof Invalid;
        })) {
          return Invalid(""); // this message will never be shown anyway
        }

        return Blank();
      };
    }
    /** Gets the value if the result is `Valid` or throws otherwise. */

  }]);

  return Result;
}();
/** The type for a function that can receive any number of arguments of type `X`
 * and returns a value of type `Y`. */


exports.Result = Result;

/***
 *    ██╗   ██╗ █████╗ ██╗     ██╗██████╗
 *    ██║   ██║██╔══██╗██║     ██║██╔══██╗
 *    ██║   ██║███████║██║     ██║██║  ██║
 *    ╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
 *     ╚████╔╝ ██║  ██║███████╗██║██████╔╝
 *      ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
 *
 */

/** @see Result */
var $Valid = /*#__PURE__*/function (_Result) {
  _inherits($Valid, _Result);

  var _super = _createSuper($Valid);

  function $Valid(value) {
    var _this;

    _classCallCheck(this, $Valid);

    _this = _super.call(this);
    _this.value = value;
    return _this;
  }

  _createClass($Valid, [{
    key: "equals",
    value: function equals(other) {
      return other instanceof Valid && _immutable.default.is(this.value, other.value);
    }
  }, {
    key: "map",
    value: function map(fn) {
      return Valid(fn(this.value));
    }
  }, {
    key: "chain",
    value: function chain(fn) {
      return fn(this.value);
    }
  }, {
    key: "match",
    value: function match(arms) {
      return arms.valid(this.value);
    }
  }, {
    key: "unwrap",
    value: function unwrap() {
      return this.value;
    }
  }]);

  return $Valid;
}(Result);
/* The following code is dark magic to make `Valid` a proxy for the `$Valid`
 * class that can also be constructed without using the `new` keyword. */

/** @see Result */


/** @see Result */
var Valid = new Proxy($Valid, {
  apply: function apply(_, __, _ref) {
    var _ref2 = _slicedToArray(_ref, 1),
        value = _ref2[0];

    return new $Valid(value);
  }
});
/***
 *    ██╗███╗   ██╗██╗   ██╗ █████╗ ██╗     ██╗██████╗
 *    ██║████╗  ██║██║   ██║██╔══██╗██║     ██║██╔══██╗
 *    ██║██╔██╗ ██║██║   ██║███████║██║     ██║██║  ██║
 *    ██║██║╚██╗██║╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
 *    ██║██║ ╚████║ ╚████╔╝ ██║  ██║███████╗██║██████╔╝
 *    ╚═╝╚═╝  ╚═══╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
 *
 */

/** @see Result */

exports.Valid = Valid;

var $Invalid = /*#__PURE__*/function (_Result2) {
  _inherits($Invalid, _Result2);

  var _super2 = _createSuper($Invalid);

  function $Invalid(message) {
    var _this2;

    _classCallCheck(this, $Invalid);

    _this2 = _super2.call(this);
    _this2.message = message;
    return _this2;
  }

  _createClass($Invalid, [{
    key: "equals",
    value: function equals(other) {
      return other instanceof Invalid && this.message === other.message;
    }
  }, {
    key: "map",
    value: function map(fn) {
      return this.cast();
    }
  }, {
    key: "chain",
    value: function chain(fn) {
      return this.cast();
    }
  }, {
    key: "match",
    value: function match(arms) {
      return arms.invalid(this.message);
    }
  }, {
    key: "unwrap",
    value: function unwrap() {
      throw new Error("Attempted to unwrap an Invalid result");
    }
    /** Casts this `Invalid<T>` instance to `Invalid<U>`. */

  }, {
    key: "cast",
    value: function cast() {
      // `Invalid` results always store strings so this is safe
      return this;
    }
  }]);

  return $Invalid;
}(Result);
/* The following code is dark magic to make `Invalid` a proxy for the `$Invalid`
 * class that can also be constructed without using the `new` keyword. */

/** @see Result */


/** @see Result */
var Invalid = new Proxy($Invalid, {
  apply: function apply(_, __, _ref3) {
    var _ref4 = _slicedToArray(_ref3, 1),
        message = _ref4[0];

    return new $Invalid(message);
  }
});
/***
 *    ██████╗ ██╗      █████╗ ███╗   ██╗██╗  ██╗
 *    ██╔══██╗██║     ██╔══██╗████╗  ██║██║ ██╔╝
 *    ██████╔╝██║     ███████║██╔██╗ ██║█████╔╝
 *    ██╔══██╗██║     ██╔══██║██║╚██╗██║██╔═██╗
 *    ██████╔╝███████╗██║  ██║██║ ╚████║██║  ██╗
 *    ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝
 *
 */

exports.Invalid = Invalid;

var $Blank = /*#__PURE__*/function (_Result3) {
  _inherits($Blank, _Result3);

  var _super3 = _createSuper($Blank);

  function $Blank() {
    _classCallCheck(this, $Blank);

    return _super3.apply(this, arguments);
  }

  _createClass($Blank, [{
    key: "equals",
    value: function equals(other) {
      return other instanceof Blank;
    }
  }, {
    key: "map",
    value: function map(fn) {
      return this.cast();
    }
  }, {
    key: "chain",
    value: function chain(fn) {
      return this.cast();
    }
  }, {
    key: "match",
    value: function match(arms) {
      return arms.blank();
    }
  }, {
    key: "unwrap",
    value: function unwrap() {
      throw new Error("Attempted to unwrap a Blank result");
    }
    /** Casts this `Blank<T>` instance to `Blank<U>`. */

  }, {
    key: "cast",
    value: function cast() {
      // `Blank` results always store strings so this is safe
      return this;
    }
  }]);

  return $Blank;
}(Result);
/** There is only one possible `Blank()` result so we can store an instance
 * for it and then always return that instance in the `Blank` constructor. */


var blankSingletonInstance = new $Blank();
/* The following code is dark magic to make `Blank` a proxy for the `$Blank`
 * class that can also be constructed without using the `new` keyword. */

/** @see Result */

/** @see Result */
var Blank = new Proxy($Blank, {
  apply: function apply() {
    return blankSingletonInstance;
  } // equivalent to new $Blank()

});
exports.Blank = Blank;
//# sourceMappingURL=Result.js.map