"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useForm = useForm;

var _react = _interopRequireWildcard(require("react"));

var _useXGlobalConfig = require("./useXGlobalConfig");

var _utils = require("./utils");

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function checkFormConfig(config) {
  if (_lodash.default.isEmpty(config.fields)) throw new Error("Form with no fields");

  _lodash.default.forEach(config.fields, function (field, key) {
    if (field.config.name !== key) {
      throw new Error("In the \"fields\" object you passed to useForm, the field at \"".concat(key, "\" ") + "has a different name \"".concat(field.config.name, "\"."));
    }
  });
}

function useForm(config) {
  checkFormConfig(config);
  var xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)(); // there can be ignored and/or hidden fields here

  var allFields = config.fields; // here is where we ignore the ignored fields

  var consideredFields = _lodash.default.omitBy(allFields, function (_ref) {
    var config = _ref.config;
    return (// removed fields are ignored by definition
      config.ignored || config.removed
    );
  }); // those will not have the hidden fields


  var shownFields = _lodash.default.omitBy(allFields, function (_ref2) {
    var config = _ref2.config;
    return (// removed fields are hidden by definition
      config.hidden || config.removed
    );
  }); // convert the field values to the FormValues dictionary representation


  var values = _lodash.default.mapValues(consideredFields, "value"); // on a long form you may want to scroll to the first invalid field on submit


  var firstInvalidField = Object.values(shownFields).find(function (field) {
    return !field.valid;
  }); // consider making useValidator able to handle this as well to avoid duplication

  function validate() {
    var _config$validate, _config$validate2;

    if (firstInvalidField != null) {
      return xGlobalConfig.strings.invalidForm;
    }

    return (_config$validate = (_config$validate2 = config.validate) === null || _config$validate2 === void 0 ? void 0 : _config$validate2.call(config, values)) !== null && _config$validate !== void 0 ? _config$validate : "OK";
  }

  var validationResult = validate();
  var isValid = validationResult === "OK";

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isSubmitting = _useState2[0],
      setIsSubmitting = _useState2[1];

  function render() {
    var _config$render, _config$render2;

    return (_config$render = (_config$render2 = config.render) === null || _config$render2 === void 0 ? void 0 : _config$render2.call(config, config.fields)) !== null && _config$render !== void 0 ? _config$render : /*#__PURE__*/_react.default.createElement("form", config.props, _lodash.default.map(config.fields, function (field, name) {
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
        key: name
      }, field.render());
    }));
  }

  return {
    config: config,
    fields: config.fields,
    props: _objectSpread(_objectSpread({}, config.props), {}, {
      onSubmit: function onSubmit(event) {
        var _config$props, _config$props$onSubmi;

        event.preventDefault();
        (_config$props = config.props) === null || _config$props === void 0 ? void 0 : (_config$props$onSubmi = _config$props.onSubmit) === null || _config$props$onSubmi === void 0 ? void 0 : _config$props$onSubmi.call(_config$props, event);
      }
    }),
    values: values,
    setValues: function setValues(values) {
      Object.entries(values).forEach(function (_ref3) {
        var _ref4 = _slicedToArray(_ref3, 2),
            name = _ref4[0],
            value = _ref4[1];

        config.fields[name].setValue(value);
      });
    },
    isValid: isValid,
    isSubmitting: isSubmitting,
    submit: function submit() {
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                setIsSubmitting(true);
                _context.prev = 1;

                _lodash.default.forEach(consideredFields, function (field) {
                  return field.onSubmit();
                });

                if (!isValid) {
                  _context.next = 8;
                  break;
                }

                _context.next = 6;
                return config.onSubmitWithValidValues(values);

              case 6:
                _context.next = 9;
                break;

              case 8:
                if (firstInvalidField != null) {
                  // in future versions the user should be choosing the behavior here
                  (0, _utils.scrollToField)(firstInvalidField);
                } else {
                  // and here
                  (0, _utils.scrollToTop)();
                }

              case 9:
                _context.prev = 9;
                setIsSubmitting(false);
                return _context.finish(9);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1,, 9, 12]]);
      }))();
    },
    reset: function reset() {
      _lodash.default.forEach(allFields, function (field) {
        return field.reset();
      });
    },
    render: render
  };
}
//# sourceMappingURL=useForm.js.map