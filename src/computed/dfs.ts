import {Map} from "immutable";
import {Graph} from "./Graph";

type Color = "unvisited" | "visiting" | "visited";
class DfsState<Node> {
  private colors: Map<Node, Color>;

  private constructor(colors: Map<Node, Color>) {
    this.colors = colors;
  }

  static empty<Node>(): DfsState<Node> {
    return new DfsState<Node>(Map());
  }

  set(u: Node, color: Color): void {
    this.colors = this.colors.set(u, color);
  }

  get(u: Node): Color {
    return this.colors.get(u, "unvisited");
  }
}

type DfsParams<Node> = {
  graph: Graph<Node>;
  sourceNode: Node;
  callback: (currentNode: Node) => void;
};

type DfsRecursionParams<Node> = {
  graph: Graph<Node>;
  state: DfsState<Node>;
  currentNode: Node;
  callback: (currentNode: Node) => void;
};

export function dfs<Node>(params: DfsParams<Node>): void {
  dfsRecursion({
    graph: params.graph,
    state: DfsState.empty<Node>(),
    currentNode: params.sourceNode,
    callback: params.callback,
  });
}

function dfsRecursion<Node>(params: DfsRecursionParams<Node>): void {
  const { graph, state, currentNode, callback } = params;

  state.set(currentNode, "visiting");
  callback(currentNode);
  graph.neighbors(currentNode).forEach(neighNode => {
    if (state.get(neighNode) === "unvisited") {
      dfsRecursion({ ...params, currentNode: neighNode });
    }
  });
  state.set(currentNode, "visited");
}