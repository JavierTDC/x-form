import {Dispatch, SetStateAction, useRef, useState} from "react";

export interface StateRef<T> {
  reactiveValue: T;
  setValue: Dispatch<SetStateAction<T>>;
  getLatestValue(): T;
}

export function useStateRef<T>(initialState: T): StateRef<T> {
  const [reactiveValue, setReactiveValue] = useState(initialState);
  const valueRef = useRef(initialState);

  return {
    reactiveValue,

    setValue(action: SetStateAction<T>): void {
      if (action instanceof Function) {
        valueRef.current = action(valueRef.current);
      } else {
        valueRef.current = action;
      }

      setReactiveValue(valueRef.current);
    },

    getLatestValue(): T {
      return valueRef.current;
    },
  };
}