function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { useState } from 'react';
import { useXGlobalConfig } from "./useXGlobalConfig";
import { scrollToField, scrollToTop } from "./utils";
import _ from "lodash";

function checkFormConfig(config) {
  if (_.isEmpty(config.fields)) throw new Error("Form with no fields");

  _.forEach(config.fields, (field, key) => {
    if (field.config.name !== key) {
      throw new Error(`In the "fields" object you passed to useForm, the field at "${key}" ` + `has a different name "${field.config.name}".`);
    }
  });
}

export function useForm(config) {
  checkFormConfig(config);
  const xGlobalConfig = useXGlobalConfig(); // there can be ignored and/or hidden fields here

  const allFields = config.fields; // here is where we ignore the ignored fields

  const consideredFields = _.omitBy(allFields, ({
    config
  }) => // removed fields are ignored by definition
  config.ignored || config.removed); // those will not have the hidden fields


  const shownFields = _.omitBy(allFields, ({
    config
  }) => // removed fields are hidden by definition
  config.hidden || config.removed); // convert the field values to the FormValues dictionary representation


  const values = _.mapValues(consideredFields, "value"); // on a long form you may want to scroll to the first invalid field on submit


  const firstInvalidField = Object.values(shownFields).find(field => !field.valid); // consider making useValidator able to handle this as well to avoid duplication

  function validate() {
    if (firstInvalidField != null) {
      return xGlobalConfig.strings.invalidForm;
    }

    return config.validate?.(values) ?? "OK";
  }

  const validationResult = validate();
  const isValid = validationResult === "OK";
  const [isSubmitting, setIsSubmitting] = useState(false);

  function render() {
    return config.render?.(config.fields) ?? /*#__PURE__*/React.createElement("form", config.props, _.map(config.fields, (field, name) => /*#__PURE__*/React.createElement(React.Fragment, {
      key: name
    }, field.render())));
  }

  return {
    config,
    fields: config.fields,
    props: _objectSpread(_objectSpread({}, config.props), {}, {
      onSubmit(event) {
        event.preventDefault();
        config.props?.onSubmit?.(event);
      }

    }),
    values,

    setValues(values) {
      Object.entries(values).forEach(([name, value]) => {
        config.fields[name].setValue(value);
      });
    },

    isValid,
    isSubmitting,

    async submit() {
      setIsSubmitting(true);

      try {
        _.forEach(consideredFields, field => field.onSubmit());

        if (isValid) {
          await config.onSubmitWithValidValues(values);
        } else if (firstInvalidField != null) {
          // in future versions the user should be choosing the behavior here
          scrollToField(firstInvalidField);
        } else {
          // and here
          scrollToTop();
        }
      } finally {
        setIsSubmitting(false);
      }
    },

    reset() {
      _.forEach(allFields, field => field.reset());
    },

    render
  };
}
//# sourceMappingURL=useForm.js.map