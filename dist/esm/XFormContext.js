import React, { createContext } from "react";
export const defaultXFormConfig = {
  strings: {
    requiredField: "The field is required",
    optional: "Optional",
    invalidForm: "There are fields with errors",
    blankSelectField: "--- Select an option ---",
    invalidNumber: "The number is invalid",
    negativeNumber: "The number can't be negative",
    numberTooSmall: minValue => `Must be at least ${minValue}`,
    numberTooBig: maxValue => `Must be at most ${maxValue}`,
    tooManyDecimals: "Too many decimals",
    shouldBeInteger: "Must be an integer",
    cantBeZero: "Can't be zero",
    invalidDate: "Invalid date",
    tooYoung: minAge => `Must be older than ${minAge}`
  }
};
export const XFormContext = /*#__PURE__*/createContext({});
//# sourceMappingURL=XFormContext.js.map