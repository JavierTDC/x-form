"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Rut = require("./Rut");

Object.keys(_Rut).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _Rut[key];
    }
  });
});

var _AmountUF = require("./AmountUF");

Object.keys(_AmountUF).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _AmountUF[key];
    }
  });
});

var _Phone = require("./Phone");

Object.keys(_Phone).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _Phone[key];
    }
  });
});
//# sourceMappingURL=index.js.map