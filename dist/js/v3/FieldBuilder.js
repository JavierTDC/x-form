"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FieldBuilder = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _Field = require("./Field");

var _Result = require("../Result");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var FieldBuilder = /*#__PURE__*/function () {
  function FieldBuilder(config) {
    _classCallCheck(this, FieldBuilder);

    this.config = config;
    this.validateConfig(config);
  }

  _createClass(FieldBuilder, [{
    key: "validateConfig",
    value: function validateConfig(config) {
      if (config.defaultInput != null && config.defaultValue != null) {
        throw new Error("defaultInput and defaultValue can't be both provided at the same time");
      }
    }
  }, {
    key: "with",
    value: function _with(config) {
      return this.new(_lodash.default.merge({}, this.config, config));
    }
  }, {
    key: "build",
    value: function build(name) {
      var logic = this.buildFieldLogic();
      var initialInput = logic.initialInput;
      var state = (0, _Field.useFieldState)(initialInput);
      return this.buildFromState(name, state);
    }
  }, {
    key: "defaultValidator",
    value: function defaultValidator(value) {
      return (0, _Result.Valid)(value);
    }
  }]);

  return FieldBuilder;
}();

exports.FieldBuilder = FieldBuilder;
//# sourceMappingURL=FieldBuilder.js.map