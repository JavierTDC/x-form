(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "js-joda", "./useHtmlInput", "../useXGlobalConfig"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("js-joda"), require("./useHtmlInput"), require("../useXGlobalConfig"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jsJoda, global.useHtmlInput, global.useXGlobalConfig);
    global.undefined = mod.exports;
  }
})(this, function (exports, _jsJoda, _useHtmlInput, _useXGlobalConfig) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useDateField = useDateField;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function useDateField(config) {
    const xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
    const innerField = (0, _useHtmlInput.useHtmlInput)(_objectSpread(_objectSpread({}, config), {}, {
      xType: config.xType ?? "Date",
      defaultValue: config.defaultValue?.toString(),
      shouldValidate: config.shouldValidate,
      render: config.render,
      inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
        type: "date"
      }),

      validate(value) {
        const localDate = parseLocalDate(value);

        if (localDate == null) {
          return xGlobalConfig.strings.invalidDate;
        }

        const age = localDate.until(_jsJoda.LocalDate.now()).years();

        if (age < (config.minAge ?? -Infinity)) {
          return xGlobalConfig.strings.tooYoung(config.minAge);
        }

        return "OK";
      }

    }), {
      resetExtraState: () => {}
    });
    return _objectSpread(_objectSpread({}, innerField), {}, {
      config,
      initialValue: parseLocalDate(innerField.initialValue),
      value: parseLocalDate(innerField.value),
      reactiveValue: parseLocalDate(innerField.reactiveValue),

      setValue(action) {
        if (typeof action === "function") {
          innerField.setValue(prev => action(parseLocalDate(prev)).toString());
        } else {
          innerField.setValue(action.toString());
        }
      },

      getLatestValue() {
        return parseLocalDate(innerField.getLatestValue());
      },

      validate(value) {
        return innerField.validate(value.toString());
      }

    });
  }

  function parseLocalDate(value) {
    try {
      return _jsJoda.LocalDate.parse(value);
    } catch (error) {
      if (error instanceof _jsJoda.DateTimeParseException) return null;else throw error;
    }
  }
});
//# sourceMappingURL=useDateField.js.map