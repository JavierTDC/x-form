import {Field} from "./useField";
import {useMemo} from "react";

export function isBlank(s: string): boolean {
  return s.trim().length === 0;
}

export function useConstant<T>(factory: () => T): T {
  return useMemo(factory, []);
}

export function scrollToField(field: Field<any>): void {
  if (field.ref.current != null) {
    field.ref.current.scrollIntoView({
      behavior: "smooth",
    });
  } else {
    // defensive behavior
    scrollToTop();
    console.error(`I tried to scroll to the field named ${field.config.name}, ` +
        `but it doesn't have a ref to scroll to`);
  }
}

export function scrollToTop(): void {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  })
}