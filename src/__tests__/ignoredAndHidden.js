import { getter } from '../test-utils/common';
import { useForm } from '../useForm';
import { useTextField } from '../field-types/useTextField';
import { act } from '@testing-library/react-hooks'

function useRegularField() {
    return useTextField({
        name: 'regular',
        defaultValue: 'reg'
    });
}

function useIgnoredField() {
    return useTextField({
        name: 'ignored',
        ignored: true,
        defaultValue: 'ign',
    });
}

function useHiddenField() {
    return useTextField({
        name: 'hidden',
        hidden: true,
        defaultValue: 'hid',
    });
}

function useIgnoredAndHiddenField() {
    return useTextField({
        name: 'ignoredAndHidden',
        ignored: true,
        hidden: true,
        defaultValue: 'iah'
    });
}

function useRemovedField() {
    return useTextField({
        name: 'removed',
        removed: true,
        defaultValue: 'rem',
    });
}

function aFormWhenSubmitted(onSubmit) {
    return function useTheForm() {
        return useForm({
            fields: {
                regular: useRegularField(),
                ignored: useIgnoredField(),
                hidden: useHiddenField(),
                ignoredAndHidden: useIgnoredAndHiddenField(),
                removed: useRemovedField(),
            },
            async onSubmitWithValidValues(values) {
                onSubmit(values);
            }
        });
    }
}

describe('a regular field', () => {
    it('is submitted', async () => {
        const form = getter(aFormWhenSubmitted(values => {
            expect(values.regular).toBe('reg');
        }));
        await act(form().submit);
    });

    it('is rendered', () => {
        const field = getter(useRegularField);
        expect(field().render() != null).toBe(true);
    });
});

describe('a ignored field', () => {
    it('is not submitted', async () => {
        const form = getter(aFormWhenSubmitted(values => {
            expect('ignored' in values).toBe(false);
        }));
        await act(form().submit);
    });

    it('is rendered', () => {
        const field = getter(useIgnoredField);
        expect(field().render() != null).toBe(true);
    });
});

describe('a hidden field', () => {
   it('is submitted', async () => {
       const form = getter(aFormWhenSubmitted(values => {
           expect(values.hidden).toBe('hid');
       }));
       await act(form().submit);
   });

   it('is not rendered', () => {
      const field = getter(useHiddenField);
      expect(field().render()).toBeNull();
   });
});

describe('an ignored and hidden field', () => {
    it('is not submitted', async () => {
        const form = getter(aFormWhenSubmitted(values => {
            expect('ignoredAndHidden' in values).toBe(false);
        }));
        await act(form().submit);
    });

    it('is not rendered', () => {
        const field = getter(useIgnoredAndHiddenField);
        expect(field().render()).toBeNull();
    });
});

describe('a removed field', () => {
    it('is not submitted', async () => {
        const form = getter(aFormWhenSubmitted(values => {
            expect('removed' in values).toBe(false);
        }));
        await act(form().submit);
    });

    it('is not rendered', () => {
        const field = getter(useRemovedField);
        expect(field().render()).toBeNull();
    });
});