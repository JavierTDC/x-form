(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "./useXGlobalConfig", "./utils", "lodash"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("./useXGlobalConfig"), require("./utils"), require("lodash"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.useXGlobalConfig, global.utils, global.lodash);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _useXGlobalConfig, _utils, _lodash) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useForm = useForm;

  var _react2 = _interopRequireDefault(_react);

  var _lodash2 = _interopRequireDefault(_lodash);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function checkFormConfig(config) {
    if (_lodash2.default.isEmpty(config.fields)) throw new Error("Form with no fields");

    _lodash2.default.forEach(config.fields, (field, key) => {
      if (field.config.name !== key) {
        throw new Error(`In the "fields" object you passed to useForm, the field at "${key}" ` + `has a different name "${field.config.name}".`);
      }
    });
  }

  function useForm(config) {
    checkFormConfig(config);
    const xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)(); // there can be ignored and/or hidden fields here

    const allFields = config.fields; // here is where we ignore the ignored fields

    const consideredFields = _lodash2.default.omitBy(allFields, ({
      config
    }) => // removed fields are ignored by definition
    config.ignored || config.removed); // those will not have the hidden fields


    const shownFields = _lodash2.default.omitBy(allFields, ({
      config
    }) => // removed fields are hidden by definition
    config.hidden || config.removed); // convert the field values to the FormValues dictionary representation


    const values = _lodash2.default.mapValues(consideredFields, "value"); // on a long form you may want to scroll to the first invalid field on submit


    const firstInvalidField = Object.values(shownFields).find(field => !field.valid); // consider making useValidator able to handle this as well to avoid duplication

    function validate() {
      if (firstInvalidField != null) {
        return xGlobalConfig.strings.invalidForm;
      }

      return config.validate?.(values) ?? "OK";
    }

    const validationResult = validate();
    const isValid = validationResult === "OK";
    const [isSubmitting, setIsSubmitting] = (0, _react.useState)(false);

    function render() {
      return config.render?.(config.fields) ?? /*#__PURE__*/_react2.default.createElement("form", config.props, _lodash2.default.map(config.fields, (field, name) => /*#__PURE__*/_react2.default.createElement(_react2.default.Fragment, {
        key: name
      }, field.render())));
    }

    return {
      config,
      fields: config.fields,
      props: _objectSpread(_objectSpread({}, config.props), {}, {
        onSubmit(event) {
          event.preventDefault();
          config.props?.onSubmit?.(event);
        }

      }),
      values,

      setValues(values) {
        Object.entries(values).forEach(([name, value]) => {
          config.fields[name].setValue(value);
        });
      },

      isValid,
      isSubmitting,

      async submit() {
        setIsSubmitting(true);

        try {
          _lodash2.default.forEach(consideredFields, field => field.onSubmit());

          if (isValid) {
            await config.onSubmitWithValidValues(values);
          } else if (firstInvalidField != null) {
            // in future versions the user should be choosing the behavior here
            (0, _utils.scrollToField)(firstInvalidField);
          } else {
            // and here
            (0, _utils.scrollToTop)();
          }
        } finally {
          setIsSubmitting(false);
        }
      },

      reset() {
        _lodash2.default.forEach(allFields, field => field.reset());
      },

      render
    };
  }
});
//# sourceMappingURL=useForm.js.map