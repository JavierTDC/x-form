import {Field, FieldConfig} from "../useField";
import {InputProps} from "./useTextField";
import {useXGlobalConfig} from "../useXGlobalConfig";
import {HtmlInputField, useHtmlInput} from "./useHtmlInput";

export interface NumberFieldConfig extends FieldConfig<number> {
  readonly inputProps: Partial<InputProps> & {
    // required
    min: number;
    max: number;
  };  // prop overrides for <input/>
  readonly decimals?: number;
  readonly nonZero?: boolean;
}

export interface NumberField extends Field<number> {
  config: NumberFieldConfig;
  inputProps: InputProps;
  isFocused: boolean;
  innerField: HtmlInputField;
}

export function useNumberField(config: NumberFieldConfig): NumberField {
  const xGlobalConfig = useXGlobalConfig();

  const innerField = useHtmlInput({
    ...config,
    xType: config.xType ?? "Number",

    defaultValue: config.defaultValue?.toString(),
    shouldValidate: config.shouldValidate as any,
    render: config.render as any,

    inputProps: {
      ...config.inputProps,
      // type: "number",
      step: config.inputProps.step
          ?? 10 ** -(config.decimals ?? 0),
    },

    validate(value: string): string {
      value = value.trim();

      const match = /^([0-9]|-[1-9])[0-9]*(\.([0-9]*))?$/.exec(value);
      if (!match) {
        return xGlobalConfig.strings.invalidNumber;
      }

      const decimalPart = match[3] ?? "";
      if (decimalPart && !config.decimals) {
        return xGlobalConfig.strings.shouldBeInteger;
      }

      if (decimalPart.length > (config.decimals ?? 0)) {
        return xGlobalConfig.strings.tooManyDecimals;
      }

      const x = Number(value);

      if (x < 0 && config.inputProps.min >= 0) {
        return xGlobalConfig.strings.negativeNumber;
      }

      if (x === 0 && config.nonZero) {
        return xGlobalConfig.strings.cantBeZero;
      }

      if (x < config.inputProps.min) {
        return xGlobalConfig.strings.numberTooSmall(config.inputProps.min);
      }

      if (x > config.inputProps.max) {
        return xGlobalConfig.strings.numberTooBig(config.inputProps.max);
      }

      return config.validate?.(x) ?? "OK";
    },
  }, {
    resetExtraState: () => {},
  });

  return {
    ...innerField,
    config,
    innerField,

    initialValue: Number(innerField.initialValue),
    value: Number(innerField.value),
    reactiveValue: Number(innerField.reactiveValue),
    setValue(action): void {
      if (typeof action === "function") {
        innerField.setValue(prev =>
            action(Number(prev)).toFixed(config.decimals));
      } else {
        innerField.setValue(action.toFixed(config.decimals));
      }
    },
    getLatestValue(): number {
      return Number(innerField.getLatestValue());
    },

    validate(value: number): string {
      return innerField.validate(value.toString());
    },
  };
}