import { TextField } from './useTextField';
import { useHtmlInput } from './useHtmlInput';
import { DetailedHTMLProps, EffectCallback, OptionHTMLAttributes, useEffect, useRef } from 'react';
import { useXGlobalConfig } from '../useXGlobalConfig';
import _ from 'lodash';
import { EnumFieldConfig, EnumOption } from './useEnumField';
import { usePreviousStateEffect } from '../usePreviousStateEffect';

// the type of the props of <option/>
export type OptionProps = DetailedHTMLProps<OptionHTMLAttributes<HTMLOptionElement>, HTMLOptionElement>;

export interface SelectFieldConfig extends EnumFieldConfig {
  readonly blankOptionLabel?: string;
}

export interface SelectField extends TextField {
  readonly config: SelectFieldConfig;
  readonly options: Array<EnumOption & {props: OptionProps}>;
}

export function useSelectField(config: SelectFieldConfig): SelectField {
  const xGlobalConfig = useXGlobalConfig();
  const blankOptions = config.defaultValue ? [] : [{
    value: "",
    label: config.blankOptionLabel ?? xGlobalConfig.strings.blankSelectField,
  }];

  let field = useHtmlInput({
    ...config,
    xType: config.xType ?? "Select",
  }, {
    resetExtraState: () => {},
  });

  // if the selected value disappears from the options, reset the value
  const currentOptions = config.options;
  usePreviousStateEffect(prevOptions => {
    const value = field.getLatestValue();
    const valueIn = (options: EnumOption[]) =>
        options.some(_.matches({ value }));

    if (valueIn(prevOptions) && !valueIn(currentOptions)) {
      field.resetValue();
    }
  }, currentOptions);

  return Object.assign(field, {
    config,

    options: [...blankOptions, ...config.options].map(option => ({
      ...option,
      props: { value: option.value },
    })),
  });
}