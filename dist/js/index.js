"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ = require("./");

Object.keys(_).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (key in exports && exports[key] === _[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _[key];
    }
  });
});
//# sourceMappingURL=index.js.map