import {useEffect} from "react";
import {Form} from "../useForm";
import {NumberField} from "../field-types/useNumberField";
import {ValueObject, hash} from "immutable";
import {Graph} from "./Graph";
import {dfs} from "./dfs";
import {asFunctor, Functor, Invalid, Valid} from "./Functor";
import {isBlank} from "../utils";

type Effect = {
  computedField: NumberField;
  formula(...args: number[]): number;
  requiredFields: NumberField[];
};

function findEffect(effects: Effect[], field: NumberField): Effect {
  const effect = effects.find(effect =>
      effect.computedField === field);
  if (!effect) throw new Error(`Couldn't find effect for field named "${field.config.name}"`);
  return effect;
}

export function useComputedValuesEffect(
    form: Form<any>,
    effects: Effect[],
): void {
  const reversedGraph = makeGraph(effects).withReversedEdges();
  const sourceNodes = reversedGraph.nodes();

  sourceNodes.forEach(sourceNode => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
      if (!sourceNode.field.isFocused && !isBlank(sourceNode.field.innerField.value)) {
        dfs({
          graph: reversedGraph,
          sourceNode,
          callback(currentNode: FieldNode) {
            if (!currentNode.equals(sourceNode)) {
              const fieldToUpdate = currentNode.field;
              const effect = findEffect(effects, fieldToUpdate);

              const fn = effect.formula;
              const args = effect.requiredFields.map(asFunctor);
              const computed = Functor.map(fn)(...args);

              if (computed! instanceof Valid) {
                fieldToUpdate.setValue(computed.value);
              } else if (computed! instanceof Invalid) {
                fieldToUpdate.resetValue();
              }
            }
          },
        });
      }
      // eslint-disable-next-line
    }, [sourceNode.field.isFocused]);
  });
}

class FieldNode implements ValueObject {
  readonly field: NumberField;

  constructor(field: NumberField) {
    this.field = field;
  }

  equals(other: any): boolean {
    if (!(other instanceof FieldNode)) return false;
    return this.field.config.name === other.field.config.name;
  }

  hashCode(): number {
    return hash(this.field.config.name);
  }
}


function makeGraph(effects: Effect[]): Graph<FieldNode> {
  let graph = Graph.empty<FieldNode>();
  effects.forEach(effect => {
    const u = new FieldNode(effect.computedField);
    effect.requiredFields.forEach(requiredField => {
      const v = new FieldNode(requiredField);
      graph.addEdge(u, v);
    });
  });
  return graph;
}