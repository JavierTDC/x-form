(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "prop-types", "react", "@tdc-cl/utils"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("prop-types"), require("react"), require("@tdc-cl/utils"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.propTypes, global.react, global.utils);
    global.undefined = mod.exports;
  }
})(this, function (exports, _propTypes, _react, _utils) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Rut = Rut;

  var _propTypes2 = _interopRequireDefault(_propTypes);

  var React = _interopRequireWildcard(_react);

  function _getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();

    _getRequireWildcardCache = function () {
      return cache;
    };

    return cache;
  }

  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    }

    if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
      return {
        default: obj
      };
    }

    var cache = _getRequireWildcardCache();

    if (cache && cache.has(obj)) {
      return cache.get(obj);
    }

    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

        if (desc && (desc.get || desc.set)) {
          Object.defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }

    newObj.default = obj;

    if (cache) {
      cache.set(obj, newObj);
    }

    return newObj;
  }

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};

    var target = _objectWithoutPropertiesLoose(source, excluded);

    var key, i;

    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }

    return target;
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;

    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }

    return target;
  }

  function Rut(_ref) {
    let {
      onValid,
      rut: initialState,
      format = 'dotted'
    } = _ref,
        props = _objectWithoutProperties(_ref, ["onValid", "rut", "format"]);

    const maxLength = React.useRef(format === 'dotted' ? 12 : 10);
    const [text, setText] = React.useState(setInitialState);
    React.useEffect(() => {
      if (onValid && _utils.rut.validate(text)) {
        onValid(text);
      }
    }, [onValid, text]);

    function setInitialState() {
      if (initialState) {
        const valid = _utils.rut.validate(initialState);

        if (valid) return initialState;
        console.error(`Error trying to use '${initialState}' as initial state. Please provide a valid and formatted RUT.`);
      }

      return '';
    }

    function onChange(event) {
      const value = _utils.rut.format(event.target.value, format);

      setText(value);

      if (props.onRutChange) {
        props.onRutChange(value, _utils.rut.validate(value));
      }
    }

    return /*#__PURE__*/React.createElement("input", _extends({
      type: "text",
      value: text,
      onChange: onChange,
      maxLength: maxLength.current
    }, props));
  }

  Rut.propTypes = {
    rut: _propTypes2.default.string,
    format: _propTypes2.default.oneOf(['dotted', 'dashed'])
  };
});
//# sourceMappingURL=Rut.js.map