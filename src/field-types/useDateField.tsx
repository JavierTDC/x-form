import {Field, FieldConfig} from "../useField";
import {DateTimeParseException, LocalDate} from "js-joda";
import { useHtmlInput } from "./useHtmlInput";
import {InputProps} from "./useTextField";
import {useXGlobalConfig} from "../useXGlobalConfig";

export interface DateFieldConfig extends FieldConfig<LocalDate> {
  readonly inputProps?: Partial<InputProps>;
  readonly minAge?: number;
}

export interface DateField extends Field<LocalDate> {
  config: DateFieldConfig;
  inputProps: InputProps;
}

export function useDateField(config: DateFieldConfig): DateField {
  const xGlobalConfig = useXGlobalConfig();

  const innerField = useHtmlInput({
    ...config,
    xType: config.xType ?? "Date",

    defaultValue: config.defaultValue?.toString(),
    shouldValidate: config.shouldValidate as any,
    render: config.render as any,

    inputProps: {
      ...config.inputProps,
      type: "date",
    },

    validate(value: string): string {
      const localDate = parseLocalDate(value);
      if (localDate == null) {
        return xGlobalConfig.strings.invalidDate;
      }

      const age = localDate.until(LocalDate.now()).years();
      if (age < (config.minAge ?? -Infinity)) {
        return xGlobalConfig.strings.tooYoung(config.minAge!);
      }

      return "OK";
    },
  }, {
    resetExtraState: () => {},
  });

  return {
    ...innerField,
    config,

    initialValue: parseLocalDate(innerField.initialValue)!,
    value: parseLocalDate(innerField.value)!,
    reactiveValue: parseLocalDate(innerField.reactiveValue)!,
    setValue(action): void {
      if (typeof action === "function") {
        innerField.setValue(prev =>
            action(parseLocalDate(prev)!).toString());
      } else {
        innerField.setValue(action.toString());
      }
    },
    getLatestValue(): LocalDate {
      return parseLocalDate(innerField.getLatestValue())!;
    },

    validate(value: LocalDate): string {
      return innerField.validate(value.toString());
    },
  }
}

function parseLocalDate(value: string): LocalDate | null {
  try {
    return LocalDate.parse(value);
  } catch (error) {
    if (error instanceof DateTimeParseException) return null;
    else throw error;
  }
}