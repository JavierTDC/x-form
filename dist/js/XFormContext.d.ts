/// <reference types="react" />
export declare type RecursivePartial<T> = {
    [P in keyof T]?: T[P] extends (infer U)[] ? RecursivePartial<U>[] : T[P] extends object ? RecursivePartial<T[P]> : T[P];
};
export interface XFormConfig {
    readonly strings: {
        requiredField: string;
        optional: string;
        invalidForm: string;
        blankSelectField: string;
        invalidNumber: string;
        negativeNumber: string;
        numberTooSmall(minValue: number): string;
        numberTooBig(maxValue: number): string;
        tooManyDecimals: string;
        shouldBeInteger: string;
        cantBeZero: string;
        invalidDate: string;
        tooYoung(minAge: number): string;
    };
}
export declare const defaultXFormConfig: {
    strings: {
        requiredField: string;
        optional: string;
        invalidForm: string;
        blankSelectField: string;
        invalidNumber: string;
        negativeNumber: string;
        numberTooSmall: (minValue: number) => string;
        numberTooBig: (maxValue: number) => string;
        tooManyDecimals: string;
        shouldBeInteger: string;
        cantBeZero: string;
        invalidDate: string;
        tooYoung: (minAge: number) => string;
    };
};
export declare const XFormContext: import("react").Context<RecursivePartial<XFormConfig>>;
