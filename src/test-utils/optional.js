import {renderHook, act} from '@testing-library/react-hooks'
import { getter } from './common';

export function testBlankField(kind, useField, expectedValid) {
    it(spell(`should allow a ${kind} field to be blank`), () => {
        const field = getter(useField);
        expect(field().valid).toBe(expectedValid);
    });
}

export function testInvalidField(kind, useField, invalidValue) {
    it(spell(`should not allow a ${kind} field to have an invalid value`), () => {
        const field = getter(useField);
        act(() => field().setValue(invalidValue));
        expect(field().valid).toBe(false);
    });
}

export function testValidField(kind, useField, validValue) {
    it(spell(`should allow a ${kind} field to have a valid value`), () => {
       const field = getter(useField);
       act(() => field().setValue(validValue));
       expect(field().valid).toBe(true);
    });
}

function spell(brokenEnglish) {
    return brokenEnglish.replace('a optional', 'an optional');
}
