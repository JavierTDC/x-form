"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useXGlobalConfig = useXGlobalConfig;

var _react = _interopRequireDefault(require("react"));

var _merge = _interopRequireDefault(require("lodash/merge"));

var _XFormContext = require("./XFormContext");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function useXGlobalConfig() {
  var context = _react.default.useContext(_XFormContext.XFormContext);

  return (0, _merge.default)({}, _XFormContext.defaultXFormConfig, context);
}
//# sourceMappingURL=useXGlobalConfig.js.map