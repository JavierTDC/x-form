import { useNumberField } from '../field-types/useNumberField';
import { testBlankField, testInvalidField, testValidField } from '../test-utils/optional';

const name = 'test';
const inputProps = {
    min: 0,
    max: 10,
};
const invalidNumber = {
    toFixed: () => "42o",
};
const outOfRangeNumber = inputProps.max + 1;
const validNumber = inputProps.min;

const hooks = {
    optional: () => useNumberField({
        name,
        optional: true,
        inputProps,
    }),
    required: () => useNumberField({
        name,
        inputProps,
    }),
}

describe('useNumberField', () => {
    testBlankField('optional', hooks.optional, true);
    testBlankField('required', hooks.required, false);
    testInvalidField('optional', hooks.optional, invalidNumber);
    testInvalidField('required', hooks.required, invalidNumber);
    testInvalidField('optional', hooks.optional, outOfRangeNumber);
    testInvalidField('required', hooks.required, outOfRangeNumber);
    testValidField('optional', hooks.optional, validNumber);
    testValidField('required', hooks.required, validNumber);
});