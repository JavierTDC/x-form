"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useHtmlInput = useHtmlInput;

var _react = _interopRequireWildcard(require("react"));

var _utils = require("../utils");

var _useField = require("../useField");

var _useXState = require("../useXState");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function useHtmlInput(config, internal) {
  var _config$defaultValue, _config$inputProps$id, _config$inputProps, _config$inputProps$na, _config$inputProps2;

  var inputRef = (0, _react.useRef)(null);
  var state = (0, _useXState.useXState)((_config$defaultValue = config.defaultValue) !== null && _config$defaultValue !== void 0 ? _config$defaultValue : "");

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isFocused = _useState2[0],
      setIsFocused = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      hasBeenBlurred = _useState4[0],
      setHasBeenBlurred = _useState4[1];

  function shouldValidate(field) {
    return hasBeenBlurred || field.hasBeenSubmitted;
  }

  var field = (0, _useField.useField)(_objectSpread(_objectSpread({}, config), {}, {
    shouldValidate: shouldValidate
  }), _objectSpread(_objectSpread({}, state), {}, {
    resetExtraState: function resetExtraState() {
      setHasBeenBlurred(false);
      internal.resetExtraState();
    },
    isBlank: _utils.isBlank
  }));
  return Object.assign(field, {
    inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
      id: (_config$inputProps$id = (_config$inputProps = config.inputProps) === null || _config$inputProps === void 0 ? void 0 : _config$inputProps.id) !== null && _config$inputProps$id !== void 0 ? _config$inputProps$id : config.name,
      name: (_config$inputProps$na = (_config$inputProps2 = config.inputProps) === null || _config$inputProps2 === void 0 ? void 0 : _config$inputProps2.name) !== null && _config$inputProps$na !== void 0 ? _config$inputProps$na : config.name,
      value: state.value,
      onChange: function onChange(event) {
        var _config$inputProps3, _config$inputProps3$o;

        state.setValue(event.target.value);
        (_config$inputProps3 = config.inputProps) === null || _config$inputProps3 === void 0 ? void 0 : (_config$inputProps3$o = _config$inputProps3.onChange) === null || _config$inputProps3$o === void 0 ? void 0 : _config$inputProps3$o.call(_config$inputProps3, event);
      },
      onFocus: function onFocus(event) {
        var _config$inputProps4, _config$inputProps4$o;

        setIsFocused(true);
        (_config$inputProps4 = config.inputProps) === null || _config$inputProps4 === void 0 ? void 0 : (_config$inputProps4$o = _config$inputProps4.onFocus) === null || _config$inputProps4$o === void 0 ? void 0 : _config$inputProps4$o.call(_config$inputProps4, event);
      },
      onBlur: function onBlur(event) {
        var _config$inputProps5, _config$inputProps5$o;

        setIsFocused(false);
        setHasBeenBlurred(true);
        (_config$inputProps5 = config.inputProps) === null || _config$inputProps5 === void 0 ? void 0 : (_config$inputProps5$o = _config$inputProps5.onBlur) === null || _config$inputProps5$o === void 0 ? void 0 : _config$inputProps5$o.call(_config$inputProps5, event);
      }
    }),
    inputRef: inputRef,
    isFocused: isFocused,
    isBlank: (0, _utils.isBlank)(state.value)
  });
}
//# sourceMappingURL=useHtmlInput.js.map