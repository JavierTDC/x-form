import {Field, FieldConfig, useField} from "../useField";
import {List} from "immutable";
import {Form} from "../useForm";
import {ButtonHTMLAttributes, DetailedHTMLProps, MouseEvent} from "react";
import {useXState} from "../useXState";

export type ButtonProps = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;

export interface ListFieldConfig<T> extends FieldConfig<List<T>> {
  readonly form: Form<T>;
  readonly buttonProps?: Partial<ButtonProps>;
  readonly emptyMessage?: string;
}

export interface ListField<T> extends Field<List<T>> {
  readonly config: ListFieldConfig<T>;
  readonly buttonProps: ButtonProps;
  add(item: T): void;
  update(index: number, newValues: T): void;
  remove(index: number): void;
}

export function useListField<T>(config: ListFieldConfig<T>): ListField<T> {
  const state = useXState(config.defaultValue ?? List<T>());

  function add(item: T): void {
    state.setValue(list => list.push(item));
  }

  function update(index: number, newValues: T): void {
    state.setValue(list => list.set(index, newValues));
  }

  function remove(index: number): void {
    state.setValue(list => list.remove(index));
  }

  const requiredFieldMessage =
      config.emptyMessage  // support for deprecated property
      ?? config.requiredFieldMessage
      ?? "Can't be empty";

  let field = useField<List<T>>({
    ...config,
    xType: config.xType ?? "List",
    requiredFieldMessage,
  }, {
    ...state,
    resetExtraState: () => {},
    isBlank: list => list.isEmpty(),
  });

  return Object.assign(field, {
    config,

    buttonProps: {
      ...config.buttonProps,

      onClick(event: MouseEvent<HTMLButtonElement>): void {
        add(config.form.values as T);
        config.form.reset();
        config.buttonProps?.onClick?.(event);
      },

      disabled: config.buttonProps?.disabled || !config.form.isValid,
    },

    add, update, remove,
  });
}