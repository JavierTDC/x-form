import {ChangeEvent, RefObject, useRef} from "react";
import {Field, FieldConfig, useField} from "../useField";
import {InputProps} from "./useTextField";
import {useXState} from "../useXState";

export interface BinaryFieldConfig extends FieldConfig<boolean> {
  readonly inputProps?: Partial<InputProps>;
}

export interface BinaryField extends Field<boolean> {
  config: BinaryFieldConfig;
  inputProps: InputProps;
  inputRef: RefObject<HTMLInputElement>;
}

export function useBinaryField(config: BinaryFieldConfig, internal: {
  resetExtraState(): void;
}): BinaryField {
  const inputRef = useRef<HTMLInputElement>(null);
  const state = useXState(config.defaultValue ?? false);

  let field = useField(config, {
    ...state,
    resetExtraState: internal.resetExtraState,
    isBlank: _checked => false,
  });

  return Object.assign(field, {
    inputProps: {
      ...config.inputProps,

      type: "checkbox",
      id: config.inputProps?.id ?? config.name,
      name: config.inputProps?.name ?? config.name,

      checked: state.value,
      onChange(event: ChangeEvent<HTMLInputElement>): void {
        state.setValue(event.target.checked);
        config.inputProps?.onChange?.(event);
      },
    },

    inputRef,
  });
}