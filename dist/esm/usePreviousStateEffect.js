import React, { useEffect, useRef } from 'react';
export function usePreviousStateEffect(effect, state) {
  const prevStateRef = useRef(state);
  useEffect(() => {
    const destructor = effect(prevStateRef.current);
    prevStateRef.current = state;
    return destructor;
  }, [state]);
}
//# sourceMappingURL=usePreviousStateEffect.js.map