(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.isBlank = isBlank;
  exports.useConstant = useConstant;
  exports.scrollToField = scrollToField;
  exports.scrollToTop = scrollToTop;

  function isBlank(s) {
    return s.trim().length === 0;
  }

  function useConstant(factory) {
    return (0, _react.useMemo)(factory, []);
  }

  function scrollToField(field) {
    if (field.ref.current != null) {
      field.ref.current.scrollIntoView({
        behavior: "smooth"
      });
    } else {
      // defensive behavior
      scrollToTop();
      console.error(`I tried to scroll to the field named ${field.config.name}, ` + `but it doesn't have a ref to scroll to`);
    }
  }

  function scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }
});
//# sourceMappingURL=utils.js.map