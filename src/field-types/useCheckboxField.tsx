import {BinaryField, BinaryFieldConfig, useBinaryField} from "./useBinaryField";

export interface CheckboxFieldConfig extends BinaryFieldConfig {}

export interface CheckboxField extends BinaryField {
  config: CheckboxFieldConfig;
}

export function useCheckboxField(config: CheckboxFieldConfig): CheckboxField {
  return useBinaryField({
    ...config,
    xType: config.xType ?? "Checkbox",
  }, {
    resetExtraState: () => {},
  });
}