(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "@testing-library/react-hooks"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("@testing-library/react-hooks"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.reactHooks);
    global.undefined = mod.exports;
  }
})(this, function (exports, _reactHooks) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.getter = getter;

  function getter(hook) {
    const {
      result
    } = (0, _reactHooks.renderHook)(hook);
    return () => result.current;
  }
});
//# sourceMappingURL=common.js.map