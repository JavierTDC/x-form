export declare function usePreviousStateEffect<T>(effect: PreviousStateEffectCallback<T>, state: T): void;
declare type PreviousStateEffectCallback<T> = (prevState: T) => (void | (() => void | undefined));
export {};
