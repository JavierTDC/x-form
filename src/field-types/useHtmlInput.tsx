import { ChangeEvent, FocusEvent, RefObject, useRef, useState } from 'react';
import { isBlank } from '../utils';
import { InputProps } from './useTextField';
import { Field, FieldConfig, PartialField, useField } from '../useField';
import { useXState } from '../useXState';

export interface HtmlInputFieldConfig extends FieldConfig<string> {
  readonly inputProps?: Partial<InputProps>;  // prop overrides for <input/>
}

export interface HtmlInputField extends Field<string> {
  readonly config: HtmlInputFieldConfig;
  readonly inputProps: InputProps;
  readonly inputRef: RefObject<HTMLInputElement>;
  readonly isFocused: boolean;
}

export function useHtmlInput(config: HtmlInputFieldConfig, internal: {
  resetExtraState(): void;
}): HtmlInputField {
  const inputRef = useRef<HTMLInputElement>(null);

  const state = useXState(config.defaultValue ?? "");
  const [isFocused, setIsFocused] = useState(false);
  const [hasBeenBlurred, setHasBeenBlurred] = useState(false);

  function shouldValidate(field: PartialField<string>): boolean {
    return hasBeenBlurred || field.hasBeenSubmitted;
  }

  let field = useField({
    ...config,
    shouldValidate,
  }, {
    ...state,
    resetExtraState(): void {
      setHasBeenBlurred(false);
      internal.resetExtraState();
    },
    isBlank,
  });

  return Object.assign(field, {
    inputProps: {
      ...config.inputProps,

      id: config.inputProps?.id ?? config.name,
      name: config.inputProps?.name ?? config.name,

      value: state.value,
      onChange(event: ChangeEvent<HTMLInputElement>): void {
        state.setValue(event.target.value);
        config.inputProps?.onChange?.(event);
      },

      onFocus(event: FocusEvent<HTMLInputElement>): void {
        setIsFocused(true);
        config.inputProps?.onFocus?.(event);
      },

      onBlur(event: FocusEvent<HTMLInputElement>): void {
        setIsFocused(false);
        setHasBeenBlurred(true);
        config.inputProps?.onBlur?.(event);
      },
    },

    inputRef,
    isFocused,
    isBlank: isBlank(state.value),
  });
}