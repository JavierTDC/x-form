"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useFieldState = useFieldState;
exports.Field = exports.FieldLogic = exports.ON_SUBMIT = void 0;

var _Result = require("../Result");

var _react = _interopRequireWildcard(require("react"));

var _FormTheme = require("../FormTheme");

var _useXState = require("../useXState");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/** Used internally by `Form` to trigger the "on submit" event on its fields. */
var ON_SUBMIT = Symbol('ON_SUBMIT');
exports.ON_SUBMIT = ON_SUBMIT;

var FieldLogic = /*#__PURE__*/function () {
  function FieldLogic(config) {
    var _this$config$parse, _this$config$validate, _this$config$format;

    _classCallCheck(this, FieldLogic);

    _defineProperty(this, "parse", (_this$config$parse = this.config.parse) !== null && _this$config$parse !== void 0 ? _this$config$parse : this.defaultParser);

    _defineProperty(this, "validate", (_this$config$validate = this.config.validate) !== null && _this$config$validate !== void 0 ? _this$config$validate : this.defaultValidator);

    _defineProperty(this, "format", (_this$config$format = this.config.format) !== null && _this$config$format !== void 0 ? _this$config$format : this.defaultFormatter);

    this.config = config;
  }

  _createClass(FieldLogic, [{
    key: "defaultValidator",
    value: function defaultValidator(value) {
      return (0, _Result.Valid)(value);
    }
  }, {
    key: "initialInput",
    get: function get() {
      if (this.config.defaultInput != null) {
        return this.config.defaultInput;
      }

      if (this.config.defaultValue != null) {
        return this.format(this.config.defaultValue);
      }

      return this.blankInput;
    }
  }]);

  return FieldLogic;
}();

exports.FieldLogic = FieldLogic;
var CACHED_RESULT = Symbol('CACHED_RESULT');

var StatefulFieldMixin = /*#__PURE__*/function () {
  function StatefulFieldMixin(logic, state) {
    _classCallCheck(this, StatefulFieldMixin);

    this.logic = logic;
    this.state = state;
  }

  _createClass(StatefulFieldMixin, [{
    key: "setInput",
    value: function setInput(action) {
      this.state.inputState.setValue(action);
    }
  }, {
    key: "setValue",
    value: function setValue(newValue) {
      this.setInput(this.logic.format(newValue));
    }
  }, {
    key: "reset",
    value: function reset() {
      this.state.reset();
    }
  }, {
    key: "getLatestInput",
    value: function getLatestInput() {
      return this.state.inputState.getLatestValue();
    }
  }, {
    key: "input",
    get: function get() {
      return this.state.inputState.reactiveValue;
    }
  }, {
    key: "initialInput",
    get: function get() {
      return this.state.inputState.initialValue;
    }
  }, {
    key: "hasBeenSubmitted",
    get: function get() {
      return this.state.hasBeenSubmitted;
    }
  }, {
    key: "isFocused",
    get: function get() {
      return this.state.isFocused;
    }
  }]);

  return StatefulFieldMixin;
}();

var Field = /*#__PURE__*/function (_StatefulFieldMixin) {
  _inherits(Field, _StatefulFieldMixin);

  var _super = _createSuper(Field);

  function Field(config, logic, state, ref) {
    var _this;

    _classCallCheck(this, Field);

    _this = _super.call(this, logic, state);

    _defineProperty(_assertThisInitialized(_this), CACHED_RESULT, null);

    _this.config = config;
    _this.logic = logic;
    _this.state = state;
    _this.ref = ref;
    return _this;
  }

  _createClass(Field, [{
    key: ON_SUBMIT,
    value: function value() {
      this.state.setHasBeenSubmitted(true);
    }
  }, {
    key: "getResultSlow",
    value: function getResultSlow() {
      return this.logic.parse(this.input).chain(this.logic.validate);
    }
  }, {
    key: "unwrapValue",
    value: function unwrapValue() {
      return this.result.unwrap();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      if (this.config.render != null) {
        return this.config.render(this);
      }

      return /*#__PURE__*/_react.default.createElement(_FormTheme.FormThemeContext.Consumer, null, function (formTheme) {
        var _formTheme$render$_th, _formTheme$render;

        return (_formTheme$render$_th = (_formTheme$render = formTheme.render)[_this2.xType]) === null || _formTheme$render$_th === void 0 ? void 0 : _formTheme$render$_th.call(_formTheme$render, _this2);
      });
    }
  }, {
    key: "result",
    get: function get() {
      var _this$CACHED_RESULT;

      this[CACHED_RESULT] = (_this$CACHED_RESULT = this[CACHED_RESULT]) !== null && _this$CACHED_RESULT !== void 0 ? _this$CACHED_RESULT : this.getResultSlow();
      return this[CACHED_RESULT];
    }
  }, {
    key: "isValid",
    get: function get() {
      return this.result instanceof _Result.Valid;
    }
  }]);

  return Field;
}(StatefulFieldMixin);

exports.Field = Field;

function useFieldState(initialInput) {
  var inputState = (0, _useXState.useXState)(initialInput); // const extraState = useXState(initialExtra);

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isFocused = _useState2[0],
      setIsFocused = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      hasBeenBlurred = _useState4[0],
      setHasBeenBlurred = _useState4[1];

  var _useState5 = (0, _react.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      hasBeenSubmitted = _useState6[0],
      setHasBeenSubmitted = _useState6[1];

  function reset() {
    setHasBeenSubmitted(false);
    setHasBeenBlurred(false);
    inputState.resetValue(); // extraState.resetValue();
  }

  return {
    inputState: inputState,
    // extraState,
    isFocused: isFocused,
    setIsFocused: setIsFocused,
    hasBeenBlurred: hasBeenBlurred,
    setHasBeenBlurred: setHasBeenBlurred,
    hasBeenSubmitted: hasBeenSubmitted,
    setHasBeenSubmitted: setHasBeenSubmitted,
    reset: reset
  };
}
/** The error message shown to the dev when a field doesn't have a render
 * function in the `FormTheme` implementation. */


function MissingThemeRender(_ref) {
  var field = _ref.field;
  return /*#__PURE__*/_react.default.createElement("p", null, "Missing render function for field of type ", field.xType, " in FormTheme implementation");
}
//# sourceMappingURL=Field.js.map