import * as React from 'react';
import { Phone } from './Phone';
export default {
  title: 'Phone',
  component: Phone
};

const Template = args => /*#__PURE__*/React.createElement(Phone, args);

export const PhoneNormal = Template.bind({});
PhoneNormal.args = {
  format: 'mobile'
};
//# sourceMappingURL=Phone.stories.js.map