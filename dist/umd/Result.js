(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "lodash", "immutable"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("lodash"), require("immutable"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.lodash, global.immutable);
    global.undefined = mod.exports;
  }
})(this, function (exports, _lodash, _immutable) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Blank = exports.Invalid = exports.Valid = exports.Result = undefined;

  var _lodash2 = _interopRequireDefault(_lodash);

  var _immutable2 = _interopRequireDefault(_immutable);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  /***
   *    ██████╗ ███████╗███████╗██╗   ██╗██╗  ████████╗
   *    ██╔══██╗██╔════╝██╔════╝██║   ██║██║  ╚══██╔══╝
   *    ██████╔╝█████╗  ███████╗██║   ██║██║     ██║
   *    ██╔══██╗██╔══╝  ╚════██║██║   ██║██║     ██║
   *    ██║  ██║███████╗███████║╚██████╔╝███████╗██║
   *    ╚═╝  ╚═╝╚══════╝╚══════╝ ╚═════╝ ╚══════╝╚═╝
   *
   */

  /** The return type for `parse` and `validate` functions.
   *
   * `Result`s are chainable functors that can store the following results after
   * a field parsing and/or validation:
   * - a `Valid(value)`
   * - an `Invalid(message)` where `message` is the error message shown to the
   * user.
   * - a `Blank()` for fields that have not been filled. */
  class Result {
    /** Use this instead of `===` for equality by value. */

    /** Applies the `fn` function to the value of a `Valid` result, returning
     * another `Valid` result with the transformed value.
     *
     * If applied to a non-`Valid` result it returns that same result unmodified.
     *
     * Examples:
     * - `Valid(42).map(x => x + 1)  // Valid(43)`
     * - `Invalid("Oops").map(x => x + 1)  // Invalid("Oops")`
     * - `Blank().map(x => x + 1)  // Blank()`
     * */

    /** Applies the `fn` function to the value of a `Valid` result, returning the
     * same result that `fn` returned.
     *
     * If applied to a non-`Valid` result it returns that same result unmodified.
     *
     * Examples:
     * - `Valid(42).chain(x => Invalid(x.toString()))  // Invalid("42")`
     * - `Invalid("Oops").chain(x => Invalid(x.toString())) // Invalid("Oops")`
     * - `Blank().chain(x => Invalid(x.toString())) // Blank()`
     * */

    /** Syntax sugar that is similar to a `match` statement in
     * a functional language, where the resulting expression depends on the type
     * of the result that `match` is applied to.
     *
     * Example:
     * ```
     * const description = result.match({
     *   valid: (value) => `It is a valid ${value}`,
     *   invalid: (message) => `Invalid with message ${message}`,
     *   blank: () => `A blank`,
     * });
     * ``` */

    /** Intuitively, it takes a function that doesn't work with `Result`s and
     * returns a new function that works with `Result` parameters and return type.
     *
     *
     * Example:
     * ```
     * const originalFn = (a, b, c) => a + b + c;
     * const newFn = Result.fMap(originalFn);
     * newFn(Valid(100), Valid(20), Valid(3))  // Valid(123)
     * newFn(Valid(100), Invalid("Oops"), Invalid("Boom"))  // Invalid("")
     * newFn(Valid(100), Invalid("Oops"), Blank())  // Invalid("")
     * newFn(Valid(100), Blank(), Blank())  // Blank()
     * ```
     *
     * Specifically, if `fn` is a function that receive N parameters of type `X`
     * and returns a value of type `Y`, `fMap` will return a new function that
     * receive N parameters of type `Result<X>` and returns a `Result<Y>`.
     *
     * Then the new function will work as follows:
     * - If all arguments are `Valid`, the result will be `Valid`.
     * - If there is any `Invalid` argument, the result will be `Invalid`.
     * - If there is any `Blank` argument (and no `Invalid`s), the result will be
     * `Blank`.
     * */
    static fMap(fn) {
      return function (...xs) {
        if (xs.every(x => x instanceof Valid)) {
          const unwrappedValues = _lodash2.default.map(xs, 'value');

          return Valid(fn(...unwrappedValues));
        }

        if (xs.some(x => x instanceof Invalid)) {
          return Invalid(""); // this message will never be shown anyway
        }

        return Blank();
      };
    }
    /** Gets the value if the result is `Valid` or throws otherwise. */


  }

  exports.Result = Result;

  /** The type for a function that can receive any number of arguments of type `X`
   * and returns a value of type `Y`. */

  /***
   *    ██╗   ██╗ █████╗ ██╗     ██╗██████╗
   *    ██║   ██║██╔══██╗██║     ██║██╔══██╗
   *    ██║   ██║███████║██║     ██║██║  ██║
   *    ╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
   *     ╚████╔╝ ██║  ██║███████╗██║██████╔╝
   *      ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
   *
   */

  /** @see Result */
  class $Valid extends Result {
    constructor(value) {
      super();
      this.value = value;
    }

    equals(other) {
      return other instanceof Valid && _immutable2.default.is(this.value, other.value);
    }

    map(fn) {
      return Valid(fn(this.value));
    }

    chain(fn) {
      return fn(this.value);
    }

    match(arms) {
      return arms.valid(this.value);
    }

    unwrap() {
      return this.value;
    }

  }
  /* The following code is dark magic to make `Valid` a proxy for the `$Valid`
   * class that can also be constructed without using the `new` keyword. */

  /** @see Result */

  /** @see Result */


  const Valid = exports.Valid = new Proxy($Valid, {
    apply: (_, __, [value]) => new $Valid(value)
  });
  /***
   *    ██╗███╗   ██╗██╗   ██╗ █████╗ ██╗     ██╗██████╗
   *    ██║████╗  ██║██║   ██║██╔══██╗██║     ██║██╔══██╗
   *    ██║██╔██╗ ██║██║   ██║███████║██║     ██║██║  ██║
   *    ██║██║╚██╗██║╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
   *    ██║██║ ╚████║ ╚████╔╝ ██║  ██║███████╗██║██████╔╝
   *    ╚═╝╚═╝  ╚═══╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
   *
   */

  /** @see Result */

  class $Invalid extends Result {
    constructor(message) {
      super();
      this.message = message;
    }

    equals(other) {
      return other instanceof Invalid && this.message === other.message;
    }

    map(fn) {
      return this.cast();
    }

    chain(fn) {
      return this.cast();
    }

    match(arms) {
      return arms.invalid(this.message);
    }

    unwrap() {
      throw new Error("Attempted to unwrap an Invalid result");
    }
    /** Casts this `Invalid<T>` instance to `Invalid<U>`. */


    cast() {
      // `Invalid` results always store strings so this is safe
      return this;
    }

  }
  /* The following code is dark magic to make `Invalid` a proxy for the `$Invalid`
   * class that can also be constructed without using the `new` keyword. */

  /** @see Result */

  /** @see Result */


  const Invalid = exports.Invalid = new Proxy($Invalid, {
    apply: (_, __, [message]) => new $Invalid(message)
  });
  /***
   *    ██████╗ ██╗      █████╗ ███╗   ██╗██╗  ██╗
   *    ██╔══██╗██║     ██╔══██╗████╗  ██║██║ ██╔╝
   *    ██████╔╝██║     ███████║██╔██╗ ██║█████╔╝
   *    ██╔══██╗██║     ██╔══██║██║╚██╗██║██╔═██╗
   *    ██████╔╝███████╗██║  ██║██║ ╚████║██║  ██╗
   *    ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝
   *
   */

  class $Blank extends Result {
    equals(other) {
      return other instanceof Blank;
    }

    map(fn) {
      return this.cast();
    }

    chain(fn) {
      return this.cast();
    }

    match(arms) {
      return arms.blank();
    }

    unwrap() {
      throw new Error("Attempted to unwrap a Blank result");
    }
    /** Casts this `Blank<T>` instance to `Blank<U>`. */


    cast() {
      // `Blank` results always store strings so this is safe
      return this;
    }

  }
  /** There is only one possible `Blank()` result so we can store an instance
   * for it and then always return that instance in the `Blank` constructor. */


  const blankSingletonInstance = new $Blank();
  /* The following code is dark magic to make `Blank` a proxy for the `$Blank`
   * class that can also be constructed without using the `new` keyword. */

  /** @see Result */

  /** @see Result */

  const Blank = exports.Blank = new Proxy($Blank, {
    apply: () => blankSingletonInstance // equivalent to new $Blank()

  });
});
//# sourceMappingURL=Result.js.map