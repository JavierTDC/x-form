import { ReactElement } from 'react';
import { Result } from '../Result';
import _ from 'lodash';
import { Field } from './Field';

export interface FieldConfig<S, T> {
  /** The label that will be shown to the user, generally above the field, to
   * indicate what the field should be filled with.
   * Example: "Last name" */
  readonly label?: string | ReactElement;

  /** If true, the field contents can't be modified by the user, but the app
   * can still change the field value. */
  readonly readOnly?: boolean;

  /** If present on mount, the field will start having this input filled by
   * default. Changing the value of this afterwards will have no effect. */
  readonly defaultInput?: S;

  /** If present on mount, the field will start having this value filled by
   * default. Changing this value afterwards will have no effect. */
  readonly defaultValue?: T;

  /** Allows to override how the field input value (generally a string) is
   * parsed into a value of the desired field type.
   *
   * Example:
   * ```
   * // in a NumberField
   * parse(input: string): Result<number> {
   *   const value = parseInt(input);
   *   if (value.trim().length === 0) return Blank();
   *   if (Number.isNaN(value)) return Invalid("not a number");
   *   return Valid(value);
   * }
   * ``` */
  parse?(input: S): Result<T>;

  /** Allows to define a custom validation function for this field. */
  validate?(value: T): Result<T>;

  /** Allows to define a custom formatter function for this field. */
  format?(value: T): S;

  /** Allows to define a custom render function for this field. */
  render?(field: Field<S, T>): ReactElement;

  /** Allows to override the error message when the field has not been
   * filled. */
  readonly requiredFieldMessage?: string;

  readonly xType?: string;
}