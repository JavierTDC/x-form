(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "@testing-library/react-hooks", "./common"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("@testing-library/react-hooks"), require("./common"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.reactHooks, global.common);
    global.undefined = mod.exports;
  }
})(this, function (exports, _reactHooks, _common) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.testBlankField = testBlankField;
  exports.testInvalidField = testInvalidField;
  exports.testValidField = testValidField;

  function testBlankField(kind, useField, expectedValid) {
    it(spell(`should allow a ${kind} field to be blank`), () => {
      const field = (0, _common.getter)(useField);
      expect(field().valid).toBe(expectedValid);
    });
  }

  function testInvalidField(kind, useField, invalidValue) {
    it(spell(`should not allow a ${kind} field to have an invalid value`), () => {
      const field = (0, _common.getter)(useField);
      (0, _reactHooks.act)(() => field().setValue(invalidValue));
      expect(field().valid).toBe(false);
    });
  }

  function testValidField(kind, useField, validValue) {
    it(spell(`should allow a ${kind} field to have a valid value`), () => {
      const field = (0, _common.getter)(useField);
      (0, _reactHooks.act)(() => field().setValue(validValue));
      expect(field().valid).toBe(true);
    });
  }

  function spell(brokenEnglish) {
    return brokenEnglish.replace('a optional', 'an optional');
  }
});
//# sourceMappingURL=optional.js.map