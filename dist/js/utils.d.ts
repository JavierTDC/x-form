import { Field } from "./useField";
export declare function isBlank(s: string): boolean;
export declare function useConstant<T>(factory: () => T): T;
export declare function scrollToField(field: Field<any>): void;
export declare function scrollToTop(): void;
