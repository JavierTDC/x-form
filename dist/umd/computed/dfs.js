(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "immutable"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("immutable"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.immutable);
    global.undefined = mod.exports;
  }
})(this, function (exports, _immutable) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.dfs = dfs;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  class DfsState {
    constructor(colors) {
      this.colors = colors;
    }

    static empty() {
      return new DfsState((0, _immutable.Map)());
    }

    set(u, color) {
      this.colors = this.colors.set(u, color);
    }

    get(u) {
      return this.colors.get(u, "unvisited");
    }

  }

  function dfs(params) {
    dfsRecursion({
      graph: params.graph,
      state: DfsState.empty(),
      currentNode: params.sourceNode,
      callback: params.callback
    });
  }

  function dfsRecursion(params) {
    const {
      graph,
      state,
      currentNode,
      callback
    } = params;
    state.set(currentNode, "visiting");
    callback(currentNode);
    graph.neighbors(currentNode).forEach(neighNode => {
      if (state.get(neighNode) === "unvisited") {
        dfsRecursion(_objectSpread(_objectSpread({}, params), {}, {
          currentNode: neighNode
        }));
      }
    });
    state.set(currentNode, "visited");
  }
});
//# sourceMappingURL=dfs.js.map