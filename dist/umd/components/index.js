(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./Rut", "./AmountUF", "./Phone"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./Rut"), require("./AmountUF"), require("./Phone"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.Rut, global.AmountUF, global.Phone);
    global.undefined = mod.exports;
  }
})(this, function (exports, _Rut, _AmountUF, _Phone) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.keys(_Rut).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
      enumerable: true,
      get: function () {
        return _Rut[key];
      }
    });
  });
  Object.keys(_AmountUF).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
      enumerable: true,
      get: function () {
        return _AmountUF[key];
      }
    });
  });
  Object.keys(_Phone).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
      enumerable: true,
      get: function () {
        return _Phone[key];
      }
    });
  });
});
//# sourceMappingURL=index.js.map