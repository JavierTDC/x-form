function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { createContext } from "react";
import { useXGlobalConfig } from "./useXGlobalConfig";
import autoBind from "auto-bind";
export class DefaultFormTheme {
  constructor(config) {
    autoBind(this);
    this.config = config;
    this.render = {
      Text: this.Field,
      Checkbox: this.Field,
      Number: this.Field,
      Date: this.Field,
      Select: this.Select,
      Radio: this.Radio
    };
  }

  Field(field) {
    return /*#__PURE__*/React.createElement(this.FieldContainer, {
      field: field
    }, /*#__PURE__*/React.createElement(this.Label, {
      field: field
    }), /*#__PURE__*/React.createElement(this.Input, {
      field: field
    }), /*#__PURE__*/React.createElement(this.Error, {
      field: field
    }));
  }

  Select(field) {
    return /*#__PURE__*/React.createElement(this.FieldContainer, {
      field: field
    }, /*#__PURE__*/React.createElement(this.Label, {
      field: field
    }), /*#__PURE__*/React.createElement(this.SelectContainer, {
      field: field
    }, field.options.map(option => /*#__PURE__*/React.createElement("option", _extends({
      key: option.value,
      className: this.config?.classNames?.option
    }, option.props), option.label))), /*#__PURE__*/React.createElement(this.Error, {
      field: field
    }));
  }

  Radio(field) {
    return /*#__PURE__*/React.createElement(this.FieldContainer, {
      field: field
    }, /*#__PURE__*/React.createElement(this.Label, {
      field: field
    }), /*#__PURE__*/React.createElement(this.RadioOptions, {
      field: field
    }), /*#__PURE__*/React.createElement(this.Error, {
      field: field
    }));
  }

  RadioOptions(props) {
    const field = props.field;
    return /*#__PURE__*/React.createElement(React.Fragment, null, field.options.map(option => /*#__PURE__*/React.createElement(this.RadioOptionContainer, {
      field: field,
      key: option.value
    }, /*#__PURE__*/React.createElement(this.RadioOption, {
      field: field,
      option: option
    }))));
  }

  RadioOptionContainer(props) {
    return /*#__PURE__*/React.createElement("span", {
      className: this.config?.classNames?.radioOption
    }, props.children);
  }

  RadioOption(props) {
    const {
      label,
      inputProps
    } = props.option;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("input", _extends({
      className: this.config?.classNames?.input
    }, inputProps)), /*#__PURE__*/React.createElement("label", {
      className: this.config?.classNames?.label,
      htmlFor: inputProps.id
    }, label));
  }

  FieldContainer(props) {
    return /*#__PURE__*/React.createElement("div", {
      className: this.config?.classNames?.field,
      ref: props.field.ref
    }, props.children);
  }

  SelectContainer(props) {
    return /*#__PURE__*/React.createElement("select", _extends({
      className: this.config?.classNames?.select
    }, props.field.inputProps), props.children);
  }

  Label(props) {
    return /*#__PURE__*/React.createElement(this.LabelContainer, {
      field: props.field
    }, props.field.config.label, props.field.config.label && props.field.config.optional && /*#__PURE__*/React.createElement(this.Optional, {
      field: props.field
    }));
  }

  LabelContainer(props) {
    return /*#__PURE__*/React.createElement("label", {
      className: this.config?.classNames?.label,
      htmlFor: props.field.inputProps?.id ?? props.field.config.name
    }, props.children);
  }

  Optional(_props) {
    const xGlobalConfig = useXGlobalConfig();
    return /*#__PURE__*/React.createElement(React.Fragment, null, " (", xGlobalConfig.strings.optional, ")");
  }

  Input(props) {
    return /*#__PURE__*/React.createElement("input", _extends({
      className: this.config?.classNames?.input
    }, props.field.inputProps, {
      ref: props.field.inputRef
    }));
  }

  Error(props) {
    return /*#__PURE__*/React.createElement(React.Fragment, null, props.field.shouldShowError && /*#__PURE__*/React.createElement(this.TextError, {
      field: props.field
    }));
  }

  TextError(props) {
    return /*#__PURE__*/React.createElement("p", {
      style: {
        color: 'red'
      }
    }, props.field.validationResult);
  }

}
export const FormThemeContext = /*#__PURE__*/createContext(new DefaultFormTheme());
//# sourceMappingURL=FormTheme.js.map