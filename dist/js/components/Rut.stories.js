"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RutWithOnChange = exports.RutWithDottedFormat = exports.RutWithInitialState = exports.RutWithOnValid = exports.default = void 0;

var React = _interopRequireWildcard(require("react"));

var _Rut = require("./Rut");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = {
  title: 'Rut',
  component: _Rut.Rut
};
exports.default = _default;

var Template = function Template(args) {
  return /*#__PURE__*/React.createElement(_Rut.Rut, args);
};

var RutWithOnValid = Template.bind({});
exports.RutWithOnValid = RutWithOnValid;
RutWithOnValid.args = {
  onValid: function onValid(rut) {
    return alert('This RUT is valid: ' + rut);
  }
};
var RutWithInitialState = Template.bind({});
exports.RutWithInitialState = RutWithInitialState;
RutWithInitialState.args = {
  rut: '12.312.312-3'
};
var RutWithDottedFormat = Template.bind({});
exports.RutWithDottedFormat = RutWithDottedFormat;
RutWithDottedFormat.args = {
  format: 'dashed'
};
var RutWithOnChange = Template.bind({});
exports.RutWithOnChange = RutWithOnChange;
RutWithOnChange.args = {
  onRutChange: function onRutChange(rut, isValid) {
    console.log('rut', rut, isValid);
  }
};
//# sourceMappingURL=Rut.stories.js.map