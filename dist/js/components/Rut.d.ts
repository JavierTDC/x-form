import * as React from 'react';
export declare type RutProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
    rut?: string;
    format?: 'dotted' | 'dashed';
    onValid?(rut: string): void;
    onRutChange?(rut: string, isValid: boolean): void;
};
export declare function Rut({ onValid, rut: initialState, format, ...props }: RutProps): React.ReactElement;
