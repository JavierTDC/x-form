import React, { DetailedHTMLProps, FormEvent, FormHTMLAttributes, ReactElement, useState } from 'react';
import {Field} from "./useField";
import {useXGlobalConfig} from "./useXGlobalConfig";
import {scrollToField, scrollToTop} from "./utils";
import _ from "lodash";

export type FormValues = {
  readonly [fieldName: string]: any;
};

export type FormFields<T extends FormValues> = {
  [P in keyof T]: Field<T[P]>;
};

export type FormProps = DetailedHTMLProps<FormHTMLAttributes<HTMLFormElement>, HTMLFormElement>;

export interface FormConfig<T extends FormValues> {
  readonly fields: FormFields<T>;

  /** The props to be passed to the <form> element. */
  readonly props?: FormProps;

  onSubmitWithValidValues(values: T): Promise<void>;

  validate?(values: T): string;  // custom validation to apply additionally to field validations

  render?(fields: FormFields<T>): ReactElement;  // custom render function
}

function checkFormConfig<T extends FormValues>(config: FormConfig<T>): void {
  if (_.isEmpty(config.fields)) throw new Error("Form with no fields");

  _.forEach(config.fields, (field, key) => {
    if (field.config.name !== key) {
      throw new Error(`In the "fields" object you passed to useForm, the field at "${key}" ` +
          `has a different name "${field.config.name}".`);
    }
  });
}

export interface Form<T> {
  // meta
  config: FormConfig<T>;

  /** A dictionary with each of the field objects corresponding to this form.
   *
   * Those field objects then allow you to do useful operations on each field.
   *
   * For example, if your form has a "firstName" field, then you can do
   * `form.fields.firstName.render()`, `form.fields.firstName.value`, etc.
   * */
  fields: FormFields<T>;

  /** The props to be passed to the <form> element. */
  props: FormProps;

  // state
  values: T;
  setValues(values: T): void;

  isValid: boolean;
  isSubmitting: boolean;
  submit(): Promise<void>;
  reset(): void;

  render(): ReactElement;
}

export function useForm<T extends FormValues>(config: FormConfig<T>): Form<T> {
  checkFormConfig(config);
  const xGlobalConfig = useXGlobalConfig();

  // there can be ignored and/or hidden fields here
  const allFields = config.fields;

  // here is where we ignore the ignored fields
  const consideredFields = _.omitBy(allFields, ({config}) =>
      // removed fields are ignored by definition
      config.ignored || config.removed
  );

  // those will not have the hidden fields
  const shownFields = _.omitBy(allFields, ({config}) =>
      // removed fields are hidden by definition
      config.hidden || config.removed
  );

  // convert the field values to the FormValues dictionary representation
  const values = _.mapValues(consideredFields, "value") as T;

  // on a long form you may want to scroll to the first invalid field on submit
  const firstInvalidField = Object.values(shownFields)
      .find(field => !field.valid);

  // consider making useValidator able to handle this as well to avoid duplication
  function validate(): string {
    if (firstInvalidField != null) {
      return xGlobalConfig.strings.invalidForm;
    }

    return config.validate?.(values) ?? "OK";
  }
  const validationResult = validate();
  const isValid = validationResult === "OK";

  const [isSubmitting, setIsSubmitting] = useState(false);

  function render(): ReactElement {
    return config.render?.(config.fields) ?? (
        <form {...config.props}>
          {_.map(config.fields, (field, name) => (
              <React.Fragment key={name}>
                {field.render()}
              </React.Fragment>
          ))}
        </form>
    );
  }

  return {
    config,

    fields: config.fields,
    props: {
      ...config.props,
      onSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();
        config.props?.onSubmit?.(event);
      },
    },

    values,
    setValues(values: FormValues): void {
      Object.entries(values).forEach(([name, value]) => {
        config.fields[name].setValue(value);
      });
    },

    isValid,
    isSubmitting,
    async submit(): Promise<void> {
      setIsSubmitting(true);
      try {
        _.forEach(consideredFields, field => field.onSubmit());

        if (isValid) {
          await config.onSubmitWithValidValues(values);
        } else if (firstInvalidField != null) {
          // in future versions the user should be choosing the behavior here
          scrollToField(firstInvalidField);
        } else {
          // and here
          scrollToTop();
        }
      } finally {
        setIsSubmitting(false);
      }
    },

    reset(): void {
      _.forEach(allFields, field => field.reset());
    },

    render,
  };
}