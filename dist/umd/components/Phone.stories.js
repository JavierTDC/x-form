(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "./Phone"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("./Phone"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.Phone);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _Phone) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.PhoneNormal = undefined;

  var React = _interopRequireWildcard(_react);

  function _getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();

    _getRequireWildcardCache = function () {
      return cache;
    };

    return cache;
  }

  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    }

    if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
      return {
        default: obj
      };
    }

    var cache = _getRequireWildcardCache();

    if (cache && cache.has(obj)) {
      return cache.get(obj);
    }

    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

        if (desc && (desc.get || desc.set)) {
          Object.defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }

    newObj.default = obj;

    if (cache) {
      cache.set(obj, newObj);
    }

    return newObj;
  }

  exports.default = {
    title: 'Phone',
    component: _Phone.Phone
  };

  const Template = args => /*#__PURE__*/React.createElement(_Phone.Phone, args);

  const PhoneNormal = exports.PhoneNormal = Template.bind({});
  PhoneNormal.args = {
    format: 'mobile'
  };
});
//# sourceMappingURL=Phone.stories.js.map