(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "lodash", "./Field", "../Result"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("lodash"), require("./Field"), require("../Result"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.lodash, global.Field, global.Result);
    global.undefined = mod.exports;
  }
})(this, function (exports, _lodash, _Field, _Result) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.FieldBuilder = undefined;

  var _lodash2 = _interopRequireDefault(_lodash);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  class FieldBuilder {
    constructor(config) {
      this.config = config;
      this.validateConfig(config);
    }

    validateConfig(config) {
      if (config.defaultInput != null && config.defaultValue != null) {
        throw new Error("defaultInput and defaultValue can't be both provided at the same time");
      }
    }

    with(config) {
      return this.new(_lodash2.default.merge({}, this.config, config));
    }

    build(name) {
      const logic = this.buildFieldLogic();
      const initialInput = logic.initialInput;
      const state = (0, _Field.useFieldState)(initialInput);
      return this.buildFromState(name, state);
    }

    defaultValidator(value) {
      return (0, _Result.Valid)(value);
    }

  }

  exports.FieldBuilder = FieldBuilder;
});
//# sourceMappingURL=FieldBuilder.js.map