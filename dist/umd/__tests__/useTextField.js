(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["../field-types/useTextField", "../test-utils/optional"], factory);
  } else if (typeof exports !== "undefined") {
    factory(require("../field-types/useTextField"), require("../test-utils/optional"));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.useTextField, global.optional);
    global.undefined = mod.exports;
  }
})(this, function (_useTextField, _optional) {
  "use strict";

  const name = 'test';

  function validate(value) {
    if (!value.includes('@')) return 'Invalid email';
    return 'OK';
  }

  function validateRedundantly(value) {
    if (value.trim().length === 0) return 'Required field';
    return validate(value);
  }

  const invalidValue = 'not an email';
  const validValue = 'test@example.com';
  const hooks = {
    optional: () => (0, _useTextField.useTextField)({
      name,
      optional: true,
      validate: validateRedundantly
    }),
    required: () => (0, _useTextField.useTextField)({
      name,
      validate
    })
  };
  describe('useTextField', () => {
    (0, _optional.testBlankField)('optional', hooks.optional, true);
    (0, _optional.testBlankField)('required', hooks.required, false);
    (0, _optional.testInvalidField)('optional', hooks.optional, invalidValue);
    (0, _optional.testInvalidField)('required', hooks.required, invalidValue);
    (0, _optional.testValidField)('optional', hooks.optional, validValue);
    (0, _optional.testValidField)('required', hooks.required, validValue);
  });
});
//# sourceMappingURL=useTextField.js.map