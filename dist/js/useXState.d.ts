import { StateRef } from './useStateRef';
export interface XState<T> extends StateRef<T> {
    value: T;
    initialValue: T;
    resetValue(): void;
}
export declare function useXState<T>(initialValue: T): XState<T>;
