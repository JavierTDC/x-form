(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./Field", "react", "../utils", "../Result", "./FieldBuilder"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./Field"), require("react"), require("../utils"), require("../Result"), require("./FieldBuilder"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.Field, global.react, global.utils, global.Result, global.FieldBuilder);
    global.undefined = mod.exports;
  }
})(this, function (exports, _Field, _react, _utils, _Result, _FieldBuilder) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.TextField = exports.TextFieldBuilder = undefined;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  class TextFieldLogic extends _Field.FieldLogic {
    constructor(config) {
      super(config);

      _defineProperty(this, "blankInput", "");

      this.config = config;
    }

    isBlank(input) {
      return (0, _utils.isBlank)(input);
    }

    defaultParser(input) {
      return (0, _Result.Valid)(input);
    }

    defaultFormatter(value) {
      return value;
    }

  }

  class TextFieldBuilder extends _FieldBuilder.FieldBuilder {
    constructor(config) {
      super(config);
      this.config = config;
    }

    new(config) {
      return new TextFieldBuilder(config);
    }

    buildFieldLogic() {
      return new TextFieldLogic(this.config);
    }

    buildFromState(name, state) {
      const config = this.config;
      const logic = this.buildFieldLogic();
      const ref = (0, _react.useRef)(null);
      const inputRef = (0, _react.useRef)(null);
      return new TextField(config, logic, state, ref, inputRef);
    }

    getInputProps(name, state) {
      const config = this.config;

      function onChange(event) {
        state.inputState.setValue(event.target.value);
        config.inputProps?.onChange?.(event);
      }

      function onFocus(event) {
        state.setIsFocused(true);
        config.inputProps?.onFocus?.(event);
      }

      function onBlur(event) {
        state.setIsFocused(false);
        state.setHasBeenBlurred(true);
        config.inputProps?.onBlur?.(event);
      }

      return _objectSpread(_objectSpread({
        type: 'text',
        id: name,
        name
      }, this.config.inputProps), {}, {
        value: state.inputState.reactiveValue,
        onChange,
        onFocus,
        onBlur
      });
    }

  }

  exports.TextFieldBuilder = TextFieldBuilder;

  class TextField extends _Field.Field {
    constructor(config, logic, state, ref, inputRef) {
      super(config, logic, state, ref);

      _defineProperty(this, "xType", "TextField");

      this.config = config;
      this.logic = logic;
      this.state = state;
      this.ref = ref;
      this.inputRef = inputRef;
    }

  }

  exports.TextField = TextField;
});
//# sourceMappingURL=TextField.js.map