import _ from "lodash";
import {NumberField} from "../field-types/useNumberField";
import {isBlank} from "../utils";

export abstract class Functor<T> {
  abstract get type(): string;

  static map<X, Y>(
      fn: (...args: X[]) => Y
  ): (...args: Functor<X>[]) => Functor<Y> {
    return (...args: Functor<X>[]): Functor<Y> => {
      if (args.every(x => x instanceof Valid)) return new Valid(fn(..._.map(args, "value")));
      if (args.some(x => x instanceof Invalid)) return invalid;
      return blank;
    }
  }
}

export class Blank<T> extends Functor<T> { type = "Blank" }
export class Invalid<T> extends Functor<T> { type = "Invalid" }
export class Valid<T> extends Functor<T> {
  type = "Valid";
  readonly value: T;
  constructor(value: T) {
    super();
    this.value = value;
  }
}
const blank = new Blank();
const invalid = new Invalid();

export function asFunctor(field: NumberField): Functor<number> {
  const numberValue = field.getLatestValue();
  const stringValue = field.innerField.getLatestValue();
  const validate = field.innerField.validate;

  if (validate(stringValue) === "OK") return new Valid(numberValue);
  if (isBlank(stringValue)) return blank;
  return invalid;
}