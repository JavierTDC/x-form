"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useDateField = useDateField;

var _jsJoda = require("js-joda");

var _useHtmlInput = require("./useHtmlInput");

var _useXGlobalConfig = require("../useXGlobalConfig");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function useDateField(config) {
  var _config$xType, _config$defaultValue;

  var xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
  var innerField = (0, _useHtmlInput.useHtmlInput)(_objectSpread(_objectSpread({}, config), {}, {
    xType: (_config$xType = config.xType) !== null && _config$xType !== void 0 ? _config$xType : "Date",
    defaultValue: (_config$defaultValue = config.defaultValue) === null || _config$defaultValue === void 0 ? void 0 : _config$defaultValue.toString(),
    shouldValidate: config.shouldValidate,
    render: config.render,
    inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
      type: "date"
    }),
    validate: function validate(value) {
      var _config$minAge;

      var localDate = parseLocalDate(value);

      if (localDate == null) {
        return xGlobalConfig.strings.invalidDate;
      }

      var age = localDate.until(_jsJoda.LocalDate.now()).years();

      if (age < ((_config$minAge = config.minAge) !== null && _config$minAge !== void 0 ? _config$minAge : -Infinity)) {
        return xGlobalConfig.strings.tooYoung(config.minAge);
      }

      return "OK";
    }
  }), {
    resetExtraState: function resetExtraState() {}
  });
  return _objectSpread(_objectSpread({}, innerField), {}, {
    config: config,
    initialValue: parseLocalDate(innerField.initialValue),
    value: parseLocalDate(innerField.value),
    reactiveValue: parseLocalDate(innerField.reactiveValue),
    setValue: function setValue(action) {
      if (typeof action === "function") {
        innerField.setValue(function (prev) {
          return action(parseLocalDate(prev)).toString();
        });
      } else {
        innerField.setValue(action.toString());
      }
    },
    getLatestValue: function getLatestValue() {
      return parseLocalDate(innerField.getLatestValue());
    },
    validate: function validate(value) {
      return innerField.validate(value.toString());
    }
  });
}

function parseLocalDate(value) {
  try {
    return _jsJoda.LocalDate.parse(value);
  } catch (error) {
    if (error instanceof _jsJoda.DateTimeParseException) return null;else throw error;
  }
}
//# sourceMappingURL=useDateField.js.map