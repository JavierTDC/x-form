import { Graph } from "./Graph";
declare type DfsParams<Node> = {
    graph: Graph<Node>;
    sourceNode: Node;
    callback: (currentNode: Node) => void;
};
export declare function dfs<Node>(params: DfsParams<Node>): void;
export {};
