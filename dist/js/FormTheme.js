"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormThemeContext = exports.DefaultFormTheme = void 0;

var _react = _interopRequireWildcard(require("react"));

var _useXGlobalConfig = require("./useXGlobalConfig");

var _autoBind = _interopRequireDefault(require("auto-bind"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DefaultFormTheme = /*#__PURE__*/function () {
  function DefaultFormTheme(config) {
    _classCallCheck(this, DefaultFormTheme);

    (0, _autoBind.default)(this);
    this.config = config;
    this.render = {
      Text: this.Field,
      Checkbox: this.Field,
      Number: this.Field,
      Date: this.Field,
      Select: this.Select,
      Radio: this.Radio
    };
  }

  _createClass(DefaultFormTheme, [{
    key: "Field",
    value: function (_Field) {
      function Field(_x) {
        return _Field.apply(this, arguments);
      }

      Field.toString = function () {
        return _Field.toString();
      };

      return Field;
    }(function (field) {
      return /*#__PURE__*/_react.default.createElement(this.FieldContainer, {
        field: field
      }, /*#__PURE__*/_react.default.createElement(this.Label, {
        field: field
      }), /*#__PURE__*/_react.default.createElement(this.Input, {
        field: field
      }), /*#__PURE__*/_react.default.createElement(this.Error, {
        field: field
      }));
    })
  }, {
    key: "Select",
    value: function Select(field) {
      var _this = this;

      return /*#__PURE__*/_react.default.createElement(this.FieldContainer, {
        field: field
      }, /*#__PURE__*/_react.default.createElement(this.Label, {
        field: field
      }), /*#__PURE__*/_react.default.createElement(this.SelectContainer, {
        field: field
      }, field.options.map(function (option) {
        var _this$config, _this$config$classNam;

        return /*#__PURE__*/_react.default.createElement("option", _extends({
          key: option.value,
          className: (_this$config = _this.config) === null || _this$config === void 0 ? void 0 : (_this$config$classNam = _this$config.classNames) === null || _this$config$classNam === void 0 ? void 0 : _this$config$classNam.option
        }, option.props), option.label);
      })), /*#__PURE__*/_react.default.createElement(this.Error, {
        field: field
      }));
    }
  }, {
    key: "Radio",
    value: function Radio(field) {
      return /*#__PURE__*/_react.default.createElement(this.FieldContainer, {
        field: field
      }, /*#__PURE__*/_react.default.createElement(this.Label, {
        field: field
      }), /*#__PURE__*/_react.default.createElement(this.RadioOptions, {
        field: field
      }), /*#__PURE__*/_react.default.createElement(this.Error, {
        field: field
      }));
    }
  }, {
    key: "RadioOptions",
    value: function RadioOptions(props) {
      var _this2 = this;

      var field = props.field;
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, field.options.map(function (option) {
        return /*#__PURE__*/_react.default.createElement(_this2.RadioOptionContainer, {
          field: field,
          key: option.value
        }, /*#__PURE__*/_react.default.createElement(_this2.RadioOption, {
          field: field,
          option: option
        }));
      }));
    }
  }, {
    key: "RadioOptionContainer",
    value: function RadioOptionContainer(props) {
      var _this$config2, _this$config2$classNa;

      return /*#__PURE__*/_react.default.createElement("span", {
        className: (_this$config2 = this.config) === null || _this$config2 === void 0 ? void 0 : (_this$config2$classNa = _this$config2.classNames) === null || _this$config2$classNa === void 0 ? void 0 : _this$config2$classNa.radioOption
      }, props.children);
    }
  }, {
    key: "RadioOption",
    value: function RadioOption(props) {
      var _this$config3, _this$config3$classNa, _this$config4, _this$config4$classNa;

      var _props$option = props.option,
          label = _props$option.label,
          inputProps = _props$option.inputProps;
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("input", _extends({
        className: (_this$config3 = this.config) === null || _this$config3 === void 0 ? void 0 : (_this$config3$classNa = _this$config3.classNames) === null || _this$config3$classNa === void 0 ? void 0 : _this$config3$classNa.input
      }, inputProps)), /*#__PURE__*/_react.default.createElement("label", {
        className: (_this$config4 = this.config) === null || _this$config4 === void 0 ? void 0 : (_this$config4$classNa = _this$config4.classNames) === null || _this$config4$classNa === void 0 ? void 0 : _this$config4$classNa.label,
        htmlFor: inputProps.id
      }, label));
    }
  }, {
    key: "FieldContainer",
    value: function FieldContainer(props) {
      var _this$config5, _this$config5$classNa;

      return /*#__PURE__*/_react.default.createElement("div", {
        className: (_this$config5 = this.config) === null || _this$config5 === void 0 ? void 0 : (_this$config5$classNa = _this$config5.classNames) === null || _this$config5$classNa === void 0 ? void 0 : _this$config5$classNa.field,
        ref: props.field.ref
      }, props.children);
    }
  }, {
    key: "SelectContainer",
    value: function SelectContainer(props) {
      var _this$config6, _this$config6$classNa;

      return /*#__PURE__*/_react.default.createElement("select", _extends({
        className: (_this$config6 = this.config) === null || _this$config6 === void 0 ? void 0 : (_this$config6$classNa = _this$config6.classNames) === null || _this$config6$classNa === void 0 ? void 0 : _this$config6$classNa.select
      }, props.field.inputProps), props.children);
    }
  }, {
    key: "Label",
    value: function Label(props) {
      return /*#__PURE__*/_react.default.createElement(this.LabelContainer, {
        field: props.field
      }, props.field.config.label, props.field.config.label && props.field.config.optional && /*#__PURE__*/_react.default.createElement(this.Optional, {
        field: props.field
      }));
    }
  }, {
    key: "LabelContainer",
    value: function LabelContainer(props) {
      var _this$config7, _this$config7$classNa, _inputProps$id, _inputProps;

      return /*#__PURE__*/_react.default.createElement("label", {
        className: (_this$config7 = this.config) === null || _this$config7 === void 0 ? void 0 : (_this$config7$classNa = _this$config7.classNames) === null || _this$config7$classNa === void 0 ? void 0 : _this$config7$classNa.label,
        htmlFor: (_inputProps$id = (_inputProps = props.field.inputProps) === null || _inputProps === void 0 ? void 0 : _inputProps.id) !== null && _inputProps$id !== void 0 ? _inputProps$id : props.field.config.name
      }, props.children);
    }
  }, {
    key: "Optional",
    value: function Optional(_props) {
      var xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, " (", xGlobalConfig.strings.optional, ")");
    }
  }, {
    key: "Input",
    value: function Input(props) {
      var _this$config8, _this$config8$classNa;

      return /*#__PURE__*/_react.default.createElement("input", _extends({
        className: (_this$config8 = this.config) === null || _this$config8 === void 0 ? void 0 : (_this$config8$classNa = _this$config8.classNames) === null || _this$config8$classNa === void 0 ? void 0 : _this$config8$classNa.input
      }, props.field.inputProps, {
        ref: props.field.inputRef
      }));
    }
  }, {
    key: "Error",
    value: function Error(props) {
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, props.field.shouldShowError && /*#__PURE__*/_react.default.createElement(this.TextError, {
        field: props.field
      }));
    }
  }, {
    key: "TextError",
    value: function TextError(props) {
      return /*#__PURE__*/_react.default.createElement("p", {
        style: {
          color: 'red'
        }
      }, props.field.validationResult);
    }
  }]);

  return DefaultFormTheme;
}();

exports.DefaultFormTheme = DefaultFormTheme;
var FormThemeContext = /*#__PURE__*/(0, _react.createContext)(new DefaultFormTheme());
exports.FormThemeContext = FormThemeContext;
//# sourceMappingURL=FormTheme.js.map