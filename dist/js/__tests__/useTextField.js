"use strict";

var _useTextField = require("../field-types/useTextField");

var _optional = require("../test-utils/optional");

var name = 'test';

function validate(value) {
  if (!value.includes('@')) return 'Invalid email';
  return 'OK';
}

function validateRedundantly(value) {
  if (value.trim().length === 0) return 'Required field';
  return validate(value);
}

var invalidValue = 'not an email';
var validValue = 'test@example.com';
var hooks = {
  optional: function optional() {
    return (0, _useTextField.useTextField)({
      name: name,
      optional: true,
      validate: validateRedundantly
    });
  },
  required: function required() {
    return (0, _useTextField.useTextField)({
      name: name,
      validate: validate
    });
  }
};
describe('useTextField', function () {
  (0, _optional.testBlankField)('optional', hooks.optional, true);
  (0, _optional.testBlankField)('required', hooks.required, false);
  (0, _optional.testInvalidField)('optional', hooks.optional, invalidValue);
  (0, _optional.testInvalidField)('required', hooks.required, invalidValue);
  (0, _optional.testValidField)('optional', hooks.optional, validValue);
  (0, _optional.testValidField)('required', hooks.required, validValue);
});
//# sourceMappingURL=useTextField.js.map