import { createContext } from "react";

export type RecursivePartial<T> = {
  [P in keyof T]?: T[P] extends (infer U)[]
    ? RecursivePartial<U>[]
    : T[P] extends object
    ? RecursivePartial<T[P]>
    : T[P];
};

export interface XFormConfig {
  readonly strings: {
    requiredField: string;
    optional: string;
    invalidForm: string;
    blankSelectField: string;

    invalidNumber: string;
    negativeNumber: string;
    numberTooSmall(minValue: number): string;
    numberTooBig(maxValue: number): string;
    tooManyDecimals: string;
    shouldBeInteger: string;
    cantBeZero: string;

    invalidDate: string;
    tooYoung(minAge: number): string;
  };
}

export const defaultXFormConfig = {
  strings: {
    requiredField: "The field is required",
    optional: "Optional",
    invalidForm: "There are fields with errors",
    blankSelectField: "--- Select an option ---",

    invalidNumber: "The number is invalid",
    negativeNumber: "The number can't be negative",
    numberTooSmall: (minValue: number) => `Must be at least ${minValue}`,
    numberTooBig: (maxValue: number) => `Must be at most ${maxValue}`,
    tooManyDecimals: "Too many decimals",
    shouldBeInteger: "Must be an integer",
    cantBeZero: "Can't be zero",

    invalidDate: "Invalid date",
    tooYoung: (minAge: number) => `Must be older than ${minAge}`,
  },
};

export const XFormContext = createContext<RecursivePartial<XFormConfig>>({});
