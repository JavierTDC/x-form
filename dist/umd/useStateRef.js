(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useStateRef = useStateRef;

  function useStateRef(initialState) {
    const [reactiveValue, setReactiveValue] = (0, _react.useState)(initialState);
    const valueRef = (0, _react.useRef)(initialState);
    return {
      reactiveValue,

      setValue(action) {
        if (action instanceof Function) {
          valueRef.current = action(valueRef.current);
        } else {
          valueRef.current = action;
        }

        setReactiveValue(valueRef.current);
      },

      getLatestValue() {
        return valueRef.current;
      }

    };
  }
});
//# sourceMappingURL=useStateRef.js.map