import { List } from "immutable";
export declare class Graph<Node> {
    private adj;
    private constructor();
    static empty<Node>(): Graph<Node>;
    addEdge(u: Node, v: Node): void;
    nodes(): List<Node>;
    neighbors(u: Node): List<Node>;
    edges(): List<[Node, Node]>;
    withReversedEdges(): Graph<Node>;
}
