/***
 *    ██████╗ ███████╗███████╗██╗   ██╗██╗  ████████╗
 *    ██╔══██╗██╔════╝██╔════╝██║   ██║██║  ╚══██╔══╝
 *    ██████╔╝█████╗  ███████╗██║   ██║██║     ██║
 *    ██╔══██╗██╔══╝  ╚════██║██║   ██║██║     ██║
 *    ██║  ██║███████╗███████║╚██████╔╝███████╗██║
 *    ╚═╝  ╚═╝╚══════╝╚══════╝ ╚═════╝ ╚══════╝╚═╝
 *
 */
/** The return type for `parse` and `validate` functions.
 *
 * `Result`s are chainable functors that can store the following results after
 * a field parsing and/or validation:
 * - a `Valid(value)`
 * - an `Invalid(message)` where `message` is the error message shown to the
 * user.
 * - a `Blank()` for fields that have not been filled. */
export declare abstract class Result<T> {
    /** Use this instead of `===` for equality by value. */
    abstract equals(other: any): boolean;
    /** Applies the `fn` function to the value of a `Valid` result, returning
     * another `Valid` result with the transformed value.
     *
     * If applied to a non-`Valid` result it returns that same result unmodified.
     *
     * Examples:
     * - `Valid(42).map(x => x + 1)  // Valid(43)`
     * - `Invalid("Oops").map(x => x + 1)  // Invalid("Oops")`
     * - `Blank().map(x => x + 1)  // Blank()`
     * */
    abstract map<U>(fn: (x: T) => U): Result<U>;
    /** Applies the `fn` function to the value of a `Valid` result, returning the
     * same result that `fn` returned.
     *
     * If applied to a non-`Valid` result it returns that same result unmodified.
     *
     * Examples:
     * - `Valid(42).chain(x => Invalid(x.toString()))  // Invalid("42")`
     * - `Invalid("Oops").chain(x => Invalid(x.toString())) // Invalid("Oops")`
     * - `Blank().chain(x => Invalid(x.toString())) // Blank()`
     * */
    abstract chain<U>(fn: (x: T) => Result<U>): Result<U>;
    /** Syntax sugar that is similar to a `match` statement in
     * a functional language, where the resulting expression depends on the type
     * of the result that `match` is applied to.
     *
     * Example:
     * ```
     * const description = result.match({
     *   valid: (value) => `It is a valid ${value}`,
     *   invalid: (message) => `Invalid with message ${message}`,
     *   blank: () => `A blank`,
     * });
     * ``` */
    abstract match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B;
    /** Intuitively, it takes a function that doesn't work with `Result`s and
     * returns a new function that works with `Result` parameters and return type.
     *
     *
     * Example:
     * ```
     * const originalFn = (a, b, c) => a + b + c;
     * const newFn = Result.fMap(originalFn);
     * newFn(Valid(100), Valid(20), Valid(3))  // Valid(123)
     * newFn(Valid(100), Invalid("Oops"), Invalid("Boom"))  // Invalid("")
     * newFn(Valid(100), Invalid("Oops"), Blank())  // Invalid("")
     * newFn(Valid(100), Blank(), Blank())  // Blank()
     * ```
     *
     * Specifically, if `fn` is a function that receive N parameters of type `X`
     * and returns a value of type `Y`, `fMap` will return a new function that
     * receive N parameters of type `Result<X>` and returns a `Result<Y>`.
     *
     * Then the new function will work as follows:
     * - If all arguments are `Valid`, the result will be `Valid`.
     * - If there is any `Invalid` argument, the result will be `Invalid`.
     * - If there is any `Blank` argument (and no `Invalid`s), the result will be
     * `Blank`.
     * */
    static fMap<X, Y>(fn: VariadicFn<X, Y>): VariadicFn<Result<X>, Result<Y>>;
    /** Gets the value if the result is `Valid` or throws otherwise. */
    abstract unwrap(): T;
}
/** The type for a function that can receive any number of arguments of type `X`
 * and returns a value of type `Y`. */
declare type VariadicFn<X, Y> = (...args: Array<X>) => Y;
/** @see Result#match */
declare type Arms<T, V, I, B> = Readonly<{
    valid: (value: T) => V;
    invalid: (message: string) => I;
    blank: () => B;
}>;
/***
 *    ██╗   ██╗ █████╗ ██╗     ██╗██████╗
 *    ██║   ██║██╔══██╗██║     ██║██╔══██╗
 *    ██║   ██║███████║██║     ██║██║  ██║
 *    ╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
 *     ╚████╔╝ ██║  ██║███████╗██║██████╔╝
 *      ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
 *
 */
/** @see Result */
declare class $Valid<T> extends Result<T> {
    readonly value: T;
    constructor(value: T);
    equals(other: any): boolean;
    map<U>(fn: (x: T) => U): Valid<U>;
    chain<U>(fn: (x: T) => Result<U>): Result<U>;
    match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B;
    unwrap(): T;
}
/** @see Result */
export declare type Valid<T> = $Valid<T>;
/** @see Result */
export declare const Valid: typeof $Valid & (<T>(value: T) => Valid<T>);
/***
 *    ██╗███╗   ██╗██╗   ██╗ █████╗ ██╗     ██╗██████╗
 *    ██║████╗  ██║██║   ██║██╔══██╗██║     ██║██╔══██╗
 *    ██║██╔██╗ ██║██║   ██║███████║██║     ██║██║  ██║
 *    ██║██║╚██╗██║╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
 *    ██║██║ ╚████║ ╚████╔╝ ██║  ██║███████╗██║██████╔╝
 *    ╚═╝╚═╝  ╚═══╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
 *
 */
/** @see Result */
declare class $Invalid<T> extends Result<T> {
    readonly message: string;
    constructor(message: string);
    equals(other: any): boolean;
    map<U>(fn: (x: T) => U): Invalid<U>;
    chain<U>(fn: (x: T) => Result<U>): Result<U>;
    match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B;
    unwrap(): T;
    /** Casts this `Invalid<T>` instance to `Invalid<U>`. */
    cast<U>(): Invalid<U>;
}
/** @see Result */
export declare type Invalid<T> = $Invalid<T>;
/** @see Result */
export declare const Invalid: typeof $Invalid & (<T>(message: string) => Invalid<T>);
/***
 *    ██████╗ ██╗      █████╗ ███╗   ██╗██╗  ██╗
 *    ██╔══██╗██║     ██╔══██╗████╗  ██║██║ ██╔╝
 *    ██████╔╝██║     ███████║██╔██╗ ██║█████╔╝
 *    ██╔══██╗██║     ██╔══██║██║╚██╗██║██╔═██╗
 *    ██████╔╝███████╗██║  ██║██║ ╚████║██║  ██╗
 *    ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝
 *
 */
declare class $Blank<T> extends Result<T> {
    equals(other: any): boolean;
    map<U>(fn: (x: T) => U): Result<U>;
    chain<U>(fn: (x: T) => Result<U>): Result<U>;
    match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B;
    unwrap(): T;
    /** Casts this `Blank<T>` instance to `Blank<U>`. */
    cast<U>(): Blank<U>;
}
/** @see Result */
export declare type Blank<T> = $Blank<T>;
/** @see Result */
export declare const Blank: typeof $Blank & (<T>() => Blank<T>);
export {};
