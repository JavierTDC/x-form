(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.usePreviousStateEffect = usePreviousStateEffect;

  function usePreviousStateEffect(effect, state) {
    const prevStateRef = (0, _react.useRef)(state);
    (0, _react.useEffect)(() => {
      const destructor = effect(prevStateRef.current);
      prevStateRef.current = state;
      return destructor;
    }, [state]);
  }
});
//# sourceMappingURL=usePreviousStateEffect.js.map