import { renderHook } from '@testing-library/react-hooks';

export function getter(hook) {
    const { result } = renderHook(hook);
    return () => result.current;
}