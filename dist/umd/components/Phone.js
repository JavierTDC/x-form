(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "prop-types", "react", "react-number-format"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("prop-types"), require("react"), require("react-number-format"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.propTypes, global.react, global.reactNumberFormat);
    global.undefined = mod.exports;
  }
})(this, function (exports, _propTypes, _react, _reactNumberFormat) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Phone = Phone;

  var _propTypes2 = _interopRequireDefault(_propTypes);

  var React = _interopRequireWildcard(_react);

  var _reactNumberFormat2 = _interopRequireDefault(_reactNumberFormat);

  function _getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();

    _getRequireWildcardCache = function () {
      return cache;
    };

    return cache;
  }

  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    }

    if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
      return {
        default: obj
      };
    }

    var cache = _getRequireWildcardCache();

    if (cache && cache.has(obj)) {
      return cache.get(obj);
    }

    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

        if (desc && (desc.get || desc.set)) {
          Object.defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }

    newObj.default = obj;

    if (cache) {
      cache.set(obj, newObj);
    }

    return newObj;
  }

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};

    var target = _objectWithoutPropertiesLoose(source, excluded);

    var key, i;

    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }

    return target;
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;

    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }

    return target;
  }

  // +56 9 XXXX XXXX -> Móvil
  // +56 2 XXXX XXXX -> Fijo RM
  // +56 YY XXX XXXX -> Fijo Otras regiones
  function Phone(_ref) {
    let {
      format,
      withPrefix = true
    } = _ref,
        props = _objectWithoutProperties(_ref, ["format", "withPrefix"]);

    const mask = format === 'mobile' ? '#### ####' : format === 'rm' ? '#### ####' : '## ### ####';
    const prefix = format === 'mobile' ? '+56 9' : format === 'rm' ? '+56 2' : '+56';
    return /*#__PURE__*/React.createElement(_reactNumberFormat2.default, _extends({
      type: "text",
      format: `${prefix} ${mask}`,
      allowEmptyFormatting: true
    }, props));
  }

  Phone.propTypes = {
    format: _propTypes2.default.oneOf(['mobile', 'rm', 'region']).isRequired,
    withPrefix: _propTypes2.default.bool
  };
});
//# sourceMappingURL=Phone.js.map