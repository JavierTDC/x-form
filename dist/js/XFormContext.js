"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.XFormContext = exports.defaultXFormConfig = void 0;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var defaultXFormConfig = {
  strings: {
    requiredField: "The field is required",
    optional: "Optional",
    invalidForm: "There are fields with errors",
    blankSelectField: "--- Select an option ---",
    invalidNumber: "The number is invalid",
    negativeNumber: "The number can't be negative",
    numberTooSmall: function numberTooSmall(minValue) {
      return "Must be at least ".concat(minValue);
    },
    numberTooBig: function numberTooBig(maxValue) {
      return "Must be at most ".concat(maxValue);
    },
    tooManyDecimals: "Too many decimals",
    shouldBeInteger: "Must be an integer",
    cantBeZero: "Can't be zero",
    invalidDate: "Invalid date",
    tooYoung: function tooYoung(minAge) {
      return "Must be older than ".concat(minAge);
    }
  }
};
exports.defaultXFormConfig = defaultXFormConfig;
var XFormContext = /*#__PURE__*/(0, _react.createContext)({});
exports.XFormContext = XFormContext;
//# sourceMappingURL=XFormContext.js.map