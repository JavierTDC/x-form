"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TextField = exports.TextFieldBuilder = void 0;

var _Field2 = require("./Field");

var _react = _interopRequireWildcard(require("react"));

var _utils = require("../utils");

var _Result = require("../Result");

var _FieldBuilder2 = require("./FieldBuilder");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TextFieldLogic = /*#__PURE__*/function (_FieldLogic) {
  _inherits(TextFieldLogic, _FieldLogic);

  var _super = _createSuper(TextFieldLogic);

  function TextFieldLogic(config) {
    var _this;

    _classCallCheck(this, TextFieldLogic);

    _this = _super.call(this, config);

    _defineProperty(_assertThisInitialized(_this), "blankInput", "");

    _this.config = config;
    return _this;
  }

  _createClass(TextFieldLogic, [{
    key: "isBlank",
    value: function isBlank(input) {
      return (0, _utils.isBlank)(input);
    }
  }, {
    key: "defaultParser",
    value: function defaultParser(input) {
      return (0, _Result.Valid)(input);
    }
  }, {
    key: "defaultFormatter",
    value: function defaultFormatter(value) {
      return value;
    }
  }]);

  return TextFieldLogic;
}(_Field2.FieldLogic);

var TextFieldBuilder = /*#__PURE__*/function (_FieldBuilder) {
  _inherits(TextFieldBuilder, _FieldBuilder);

  var _super2 = _createSuper(TextFieldBuilder);

  function TextFieldBuilder(config) {
    var _this2;

    _classCallCheck(this, TextFieldBuilder);

    _this2 = _super2.call(this, config);
    _this2.config = config;
    return _this2;
  }

  _createClass(TextFieldBuilder, [{
    key: "new",
    value: function _new(config) {
      return new TextFieldBuilder(config);
    }
  }, {
    key: "buildFieldLogic",
    value: function buildFieldLogic() {
      return new TextFieldLogic(this.config);
    }
  }, {
    key: "buildFromState",
    value: function buildFromState(name, state) {
      var config = this.config;
      var logic = this.buildFieldLogic();
      var ref = (0, _react.useRef)(null);
      var inputRef = (0, _react.useRef)(null);
      return new TextField(config, logic, state, ref, inputRef);
    }
  }, {
    key: "getInputProps",
    value: function getInputProps(name, state) {
      var config = this.config;

      function onChange(event) {
        var _config$inputProps, _config$inputProps$on;

        state.inputState.setValue(event.target.value);
        (_config$inputProps = config.inputProps) === null || _config$inputProps === void 0 ? void 0 : (_config$inputProps$on = _config$inputProps.onChange) === null || _config$inputProps$on === void 0 ? void 0 : _config$inputProps$on.call(_config$inputProps, event);
      }

      function onFocus(event) {
        var _config$inputProps2, _config$inputProps2$o;

        state.setIsFocused(true);
        (_config$inputProps2 = config.inputProps) === null || _config$inputProps2 === void 0 ? void 0 : (_config$inputProps2$o = _config$inputProps2.onFocus) === null || _config$inputProps2$o === void 0 ? void 0 : _config$inputProps2$o.call(_config$inputProps2, event);
      }

      function onBlur(event) {
        var _config$inputProps3, _config$inputProps3$o;

        state.setIsFocused(false);
        state.setHasBeenBlurred(true);
        (_config$inputProps3 = config.inputProps) === null || _config$inputProps3 === void 0 ? void 0 : (_config$inputProps3$o = _config$inputProps3.onBlur) === null || _config$inputProps3$o === void 0 ? void 0 : _config$inputProps3$o.call(_config$inputProps3, event);
      }

      return _objectSpread(_objectSpread({
        type: 'text',
        id: name,
        name: name
      }, this.config.inputProps), {}, {
        value: state.inputState.reactiveValue,
        onChange: onChange,
        onFocus: onFocus,
        onBlur: onBlur
      });
    }
  }]);

  return TextFieldBuilder;
}(_FieldBuilder2.FieldBuilder);

exports.TextFieldBuilder = TextFieldBuilder;

var TextField = /*#__PURE__*/function (_Field) {
  _inherits(TextField, _Field);

  var _super3 = _createSuper(TextField);

  function TextField(config, logic, state, ref, inputRef) {
    var _this3;

    _classCallCheck(this, TextField);

    _this3 = _super3.call(this, config, logic, state, ref);

    _defineProperty(_assertThisInitialized(_this3), "xType", "TextField");

    _this3.config = config;
    _this3.logic = logic;
    _this3.state = state;
    _this3.ref = ref;
    _this3.inputRef = inputRef;
    return _this3;
  }

  return TextField;
}(_Field2.Field);

exports.TextField = TextField;
//# sourceMappingURL=TextField.js.map