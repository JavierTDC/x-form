function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { useHtmlInput } from './useHtmlInput';
import { useXGlobalConfig } from '../useXGlobalConfig';
import _ from 'lodash';
import { usePreviousStateEffect } from '../usePreviousStateEffect'; // the type of the props of <option/>

export function useSelectField(config) {
  const xGlobalConfig = useXGlobalConfig();
  const blankOptions = config.defaultValue ? [] : [{
    value: "",
    label: config.blankOptionLabel ?? xGlobalConfig.strings.blankSelectField
  }];
  let field = useHtmlInput(_objectSpread(_objectSpread({}, config), {}, {
    xType: config.xType ?? "Select"
  }), {
    resetExtraState: () => {}
  }); // if the selected value disappears from the options, reset the value

  const currentOptions = config.options;
  usePreviousStateEffect(prevOptions => {
    const value = field.getLatestValue();

    const valueIn = options => options.some(_.matches({
      value
    }));

    if (valueIn(prevOptions) && !valueIn(currentOptions)) {
      field.resetValue();
    }
  }, currentOptions);
  return Object.assign(field, {
    config,
    options: [...blankOptions, ...config.options].map(option => _objectSpread(_objectSpread({}, option), {}, {
      props: {
        value: option.value
      }
    }))
  });
}
//# sourceMappingURL=useSelectField.js.map