import { StateRef, useStateRef } from './useStateRef';

export interface XState<T> extends StateRef<T> {
  value: T;
  initialValue: T;
  resetValue(): void;
}

export function useXState<T>(initialValue: T): XState<T> {
  const stateRef = useStateRef(initialValue);
  return {
    ...stateRef,
    value: stateRef.reactiveValue,
    initialValue,
    resetValue(): void {
      stateRef.setValue(initialValue);
    }
  };
}