import { Dispatch, SetStateAction } from "react";
export interface StateRef<T> {
    reactiveValue: T;
    setValue: Dispatch<SetStateAction<T>>;
    getLatestValue(): T;
}
export declare function useStateRef<T>(initialState: T): StateRef<T>;
