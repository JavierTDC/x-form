import * as React from 'react';
import { NumberFormatProps } from 'react-number-format';
export * from 'react-number-format';
export declare type AmountUFProps = NumberFormatProps & {
    withPrefix?: boolean;
};
export declare function AmountUF({ withPrefix, ...props }: AmountUFProps): React.ReactElement;
