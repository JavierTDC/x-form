(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global._);
    global.undefined = mod.exports;
  }
})(this, function (exports, _) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.keys(_).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
      enumerable: true,
      get: function () {
        return _[key];
      }
    });
  });
});
//# sourceMappingURL=index.js.map