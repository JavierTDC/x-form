"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useField = useField;

var _react = _interopRequireWildcard(require("react"));

var _FormTheme = require("./FormTheme");

var _useXGlobalConfig = require("./useXGlobalConfig");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function validateFieldConfig(config) {
  if (config.ignored != null && config.removed != null) {
    throw new Error("'ignored' and 'removed' are specified at the same time in the field" + " \"".concat(config.name, "\". Removed fields are already ignored by definition,") + " so this configuration is at best redundant and at worst wrong.");
  }

  if (config.hidden != null && config.removed != null) {
    throw new Error("'hidden' and 'removed' are specified at the same time in the field" + " \"".concat(config.name, "\". Removed fields are already hidden by definition,") + " so this configuration is at best redundant and at worst wrong.");
  }
}

function useField(config, internal) {
  var _config$xType, _config$requiredField, _config$shouldValidat, _config$shouldValidat2;

  validateFieldConfig(config);
  var xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
  var ref = (0, _react.useRef)(null); // submit

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      hasBeenSubmitted = _useState2[0],
      setHasBeenSubmitted = _useState2[1];

  var submitter = {
    hasBeenSubmitted: hasBeenSubmitted,
    onSubmit: function onSubmit() {
      setHasBeenSubmitted(true);
    }
  }; // reset
  // eslint-disable-next-line react-hooks/exhaustive-deps

  function reset() {
    setHasBeenSubmitted(false);
    internal.resetValue();
    internal.resetExtraState();
  }

  var partialField = _objectSpread(_objectSpread(_objectSpread({
    config: config,
    ref: ref
  }, internal), submitter), {}, {
    reset: reset,
    xType: (_config$xType = config.xType) !== null && _config$xType !== void 0 ? _config$xType : "Field"
  });
  /* VALIDATOR
   * --------- */


  var requiredFieldMessage = (_config$requiredField = config.requiredFieldMessage) !== null && _config$requiredField !== void 0 ? _config$requiredField : xGlobalConfig.strings.requiredField;
  var customValidator = config.validate; // minimize calls to validate function

  var validate = (0, _react.useCallback)(function (value) {
    var isBlank = internal.isBlank(value);

    if (config.optional && isBlank) {
      return 'OK';
    }

    if (isBlank) {
      return requiredFieldMessage;
    }

    if (customValidator) {
      return customValidator(value);
    }

    return "OK";
  }, [config, internal.isBlank]);
  var validationResult = (0, _react.useMemo)(function () {
    return validate(internal.value);
  }, [validate, internal.value]);
  var valid = validationResult === "OK";
  var shouldValidate = (_config$shouldValidat = (_config$shouldValidat2 = config.shouldValidate) === null || _config$shouldValidat2 === void 0 ? void 0 : _config$shouldValidat2.call(config, partialField)) !== null && _config$shouldValidat !== void 0 ? _config$shouldValidat : hasBeenSubmitted;
  var shouldShowError = shouldValidate && !valid;
  var validator = {
    validationResult: validationResult,
    valid: valid,
    shouldValidate: shouldValidate,
    shouldShowError: shouldShowError,
    validate: validate
  }; // Removed fields are hidden by definition

  var hidden = config.hidden || config.removed; // render

  function render() {
    var _config$render, _config$render2;

    // Hidden fields are not rendered by definition
    if (hidden) {
      return null;
    }

    return (_config$render = (_config$render2 = config.render) === null || _config$render2 === void 0 ? void 0 : _config$render2.call(config, field)) !== null && _config$render !== void 0 ? _config$render : /*#__PURE__*/_react.default.createElement(_FormTheme.FormThemeContext.Consumer, null, function (formTheme) {
      var _formTheme$render$fie, _formTheme$render$fie2, _formTheme$render;

      return (_formTheme$render$fie = (_formTheme$render$fie2 = (_formTheme$render = formTheme.render)[field.xType]) === null || _formTheme$render$fie2 === void 0 ? void 0 : _formTheme$render$fie2.call(_formTheme$render, field)) !== null && _formTheme$render$fie !== void 0 ? _formTheme$render$fie : /*#__PURE__*/_react.default.createElement(MissingThemeRender, {
        field: field
      });
    });
  } // those methods depend on the field instance to already exist


  var field = Object.assign(partialField, _objectSpread(_objectSpread({}, validator), {}, {
    render: render
  }));
  return field;
}

var MissingThemeRender = function MissingThemeRender(props) {
  return /*#__PURE__*/_react.default.createElement("p", null, "Missing render function for field of type ", props.field.xType, " in FormTheme implementation");
};
//# sourceMappingURL=useField.js.map