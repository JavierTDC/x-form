import * as React from 'react';
import { Rut } from './Rut';
export default {
  title: 'Rut',
  component: Rut
};

const Template = args => /*#__PURE__*/React.createElement(Rut, args);

export const RutWithOnValid = Template.bind({});
RutWithOnValid.args = {
  onValid: rut => alert('This RUT is valid: ' + rut)
};
export const RutWithInitialState = Template.bind({});
RutWithInitialState.args = {
  rut: '12.312.312-3'
};
export const RutWithDottedFormat = Template.bind({});
RutWithDottedFormat.args = {
  format: 'dashed'
};
export const RutWithOnChange = Template.bind({});
RutWithOnChange.args = {
  onRutChange: (rut, isValid) => {
    console.log('rut', rut, isValid);
  }
};
//# sourceMappingURL=Rut.stories.js.map