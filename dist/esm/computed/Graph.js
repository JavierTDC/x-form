import { List, Map } from "immutable";
export class Graph {
  constructor(adj) {
    this.adj = adj;
  }

  static empty() {
    return new Graph(Map());
  }

  addEdge(u, v) {
    this.adj = this.adj.update(u, List(), _ => _.push(v));
  }

  nodes() {
    return List(this.adj.keys());
  }

  neighbors(u) {
    return this.adj.get(u, List());
  }

  edges() {
    return this.nodes().flatMap(u => this.neighbors(u).map(v => [u, v]));
  }

  withReversedEdges() {
    let reversedGraph = Graph.empty();
    this.edges().forEach(([u, v]) => {
      reversedGraph.addEdge(v, u);
    });
    return reversedGraph;
  }

}
//# sourceMappingURL=Graph.js.map