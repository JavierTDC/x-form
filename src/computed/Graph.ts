import {List, Map} from "immutable";

export class Graph<Node> {
  private adj: Map<Node, List<Node>>;

  private constructor(adj: Map<Node, List<Node>>) {
    this.adj = adj;
  }

  static empty<Node>(): Graph<Node> {
    return new Graph(Map());
  }

  addEdge(u: Node, v: Node): void {
    this.adj = this.adj.update(u, List(),_ => _.push(v));
  }

  nodes(): List<Node> {
    return List(this.adj.keys());
  }

  neighbors(u: Node): List<Node> {
    return this.adj.get(u, List<Node>());
  }

  edges(): List<[Node, Node]> {
    return this.nodes().flatMap(u =>
        this.neighbors(u).map(v => [u, v]));
  }

  withReversedEdges(): Graph<Node> {
    let reversedGraph = Graph.empty<Node>();
    this.edges().forEach(([u, v]) => {
      reversedGraph.addEdge(v, u);
    });
    return reversedGraph;
  }
}