import { FieldConfig } from './FieldConfig';
import { Field, FieldLogic, FieldState } from './Field';
import { RefObject } from 'react';
import { InputProps } from '../field-types/useTextField';
import { FieldBuilder } from './FieldBuilder';
export interface TextFieldConfig extends FieldConfig<string, string> {
    readonly inputProps?: InputProps;
}
export declare class TextFieldBuilder extends FieldBuilder<string, string> {
    readonly config: TextFieldConfig;
    constructor(config: TextFieldConfig);
    protected new(config: TextFieldConfig): TextFieldBuilder;
    buildFieldLogic(): FieldLogic<string, string>;
    buildFromState(name: string, state: FieldState<string>): TextField;
    private getInputProps;
}
export declare class TextField extends Field<string, string> {
    readonly config: TextFieldConfig;
    readonly logic: FieldLogic<string, string>;
    readonly state: FieldState<string>;
    readonly ref: RefObject<HTMLDivElement>;
    readonly inputRef: RefObject<HTMLInputElement>;
    xType: string;
    constructor(config: TextFieldConfig, logic: FieldLogic<string, string>, state: FieldState<string>, ref: RefObject<HTMLDivElement>, inputRef: RefObject<HTMLInputElement>);
}
