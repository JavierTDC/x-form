(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "../Result", "react", "../FormTheme", "../useXState"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("../Result"), require("react"), require("../FormTheme"), require("../useXState"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.Result, global.react, global.FormTheme, global.useXState);
    global.undefined = mod.exports;
  }
})(this, function (exports, _Result, _react, _FormTheme, _useXState) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Field = exports.FieldLogic = exports.ON_SUBMIT = undefined;
  exports.useFieldState = useFieldState;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  /** Used internally by `Form` to trigger the "on submit" event on its fields. */
  const ON_SUBMIT = exports.ON_SUBMIT = Symbol('ON_SUBMIT');

  class FieldLogic {
    constructor(config) {
      _defineProperty(this, "parse", this.config.parse ?? this.defaultParser);

      _defineProperty(this, "validate", this.config.validate ?? this.defaultValidator);

      _defineProperty(this, "format", this.config.format ?? this.defaultFormatter);

      this.config = config;
    }

    defaultValidator(value) {
      return (0, _Result.Valid)(value);
    }

    get initialInput() {
      if (this.config.defaultInput != null) {
        return this.config.defaultInput;
      }

      if (this.config.defaultValue != null) {
        return this.format(this.config.defaultValue);
      }

      return this.blankInput;
    }

  }

  exports.FieldLogic = FieldLogic;
  const CACHED_RESULT = Symbol('CACHED_RESULT');

  class StatefulFieldMixin {
    constructor(logic, state) {
      this.logic = logic;
      this.state = state;
    }

    get input() {
      return this.state.inputState.reactiveValue;
    }

    setInput(action) {
      this.state.inputState.setValue(action);
    }

    setValue(newValue) {
      this.setInput(this.logic.format(newValue));
    }

    reset() {
      this.state.reset();
    }

    get initialInput() {
      return this.state.inputState.initialValue;
    }

    getLatestInput() {
      return this.state.inputState.getLatestValue();
    }

    get hasBeenSubmitted() {
      return this.state.hasBeenSubmitted;
    }

    get isFocused() {
      return this.state.isFocused;
    }

  }

  class Field extends StatefulFieldMixin {
    constructor(config, logic, state, ref) {
      super(logic, state);

      _defineProperty(this, CACHED_RESULT, null);

      this.config = config;
      this.logic = logic;
      this.state = state;
      this.ref = ref;
    }

    [ON_SUBMIT]() {
      this.state.setHasBeenSubmitted(true);
    }

    get result() {
      this[CACHED_RESULT] = this[CACHED_RESULT] ?? this.getResultSlow();
      return this[CACHED_RESULT];
    }

    getResultSlow() {
      return this.logic.parse(this.input).chain(this.logic.validate);
    }

    get isValid() {
      return this.result instanceof _Result.Valid;
    }

    unwrapValue() {
      return this.result.unwrap();
    }

    render() {
      if (this.config.render != null) {
        return this.config.render(this);
      }

      return /*#__PURE__*/_react2.default.createElement(_FormTheme.FormThemeContext.Consumer, null, formTheme => formTheme.render[this.xType]?.(this));
    }

  }

  exports.Field = Field;

  function useFieldState(initialInput) {
    const inputState = (0, _useXState.useXState)(initialInput); // const extraState = useXState(initialExtra);

    const [isFocused, setIsFocused] = (0, _react.useState)(false);
    const [hasBeenBlurred, setHasBeenBlurred] = (0, _react.useState)(false);
    const [hasBeenSubmitted, setHasBeenSubmitted] = (0, _react.useState)(false);

    function reset() {
      setHasBeenSubmitted(false);
      setHasBeenBlurred(false);
      inputState.resetValue(); // extraState.resetValue();
    }

    return {
      inputState,
      // extraState,
      isFocused,
      setIsFocused,
      hasBeenBlurred,
      setHasBeenBlurred,
      hasBeenSubmitted,
      setHasBeenSubmitted,
      reset
    };
  }
  /** The error message shown to the dev when a field doesn't have a render
   * function in the `FormTheme` implementation. */


  function MissingThemeRender({
    field
  }) {
    return /*#__PURE__*/_react2.default.createElement("p", null, "Missing render function for field of type ", field.xType, " in FormTheme implementation");
  }
});
//# sourceMappingURL=Field.js.map