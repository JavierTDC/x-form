"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRadioField = useRadioField;

var _useField = require("../useField");

var _useXState = require("../useXState");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function useRadioField(config) {
  var _config$defaultValue, _config$xType, _config$optionId;

  var state = (0, _useXState.useXState)((_config$defaultValue = config.defaultValue) !== null && _config$defaultValue !== void 0 ? _config$defaultValue : "");
  var field = (0, _useField.useField)(_objectSpread(_objectSpread({}, config), {}, {
    xType: (_config$xType = config.xType) !== null && _config$xType !== void 0 ? _config$xType : "Radio"
  }), _objectSpread(_objectSpread({}, state), {}, {
    resetExtraState: function resetExtraState() {},
    isBlank: function isBlank(value) {
      return value === "";
    }
  }));

  function onChange(event) {
    var _config$inputProps, _config$inputProps$on;

    state.setValue(event.target.value);
    (_config$inputProps = config.inputProps) === null || _config$inputProps === void 0 ? void 0 : (_config$inputProps$on = _config$inputProps.onChange) === null || _config$inputProps$on === void 0 ? void 0 : _config$inputProps$on.call(_config$inputProps, event);
  }

  var optionId = (_config$optionId = config.optionId) !== null && _config$optionId !== void 0 ? _config$optionId : defaultOptionId;

  function defaultOptionId(option, field) {
    return "".concat(field.config.name, "_").concat(option.value);
  }

  return Object.assign(field, {
    config: config,
    options: config.options.map(function (option) {
      var _config$inputProps$na, _config$inputProps2;

      return _objectSpread(_objectSpread({}, option), {}, {
        inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
          type: 'radio',

          get id() {
            return optionId(option, field);
          },

          name: (_config$inputProps$na = (_config$inputProps2 = config.inputProps) === null || _config$inputProps2 === void 0 ? void 0 : _config$inputProps2.name) !== null && _config$inputProps$na !== void 0 ? _config$inputProps$na : config.name,
          checked: state.value === option.value,
          value: option.value,
          onChange: onChange
        })
      });
    })
  });
}
//# sourceMappingURL=useRadioField.js.map