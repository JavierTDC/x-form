(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./useStateRef"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./useStateRef"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.useStateRef);
    global.undefined = mod.exports;
  }
})(this, function (exports, _useStateRef) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useXState = useXState;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function useXState(initialValue) {
    const stateRef = (0, _useStateRef.useStateRef)(initialValue);
    return _objectSpread(_objectSpread({}, stateRef), {}, {
      value: stateRef.reactiveValue,
      initialValue,

      resetValue() {
        stateRef.setValue(initialValue);
      }

    });
  }
});
//# sourceMappingURL=useXState.js.map