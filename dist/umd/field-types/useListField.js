(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "../useField", "immutable", "../useXState"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("../useField"), require("immutable"), require("../useXState"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.useField, global.immutable, global.useXState);
    global.undefined = mod.exports;
  }
})(this, function (exports, _useField, _immutable, _useXState) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useListField = useListField;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function useListField(config) {
    const state = (0, _useXState.useXState)(config.defaultValue ?? (0, _immutable.List)());

    function add(item) {
      state.setValue(list => list.push(item));
    }

    function update(index, newValues) {
      state.setValue(list => list.set(index, newValues));
    }

    function remove(index) {
      state.setValue(list => list.remove(index));
    }

    const requiredFieldMessage = config.emptyMessage // support for deprecated property
    ?? config.requiredFieldMessage ?? "Can't be empty";
    let field = (0, _useField.useField)(_objectSpread(_objectSpread({}, config), {}, {
      xType: config.xType ?? "List",
      requiredFieldMessage
    }), _objectSpread(_objectSpread({}, state), {}, {
      resetExtraState: () => {},
      isBlank: list => list.isEmpty()
    }));
    return Object.assign(field, {
      config,
      buttonProps: _objectSpread(_objectSpread({}, config.buttonProps), {}, {
        onClick(event) {
          add(config.form.values);
          config.form.reset();
          config.buttonProps?.onClick?.(event);
        },

        disabled: config.buttonProps?.disabled || !config.form.isValid
      }),
      add,
      update,
      remove
    });
  }
});
//# sourceMappingURL=useListField.js.map