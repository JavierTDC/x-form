import { RefObject } from 'react';
import { InputProps } from './useTextField';
import { Field, FieldConfig } from '../useField';
export interface HtmlInputFieldConfig extends FieldConfig<string> {
    readonly inputProps?: Partial<InputProps>;
}
export interface HtmlInputField extends Field<string> {
    readonly config: HtmlInputFieldConfig;
    readonly inputProps: InputProps;
    readonly inputRef: RefObject<HTMLInputElement>;
    readonly isFocused: boolean;
}
export declare function useHtmlInput(config: HtmlInputFieldConfig, internal: {
    resetExtraState(): void;
}): HtmlInputField;
