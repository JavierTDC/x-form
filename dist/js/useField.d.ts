import { ReactElement, RefObject } from "react";
import { XState } from "./useXState";
export interface FieldConfig<V> {
    readonly name: string;
    readonly label?: string | ReactElement;
    readonly optional?: boolean;
    readonly disabled?: boolean;
    readonly defaultValue?: V;
    validate?(value: V): string;
    shouldValidate?(field: PartialField<V>): boolean;
    render?(field: Field<V>): ReactElement;
    /** Allows you to specify a custom field type.
     *
     * If your field is instantiated with `useFooField`,
     * your xType should correspond to `"Foo"`.
     *
     * Currently used for FormTheme, but you can also use it to identify the type
     * of a field for custom logic. */
    readonly xType?: string;
    /** Allows you to override the error message when a required field has not
      * been filled. */
    readonly requiredFieldMessage?: string;
    /** If true, the field won't be submitted */
    readonly ignored?: boolean;
    /** If true, the field won't be rendered. */
    readonly hidden?: boolean;
    /** If true, the field will be ignored and hidden. */
    readonly removed?: boolean;
}
export interface PartialField<V> extends XState<V> {
    readonly xType: string;
    readonly config: FieldConfig<V>;
    readonly ref: RefObject<HTMLDivElement>;
    reset(): void;
    readonly hasBeenSubmitted: boolean;
    onSubmit(): void;
}
export interface Field<V> extends PartialField<V> {
    readonly validationResult: string;
    readonly valid: boolean;
    readonly shouldValidate: boolean;
    readonly shouldShowError: boolean;
    validate(value: V): string;
    render(): ReactElement;
}
export declare function useField<V>(config: FieldConfig<V>, internal: XState<V> & {
    resetExtraState(): void;
    /** Example: for a text field, a string value consisting of just
      * whitespace is considered blank and therefore fails validation
      * if the field is required. */
    isBlank(value: V): boolean;
}): Field<V>;
