"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.testBlankField = testBlankField;
exports.testInvalidField = testInvalidField;
exports.testValidField = testValidField;

var _reactHooks = require("@testing-library/react-hooks");

var _common = require("./common");

function testBlankField(kind, useField, expectedValid) {
  it(spell("should allow a ".concat(kind, " field to be blank")), function () {
    var field = (0, _common.getter)(useField);
    expect(field().valid).toBe(expectedValid);
  });
}

function testInvalidField(kind, useField, invalidValue) {
  it(spell("should not allow a ".concat(kind, " field to have an invalid value")), function () {
    var field = (0, _common.getter)(useField);
    (0, _reactHooks.act)(function () {
      return field().setValue(invalidValue);
    });
    expect(field().valid).toBe(false);
  });
}

function testValidField(kind, useField, validValue) {
  it(spell("should allow a ".concat(kind, " field to have a valid value")), function () {
    var field = (0, _common.getter)(useField);
    (0, _reactHooks.act)(function () {
      return field().setValue(validValue);
    });
    expect(field().valid).toBe(true);
  });
}

function spell(brokenEnglish) {
  return brokenEnglish.replace('a optional', 'an optional');
}
//# sourceMappingURL=optional.js.map