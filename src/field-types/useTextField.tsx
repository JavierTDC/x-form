import {DetailedHTMLProps, InputHTMLAttributes, ReactElement} from "react";
import {HtmlInputField, HtmlInputFieldConfig, useHtmlInput} from "./useHtmlInput";

// the type of the props of <input/>
export type InputProps = DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

export interface TextFieldConfig extends HtmlInputFieldConfig {
  // refine signature type
  render?(field: TextField): ReactElement;  // custom render function
}

export interface TextField extends HtmlInputField {
  readonly config: TextFieldConfig;
  readonly inputProps: InputProps;  // the props that are passed to <input/> on the render function
}

export function useTextField(config: TextFieldConfig): TextField {
  return useHtmlInput({
    ...config,
    xType: config.xType ?? "Text",
  }, {
    resetExtraState: () => {},
  });
}