import { Field, FieldConfig } from "../useField";
import { InputProps } from "./useTextField";
import { HtmlInputField } from "./useHtmlInput";
export interface NumberFieldConfig extends FieldConfig<number> {
    readonly inputProps: Partial<InputProps> & {
        min: number;
        max: number;
    };
    readonly decimals?: number;
    readonly nonZero?: boolean;
}
export interface NumberField extends Field<number> {
    config: NumberFieldConfig;
    inputProps: InputProps;
    isFocused: boolean;
    innerField: HtmlInputField;
}
export declare function useNumberField(config: NumberFieldConfig): NumberField;
