import React from "react";
import merge from "lodash/merge";
import { defaultXFormConfig, XFormConfig, XFormContext } from "./XFormContext";

export function useXGlobalConfig(): XFormConfig {
  const context = React.useContext(XFormContext);

  return merge({}, defaultXFormConfig, context);
}
