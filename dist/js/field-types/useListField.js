"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useListField = useListField;

var _useField = require("../useField");

var _immutable = require("immutable");

var _useXState = require("../useXState");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function useListField(config) {
  var _config$defaultValue, _ref, _config$emptyMessage, _config$xType, _config$buttonProps2;

  var state = (0, _useXState.useXState)((_config$defaultValue = config.defaultValue) !== null && _config$defaultValue !== void 0 ? _config$defaultValue : (0, _immutable.List)());

  function add(item) {
    state.setValue(function (list) {
      return list.push(item);
    });
  }

  function update(index, newValues) {
    state.setValue(function (list) {
      return list.set(index, newValues);
    });
  }

  function remove(index) {
    state.setValue(function (list) {
      return list.remove(index);
    });
  }

  var requiredFieldMessage = (_ref = (_config$emptyMessage = config.emptyMessage // support for deprecated property
  ) !== null && _config$emptyMessage !== void 0 ? _config$emptyMessage : config.requiredFieldMessage) !== null && _ref !== void 0 ? _ref : "Can't be empty";
  var field = (0, _useField.useField)(_objectSpread(_objectSpread({}, config), {}, {
    xType: (_config$xType = config.xType) !== null && _config$xType !== void 0 ? _config$xType : "List",
    requiredFieldMessage: requiredFieldMessage
  }), _objectSpread(_objectSpread({}, state), {}, {
    resetExtraState: function resetExtraState() {},
    isBlank: function isBlank(list) {
      return list.isEmpty();
    }
  }));
  return Object.assign(field, {
    config: config,
    buttonProps: _objectSpread(_objectSpread({}, config.buttonProps), {}, {
      onClick: function onClick(event) {
        var _config$buttonProps, _config$buttonProps$o;

        add(config.form.values);
        config.form.reset();
        (_config$buttonProps = config.buttonProps) === null || _config$buttonProps === void 0 ? void 0 : (_config$buttonProps$o = _config$buttonProps.onClick) === null || _config$buttonProps$o === void 0 ? void 0 : _config$buttonProps$o.call(_config$buttonProps, event);
      },
      disabled: ((_config$buttonProps2 = config.buttonProps) === null || _config$buttonProps2 === void 0 ? void 0 : _config$buttonProps2.disabled) || !config.form.isValid
    }),
    add: add,
    update: update,
    remove: remove
  });
}
//# sourceMappingURL=useListField.js.map