(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["../field-types/useNumberField", "../test-utils/optional"], factory);
  } else if (typeof exports !== "undefined") {
    factory(require("../field-types/useNumberField"), require("../test-utils/optional"));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.useNumberField, global.optional);
    global.undefined = mod.exports;
  }
})(this, function (_useNumberField, _optional) {
  "use strict";

  const name = 'test';
  const inputProps = {
    min: 0,
    max: 10
  };
  const invalidNumber = {
    toFixed: () => "42o"
  };
  const outOfRangeNumber = inputProps.max + 1;
  const validNumber = inputProps.min;
  const hooks = {
    optional: () => (0, _useNumberField.useNumberField)({
      name,
      optional: true,
      inputProps
    }),
    required: () => (0, _useNumberField.useNumberField)({
      name,
      inputProps
    })
  };
  describe('useNumberField', () => {
    (0, _optional.testBlankField)('optional', hooks.optional, true);
    (0, _optional.testBlankField)('required', hooks.required, false);
    (0, _optional.testInvalidField)('optional', hooks.optional, invalidNumber);
    (0, _optional.testInvalidField)('required', hooks.required, invalidNumber);
    (0, _optional.testInvalidField)('optional', hooks.optional, outOfRangeNumber);
    (0, _optional.testInvalidField)('required', hooks.required, outOfRangeNumber);
    (0, _optional.testValidField)('optional', hooks.optional, validNumber);
    (0, _optional.testValidField)('required', hooks.required, validNumber);
  });
});
//# sourceMappingURL=useNumberField.js.map