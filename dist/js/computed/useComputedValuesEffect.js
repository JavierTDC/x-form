"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useComputedValuesEffect = useComputedValuesEffect;

var _react = _interopRequireWildcard(require("react"));

var _immutable = require("immutable");

var _Graph = require("./Graph");

var _dfs = require("./dfs");

var _Functor = require("./Functor");

var _utils = require("../utils");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function findEffect(effects, field) {
  var effect = effects.find(function (effect) {
    return effect.computedField === field;
  });
  if (!effect) throw new Error("Couldn't find effect for field named \"".concat(field.config.name, "\""));
  return effect;
}

function useComputedValuesEffect(form, effects) {
  var reversedGraph = makeGraph(effects).withReversedEdges();
  var sourceNodes = reversedGraph.nodes();
  sourceNodes.forEach(function (sourceNode) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    (0, _react.useEffect)(function () {
      if (!sourceNode.field.isFocused && !(0, _utils.isBlank)(sourceNode.field.innerField.value)) {
        (0, _dfs.dfs)({
          graph: reversedGraph,
          sourceNode: sourceNode,
          callback: function callback(currentNode) {
            if (!currentNode.equals(sourceNode)) {
              var fieldToUpdate = currentNode.field;
              var effect = findEffect(effects, fieldToUpdate);
              var fn = effect.formula;
              var args = effect.requiredFields.map(_Functor.asFunctor);

              var computed = _Functor.Functor.map(fn).apply(void 0, _toConsumableArray(args));

              if (computed instanceof _Functor.Valid) {
                fieldToUpdate.setValue(computed.value);
              } else if (computed instanceof _Functor.Invalid) {
                fieldToUpdate.resetValue();
              }
            }
          }
        });
      } // eslint-disable-next-line

    }, [sourceNode.field.isFocused]);
  });
}

var FieldNode = /*#__PURE__*/function () {
  function FieldNode(field) {
    _classCallCheck(this, FieldNode);

    this.field = field;
  }

  _createClass(FieldNode, [{
    key: "equals",
    value: function equals(other) {
      if (!(other instanceof FieldNode)) return false;
      return this.field.config.name === other.field.config.name;
    }
  }, {
    key: "hashCode",
    value: function hashCode() {
      return (0, _immutable.hash)(this.field.config.name);
    }
  }]);

  return FieldNode;
}();

function makeGraph(effects) {
  var graph = _Graph.Graph.empty();

  effects.forEach(function (effect) {
    var u = new FieldNode(effect.computedField);
    effect.requiredFields.forEach(function (requiredField) {
      var v = new FieldNode(requiredField);
      graph.addEdge(u, v);
    });
  });
  return graph;
}
//# sourceMappingURL=useComputedValuesEffect.js.map