import _ from 'lodash';
import { FieldConfig } from './FieldConfig';
import { Field, FieldLogic, FieldState, useFieldState } from './Field';
import { Result, Valid } from '../Result';
import { RecursivePartial } from '../XFormContext';
import { ReactElement } from 'react';

export abstract class FieldBuilder<S, T> {
  protected constructor(readonly config: FieldConfig<S, T>) {
    this.validateConfig(config);
  }

  protected abstract new(config: FieldConfig<S, T>): FieldBuilder<S, T>;

  abstract buildFromState(name: string, state: FieldState<S>): Field<S, T>;

  validateConfig<S, T>(config: FieldConfig<S, T>): void {
    if (config.defaultInput != null && config.defaultValue != null) {
      throw new Error("defaultInput and defaultValue can't be both provided at the same time");
    }
  }

  with(config: RecursivePartial<FieldConfig<S, T>>): FieldBuilder<S, T> {
    return this.new( _.merge({}, this.config, config));
  }

  abstract buildFieldLogic(): FieldLogic<S, T>;

  build(name: string): Field<S, T> {
    const logic = this.buildFieldLogic();
    const initialInput = logic.initialInput;
    const state = useFieldState(initialInput);
    return this.buildFromState(name, state);
  }

  private defaultValidator(value: T): Result<T> {
    return Valid(value);
  }
}

export type Formatter<S, T> = (value: T) => S;

export type Parser<S, T> = (input: S) => Result<T>;

export type Validator<T> = (value: T) => Result<T>;