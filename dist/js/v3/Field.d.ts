import { Result } from '../Result';
import { FieldConfig } from './FieldConfig';
import { Dispatch, ReactElement, RefObject, SetStateAction } from 'react';
import { XState } from '../useXState';
/** Used internally by `Form` to trigger the "on submit" event on its fields. */
export declare const ON_SUBMIT: unique symbol;
export interface IField<S, T> extends StatefulField<S, T> {
    /** The `config` object that you used to build this field. */
    readonly config: FieldConfig<S, T>;
    /** Ref to container div. Used to scroll the field into view when submitting
     * with invalid values. */
    readonly ref: RefObject<HTMLDivElement>;
    /** Event handler internally triggered by the `Form` containing this field. */
    [ON_SUBMIT](): void;
    /** The result after parsing and validation. This can come in three shapes:
     * - `Valid(value)`
     * - `Invalid(message)`
     * - `Blank()` */
    result: Result<T>;
    /** True if the field has a `Valid` result, i.e.
     * the input value passes all validation rules and therefore the field has a
     * valid value that can be submitted with the form. */
    isValid: boolean;
    /** Gets the field value assuming the field has a valid value. If the field
     * is blank or invalid, this will throw. */
    unwrapValue(): T;
    /** Render this field with the current theme. */
    render(): ReactElement;
    readonly xType: string;
}
export interface IFieldLogic<S, T> {
    /** Allows to see what would happen if the field parser were used with the
     * specified input. */
    parse(input: S): Result<T>;
    /** Allows to see what would happen if the field validator were used with the
     * specified value. */
    validate(value: T): Result<T>;
    /** Allows to see what would happen if the field formatter were used with the
     * specified value. */
    format(value: T): S;
}
export declare abstract class FieldLogic<S, T> implements IFieldLogic<S, T> {
    readonly config: FieldConfig<S, T>;
    protected constructor(config: FieldConfig<S, T>);
    abstract readonly blankInput: S;
    /** Example: for a text field, a string input value consisting of just
     * whitespace is considered blank and therefore fails validation
     * if the field is required. */
    abstract isBlank(input: S): boolean;
    /** The inverse of the `parse` function. Used when setting the field input
     * from a valid value. */
    abstract defaultFormatter(value: T): S;
    abstract defaultParser(input: S): Result<T>;
    defaultValidator(value: T): Result<T>;
    parse: (input: S) => Result<T>;
    validate: (value: T) => Result<T>;
    format: (value: T) => S;
    get initialInput(): S;
}
declare const CACHED_RESULT: unique symbol;
interface StatefulField<S, T> {
    /** Gets the input value of the field, a value that generally correspond to
     * the string value typed by the user in an `<input>` tag. */
    readonly input: S;
    /** Set the field input value that generally corresponds to a string typed
     * by the user in an `<input>` tag.
     *
     * For example, for a `NumberField` you can
     * do `field.setInput("42")` or even leave the field with an invalid value
     * with `field.setInput("hello")`.
     *
     * This also accepts passing a function instead of a new value, in the same
     * way as can be done on `React.useState`.
     * @see Field#setValue */
    readonly setInput: Dispatch<SetStateAction<S>>;
    /** Set the field input value to something that after parsing is equal to the
     * specified value. For example, for a `NumberField` you can do
     * `field.setValue(42)` directly.
     * @see Field#setInput */
    setValue(newValue: T): void;
    /** Resets the field to its default value (generally blank) and all extra
     * state associated to the field (e.g. whether the field has been blurred). */
    reset(): void;
    /** The initial input value of this field, generally an empty string.
     * @see Field#input */
    readonly initialInput: S;
    /** Gets the latest input value set using `setInput`, without needing to wait
     * for React to propagate the state change to the rendering components. */
    getLatestInput(): S;
    /** True if the field is part of a form that has been submitted. */
    readonly hasBeenSubmitted: boolean;
    readonly isFocused: boolean;
}
declare class StatefulFieldMixin<S, T> implements StatefulField<S, T> {
    readonly logic: FieldLogic<S, T>;
    readonly state: FieldState<S>;
    constructor(logic: FieldLogic<S, T>, state: FieldState<S>);
    get input(): S;
    setInput(action: SetStateAction<S>): void;
    setValue(newValue: T): void;
    reset(): void;
    get initialInput(): S;
    getLatestInput(): S;
    get hasBeenSubmitted(): boolean;
    get isFocused(): boolean;
}
export declare abstract class Field<S, T> extends StatefulFieldMixin<S, T> implements IField<S, T> {
    readonly config: FieldConfig<S, T>;
    readonly logic: FieldLogic<S, T>;
    readonly state: FieldState<S>;
    readonly ref: RefObject<HTMLDivElement>;
    protected constructor(config: FieldConfig<S, T>, logic: FieldLogic<S, T>, state: FieldState<S>, ref: RefObject<HTMLDivElement>);
    [ON_SUBMIT](): void;
    private [CACHED_RESULT];
    get result(): Result<T>;
    private getResultSlow;
    get isValid(): boolean;
    unwrapValue(): T;
    abstract readonly xType: string;
    render(): ReactElement;
}
export interface FieldState<S> {
    inputState: XState<S>;
    isFocused: boolean;
    setIsFocused: Dispatch<SetStateAction<boolean>>;
    hasBeenBlurred: boolean;
    setHasBeenBlurred: Dispatch<SetStateAction<boolean>>;
    hasBeenSubmitted: boolean;
    setHasBeenSubmitted: Dispatch<SetStateAction<boolean>>;
    reset(): void;
}
export declare function useFieldState<S, X>(initialInput: S): FieldState<S>;
export {};
