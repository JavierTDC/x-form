(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "immutable", "./Graph", "./dfs", "./Functor", "../utils"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("immutable"), require("./Graph"), require("./dfs"), require("./Functor"), require("../utils"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.immutable, global.Graph, global.dfs, global.Functor, global.utils);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _immutable, _Graph, _dfs, _Functor, _utils) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useComputedValuesEffect = useComputedValuesEffect;

  function findEffect(effects, field) {
    const effect = effects.find(effect => effect.computedField === field);
    if (!effect) throw new Error(`Couldn't find effect for field named "${field.config.name}"`);
    return effect;
  }

  function useComputedValuesEffect(form, effects) {
    const reversedGraph = makeGraph(effects).withReversedEdges();
    const sourceNodes = reversedGraph.nodes();
    sourceNodes.forEach(sourceNode => {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      (0, _react.useEffect)(() => {
        if (!sourceNode.field.isFocused && !(0, _utils.isBlank)(sourceNode.field.innerField.value)) {
          (0, _dfs.dfs)({
            graph: reversedGraph,
            sourceNode,

            callback(currentNode) {
              if (!currentNode.equals(sourceNode)) {
                const fieldToUpdate = currentNode.field;
                const effect = findEffect(effects, fieldToUpdate);
                const fn = effect.formula;
                const args = effect.requiredFields.map(_Functor.asFunctor);

                const computed = _Functor.Functor.map(fn)(...args);

                if (computed instanceof _Functor.Valid) {
                  fieldToUpdate.setValue(computed.value);
                } else if (computed instanceof _Functor.Invalid) {
                  fieldToUpdate.resetValue();
                }
              }
            }

          });
        } // eslint-disable-next-line

      }, [sourceNode.field.isFocused]);
    });
  }

  class FieldNode {
    constructor(field) {
      this.field = field;
    }

    equals(other) {
      if (!(other instanceof FieldNode)) return false;
      return this.field.config.name === other.field.config.name;
    }

    hashCode() {
      return (0, _immutable.hash)(this.field.config.name);
    }

  }

  function makeGraph(effects) {
    let graph = _Graph.Graph.empty();

    effects.forEach(effect => {
      const u = new FieldNode(effect.computedField);
      effect.requiredFields.forEach(requiredField => {
        const v = new FieldNode(requiredField);
        graph.addEdge(u, v);
      });
    });
    return graph;
  }
});
//# sourceMappingURL=useComputedValuesEffect.js.map