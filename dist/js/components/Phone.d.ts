import * as React from 'react';
import { NumberFormatProps } from 'react-number-format';
export declare type Format = 'mobile' | 'rm' | 'region';
export declare type PhoneProps = NumberFormatProps & {
    format: Format;
    withPrefix?: boolean;
};
export declare function Phone({ format, withPrefix, ...props }: PhoneProps): React.ReactElement;
