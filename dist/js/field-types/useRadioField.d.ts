import { EnumFieldConfig, EnumOption } from './useEnumField';
import { Field } from '../useField';
import { InputProps } from './useTextField';
/** Configuration object that is passed to `useRadioField`. */
export interface RadioFieldConfig extends EnumFieldConfig {
    /** Allows you to override the props for the <input> of every option. */
    readonly inputProps?: InputProps;
    /** Allows you to specify what the id should be for every option.
      *
      * Those ids are used in the HTML markup to attach the labels to their
      * corresponding radio buttons, and every id should be unique. Then when you
      * click a label the corresponding radio button is selected.
      *
      * By default a `{name}_{value}` format is used, where `name` is the name of
      * the field and `value` is the value of that specific option. */
    optionId?(option: EnumOption, field: RadioField): string;
}
/** Field that renders to a group of `<input type="radio">`. */
export interface RadioField extends Field<string> {
    /** The configuration object you passed to `useRadioField`
      * to create this instance. */
    readonly config: RadioFieldConfig;
    /** An array of the available options corresponding each to a different
      * `<input type="radio">`.
      *
      * The `inputProps` property of each option
      * contains the props that should be passed to their corresponding
      * `<input>` tag. */
    readonly options: Array<EnumOption & {
        inputProps: InputProps;
    }>;
}
export declare function useRadioField(config: RadioFieldConfig): RadioField;
