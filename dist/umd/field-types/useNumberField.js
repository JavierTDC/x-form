(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "../useXGlobalConfig", "./useHtmlInput"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("../useXGlobalConfig"), require("./useHtmlInput"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.useXGlobalConfig, global.useHtmlInput);
    global.undefined = mod.exports;
  }
})(this, function (exports, _useXGlobalConfig, _useHtmlInput) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useNumberField = useNumberField;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function useNumberField(config) {
    const xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
    const innerField = (0, _useHtmlInput.useHtmlInput)(_objectSpread(_objectSpread({}, config), {}, {
      xType: config.xType ?? "Number",
      defaultValue: config.defaultValue?.toString(),
      shouldValidate: config.shouldValidate,
      render: config.render,
      inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
        // type: "number",
        step: config.inputProps.step ?? 10 ** -(config.decimals ?? 0)
      }),

      validate(value) {
        value = value.trim();
        const match = /^([0-9]|-[1-9])[0-9]*(\.([0-9]*))?$/.exec(value);

        if (!match) {
          return xGlobalConfig.strings.invalidNumber;
        }

        const decimalPart = match[3] ?? "";

        if (decimalPart && !config.decimals) {
          return xGlobalConfig.strings.shouldBeInteger;
        }

        if (decimalPart.length > (config.decimals ?? 0)) {
          return xGlobalConfig.strings.tooManyDecimals;
        }

        const x = Number(value);

        if (x < 0 && config.inputProps.min >= 0) {
          return xGlobalConfig.strings.negativeNumber;
        }

        if (x === 0 && config.nonZero) {
          return xGlobalConfig.strings.cantBeZero;
        }

        if (x < config.inputProps.min) {
          return xGlobalConfig.strings.numberTooSmall(config.inputProps.min);
        }

        if (x > config.inputProps.max) {
          return xGlobalConfig.strings.numberTooBig(config.inputProps.max);
        }

        return config.validate?.(x) ?? "OK";
      }

    }), {
      resetExtraState: () => {}
    });
    return _objectSpread(_objectSpread({}, innerField), {}, {
      config,
      innerField,
      initialValue: Number(innerField.initialValue),
      value: Number(innerField.value),
      reactiveValue: Number(innerField.reactiveValue),

      setValue(action) {
        if (typeof action === "function") {
          innerField.setValue(prev => action(Number(prev)).toFixed(config.decimals));
        } else {
          innerField.setValue(action.toFixed(config.decimals));
        }
      },

      getLatestValue() {
        return Number(innerField.getLatestValue());
      },

      validate(value) {
        return innerField.validate(value.toString());
      }

    });
  }
});
//# sourceMappingURL=useNumberField.js.map