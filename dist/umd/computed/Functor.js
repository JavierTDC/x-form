(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "lodash", "../utils"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("lodash"), require("../utils"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.lodash, global.utils);
    global.undefined = mod.exports;
  }
})(this, function (exports, _lodash, _utils) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Valid = exports.Invalid = exports.Blank = exports.Functor = undefined;
  exports.asFunctor = asFunctor;

  var _lodash2 = _interopRequireDefault(_lodash);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  class Functor {
    static map(fn) {
      return (...args) => {
        if (args.every(x => x instanceof Valid)) return new Valid(fn(..._lodash2.default.map(args, "value")));
        if (args.some(x => x instanceof Invalid)) return invalid;
        return blank;
      };
    }

  }

  exports.Functor = Functor;

  class Blank extends Functor {
    constructor(...args) {
      super(...args);

      _defineProperty(this, "type", "Blank");
    }

  }

  exports.Blank = Blank;

  class Invalid extends Functor {
    constructor(...args) {
      super(...args);

      _defineProperty(this, "type", "Invalid");
    }

  }

  exports.Invalid = Invalid;

  class Valid extends Functor {
    constructor(value) {
      super();

      _defineProperty(this, "type", "Valid");

      this.value = value;
    }

  }

  exports.Valid = Valid;
  const blank = new Blank();
  const invalid = new Invalid();

  function asFunctor(field) {
    const numberValue = field.getLatestValue();
    const stringValue = field.innerField.getLatestValue();
    const validate = field.innerField.validate;
    if (validate(stringValue) === "OK") return new Valid(numberValue);
    if ((0, _utils.isBlank)(stringValue)) return blank;
    return invalid;
  }
});
//# sourceMappingURL=Functor.js.map