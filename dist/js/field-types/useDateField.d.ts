import { Field, FieldConfig } from "../useField";
import { LocalDate } from "js-joda";
import { InputProps } from "./useTextField";
export interface DateFieldConfig extends FieldConfig<LocalDate> {
    readonly inputProps?: Partial<InputProps>;
    readonly minAge?: number;
}
export interface DateField extends Field<LocalDate> {
    config: DateFieldConfig;
    inputProps: InputProps;
}
export declare function useDateField(config: DateFieldConfig): DateField;
