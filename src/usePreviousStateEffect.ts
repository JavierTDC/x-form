import { useEffect, useRef } from 'react';

export function usePreviousStateEffect<T>(
    effect: PreviousStateEffectCallback<T>,
    state: T
): void {
  const prevStateRef = useRef(state);

  useEffect(() => {
    const destructor = effect(prevStateRef.current);
    prevStateRef.current = state;
    return destructor;
  }, [state]);
}

type PreviousStateEffectCallback<T> =
    (prevState: T) => (void | (() => void | undefined));