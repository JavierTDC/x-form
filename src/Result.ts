import _ from 'lodash';
import Immutable from 'immutable';

/***
 *    ██████╗ ███████╗███████╗██╗   ██╗██╗  ████████╗
 *    ██╔══██╗██╔════╝██╔════╝██║   ██║██║  ╚══██╔══╝
 *    ██████╔╝█████╗  ███████╗██║   ██║██║     ██║
 *    ██╔══██╗██╔══╝  ╚════██║██║   ██║██║     ██║
 *    ██║  ██║███████╗███████║╚██████╔╝███████╗██║
 *    ╚═╝  ╚═╝╚══════╝╚══════╝ ╚═════╝ ╚══════╝╚═╝
 *
 */

/** The return type for `parse` and `validate` functions.
 *
 * `Result`s are chainable functors that can store the following results after
 * a field parsing and/or validation:
 * - a `Valid(value)`
 * - an `Invalid(message)` where `message` is the error message shown to the
 * user.
 * - a `Blank()` for fields that have not been filled. */
export abstract class Result<T> {
  /** Use this instead of `===` for equality by value. */
  abstract equals(other: any): boolean;

  /** Applies the `fn` function to the value of a `Valid` result, returning
   * another `Valid` result with the transformed value.
   *
   * If applied to a non-`Valid` result it returns that same result unmodified.
   *
   * Examples:
   * - `Valid(42).map(x => x + 1)  // Valid(43)`
   * - `Invalid("Oops").map(x => x + 1)  // Invalid("Oops")`
   * - `Blank().map(x => x + 1)  // Blank()`
   * */
  abstract map<U>(fn: (x: T) => U): Result<U>;

  /** Applies the `fn` function to the value of a `Valid` result, returning the
   * same result that `fn` returned.
   *
   * If applied to a non-`Valid` result it returns that same result unmodified.
   *
   * Examples:
   * - `Valid(42).chain(x => Invalid(x.toString()))  // Invalid("42")`
   * - `Invalid("Oops").chain(x => Invalid(x.toString())) // Invalid("Oops")`
   * - `Blank().chain(x => Invalid(x.toString())) // Blank()`
   * */
  abstract chain<U>(fn: (x: T) => Result<U>): Result<U>;

  /** Syntax sugar that is similar to a `match` statement in
   * a functional language, where the resulting expression depends on the type
   * of the result that `match` is applied to.
   *
   * Example:
   * ```
   * const description = result.match({
   *   valid: (value) => `It is a valid ${value}`,
   *   invalid: (message) => `Invalid with message ${message}`,
   *   blank: () => `A blank`,
   * });
   * ``` */
  abstract match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B;

  /** Intuitively, it takes a function that doesn't work with `Result`s and
   * returns a new function that works with `Result` parameters and return type.
   *
   *
   * Example:
   * ```
   * const originalFn = (a, b, c) => a + b + c;
   * const newFn = Result.fMap(originalFn);
   * newFn(Valid(100), Valid(20), Valid(3))  // Valid(123)
   * newFn(Valid(100), Invalid("Oops"), Invalid("Boom"))  // Invalid("")
   * newFn(Valid(100), Invalid("Oops"), Blank())  // Invalid("")
   * newFn(Valid(100), Blank(), Blank())  // Blank()
   * ```
   *
   * Specifically, if `fn` is a function that receive N parameters of type `X`
   * and returns a value of type `Y`, `fMap` will return a new function that
   * receive N parameters of type `Result<X>` and returns a `Result<Y>`.
   *
   * Then the new function will work as follows:
   * - If all arguments are `Valid`, the result will be `Valid`.
   * - If there is any `Invalid` argument, the result will be `Invalid`.
   * - If there is any `Blank` argument (and no `Invalid`s), the result will be
   * `Blank`.
   * */
  static fMap<X, Y>(fn: VariadicFn<X, Y>): VariadicFn<Result<X>, Result<Y>> {
    return function (...xs: Array<Result<X>>): Result<Y> {
      if (xs.every(x => x instanceof Valid)) {
        const unwrappedValues = _.map(xs, 'value');
        return Valid(fn(...unwrappedValues));
      }

      if (xs.some(x => x instanceof Invalid)) {
        return Invalid("");  // this message will never be shown anyway
      }

      return Blank();
    };
  }

  /** Gets the value if the result is `Valid` or throws otherwise. */
  abstract unwrap(): T;
}

/** The type for a function that can receive any number of arguments of type `X`
 * and returns a value of type `Y`. */
type VariadicFn<X, Y> = (...args: Array<X>) => Y;

/** @see Result#match */
type Arms<T, V, I, B> = Readonly<{
  valid: (value: T) => V;
  invalid: (message: string) => I;
  blank: () => B;
}>;

/***
 *    ██╗   ██╗ █████╗ ██╗     ██╗██████╗
 *    ██║   ██║██╔══██╗██║     ██║██╔══██╗
 *    ██║   ██║███████║██║     ██║██║  ██║
 *    ╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
 *     ╚████╔╝ ██║  ██║███████╗██║██████╔╝
 *      ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
 *
 */

/** @see Result */
class $Valid<T> extends Result<T> {
  constructor(readonly value: T) {
    super();
  }

  equals(other: any): boolean {
    return other instanceof Valid && Immutable.is(this.value, other.value);
  }

  map<U>(fn: (x: T) => U): Valid<U> {
    return Valid(fn(this.value));
  }

  chain<U>(fn: (x: T) => Result<U>): Result<U> {
    return fn(this.value);
  }

  match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B {
    return arms.valid(this.value);
  }

  unwrap(): T {
    return this.value;
  }
}

/* The following code is dark magic to make `Valid` a proxy for the `$Valid`
 * class that can also be constructed without using the `new` keyword. */

/** @see Result */
export type Valid<T> = $Valid<T>;

/** @see Result */
export const Valid = new Proxy($Valid, {
  apply: (_, __, [value]) => new $Valid(value)
}) as typeof $Valid & { <T>(value: T): Valid<T> };

/***
 *    ██╗███╗   ██╗██╗   ██╗ █████╗ ██╗     ██╗██████╗
 *    ██║████╗  ██║██║   ██║██╔══██╗██║     ██║██╔══██╗
 *    ██║██╔██╗ ██║██║   ██║███████║██║     ██║██║  ██║
 *    ██║██║╚██╗██║╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║
 *    ██║██║ ╚████║ ╚████╔╝ ██║  ██║███████╗██║██████╔╝
 *    ╚═╝╚═╝  ╚═══╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝
 *
 */

/** @see Result */
class $Invalid<T> extends Result<T> {
  constructor(readonly message: string) {
    super();
  }

  equals(other: any): boolean {
    return other instanceof Invalid && this.message === other.message;
  }

  map<U>(fn: (x: T) => U): Invalid<U> {
    return this.cast();
  }

  chain<U>(fn: (x: T) => Result<U>): Result<U> {
    return this.cast();
  }

  match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B {
    return arms.invalid(this.message);
  }

  unwrap(): T {
    throw new Error("Attempted to unwrap an Invalid result");
  }

  /** Casts this `Invalid<T>` instance to `Invalid<U>`. */
  cast<U>(): Invalid<U> {
    // `Invalid` results always store strings so this is safe
    return this as unknown as Invalid<U>;
  }
}

/* The following code is dark magic to make `Invalid` a proxy for the `$Invalid`
 * class that can also be constructed without using the `new` keyword. */

/** @see Result */
export type Invalid<T> = $Invalid<T>;

/** @see Result */
export const Invalid = new Proxy($Invalid, {
  apply: (_, __, [message]) => new $Invalid(message)
}) as typeof $Invalid & { <T>(message: string): Invalid<T> };

/***
 *    ██████╗ ██╗      █████╗ ███╗   ██╗██╗  ██╗
 *    ██╔══██╗██║     ██╔══██╗████╗  ██║██║ ██╔╝
 *    ██████╔╝██║     ███████║██╔██╗ ██║█████╔╝
 *    ██╔══██╗██║     ██╔══██║██║╚██╗██║██╔═██╗
 *    ██████╔╝███████╗██║  ██║██║ ╚████║██║  ██╗
 *    ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝
 *
 */

class $Blank<T> extends Result<T> {
  equals(other: any): boolean {
    return other instanceof Blank;
  }

  map<U>(fn: (x: T) => U): Result<U> {
    return this.cast();
  }

  chain<U>(fn: (x: T) => Result<U>): Result<U> {
    return this.cast();
  }

  match<V, I, B>(arms: Arms<T, V, I, B>): V | I | B {
    return arms.blank();
  }

  unwrap(): T {
    throw new Error("Attempted to unwrap a Blank result");
  }

  /** Casts this `Blank<T>` instance to `Blank<U>`. */
  cast<U>(): Blank<U> {
    // `Blank` results always store strings so this is safe
    return this as unknown as Blank<U>;
  }
}

/** There is only one possible `Blank()` result so we can store an instance
 * for it and then always return that instance in the `Blank` constructor. */
const blankSingletonInstance = new $Blank<any>();

/* The following code is dark magic to make `Blank` a proxy for the `$Blank`
 * class that can also be constructed without using the `new` keyword. */

/** @see Result */
export type Blank<T> = $Blank<T>;

/** @see Result */
export const Blank = new Proxy($Blank, {
  apply: () => blankSingletonInstance  // equivalent to new $Blank()
}) as typeof $Blank & { <T>(): Blank<T> };