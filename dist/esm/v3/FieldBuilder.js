import _ from 'lodash';
import { useFieldState } from './Field';
import { Valid } from '../Result';
export class FieldBuilder {
  constructor(config) {
    this.config = config;
    this.validateConfig(config);
  }

  validateConfig(config) {
    if (config.defaultInput != null && config.defaultValue != null) {
      throw new Error("defaultInput and defaultValue can't be both provided at the same time");
    }
  }

  with(config) {
    return this.new(_.merge({}, this.config, config));
  }

  build(name) {
    const logic = this.buildFieldLogic();
    const initialInput = logic.initialInput;
    const state = useFieldState(initialInput);
    return this.buildFromState(name, state);
  }

  defaultValidator(value) {
    return Valid(value);
  }

}
//# sourceMappingURL=FieldBuilder.js.map