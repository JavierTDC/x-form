import { HtmlInputFieldConfig } from './useHtmlInput';
export declare type EnumOption = {
    value: string;
    label: string;
};
export interface EnumFieldConfig extends HtmlInputFieldConfig {
    options: EnumOption[];
}
