import React, { useRef, useState } from "react";
export function useStateRef(initialState) {
  const [reactiveValue, setReactiveValue] = useState(initialState);
  const valueRef = useRef(initialState);
  return {
    reactiveValue,

    setValue(action) {
      if (action instanceof Function) {
        valueRef.current = action(valueRef.current);
      } else {
        valueRef.current = action;
      }

      setReactiveValue(valueRef.current);
    },

    getLatestValue() {
      return valueRef.current;
    }

  };
}
//# sourceMappingURL=useStateRef.js.map