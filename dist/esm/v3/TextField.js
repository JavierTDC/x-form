function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { Field, FieldLogic } from './Field';
import React, { useRef } from 'react';
import { isBlank } from '../utils';
import { Valid } from '../Result';
import { FieldBuilder } from './FieldBuilder';

class TextFieldLogic extends FieldLogic {
  constructor(config) {
    super(config);

    _defineProperty(this, "blankInput", "");

    this.config = config;
  }

  isBlank(input) {
    return isBlank(input);
  }

  defaultParser(input) {
    return Valid(input);
  }

  defaultFormatter(value) {
    return value;
  }

}

export class TextFieldBuilder extends FieldBuilder {
  constructor(config) {
    super(config);
    this.config = config;
  }

  new(config) {
    return new TextFieldBuilder(config);
  }

  buildFieldLogic() {
    return new TextFieldLogic(this.config);
  }

  buildFromState(name, state) {
    const config = this.config;
    const logic = this.buildFieldLogic();
    const ref = useRef(null);
    const inputRef = useRef(null);
    return new TextField(config, logic, state, ref, inputRef);
  }

  getInputProps(name, state) {
    const config = this.config;

    function onChange(event) {
      state.inputState.setValue(event.target.value);
      config.inputProps?.onChange?.(event);
    }

    function onFocus(event) {
      state.setIsFocused(true);
      config.inputProps?.onFocus?.(event);
    }

    function onBlur(event) {
      state.setIsFocused(false);
      state.setHasBeenBlurred(true);
      config.inputProps?.onBlur?.(event);
    }

    return _objectSpread(_objectSpread({
      type: 'text',
      id: name,
      name
    }, this.config.inputProps), {}, {
      value: state.inputState.reactiveValue,
      onChange,
      onFocus,
      onBlur
    });
  }

}
export class TextField extends Field {
  constructor(config, logic, state, ref, inputRef) {
    super(config, logic, state, ref);

    _defineProperty(this, "xType", "TextField");

    this.config = config;
    this.logic = logic;
    this.state = state;
    this.ref = ref;
    this.inputRef = inputRef;
  }

}
//# sourceMappingURL=TextField.js.map