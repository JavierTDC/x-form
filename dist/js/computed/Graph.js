"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Graph = void 0;

var _immutable = require("immutable");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Graph = /*#__PURE__*/function () {
  function Graph(adj) {
    _classCallCheck(this, Graph);

    this.adj = adj;
  }

  _createClass(Graph, [{
    key: "addEdge",
    value: function addEdge(u, v) {
      this.adj = this.adj.update(u, (0, _immutable.List)(), function (_) {
        return _.push(v);
      });
    }
  }, {
    key: "nodes",
    value: function nodes() {
      return (0, _immutable.List)(this.adj.keys());
    }
  }, {
    key: "neighbors",
    value: function neighbors(u) {
      return this.adj.get(u, (0, _immutable.List)());
    }
  }, {
    key: "edges",
    value: function edges() {
      var _this = this;

      return this.nodes().flatMap(function (u) {
        return _this.neighbors(u).map(function (v) {
          return [u, v];
        });
      });
    }
  }, {
    key: "withReversedEdges",
    value: function withReversedEdges() {
      var reversedGraph = Graph.empty();
      this.edges().forEach(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            u = _ref2[0],
            v = _ref2[1];

        reversedGraph.addEdge(v, u);
      });
      return reversedGraph;
    }
  }], [{
    key: "empty",
    value: function empty() {
      return new Graph((0, _immutable.Map)());
    }
  }]);

  return Graph;
}();

exports.Graph = Graph;
//# sourceMappingURL=Graph.js.map