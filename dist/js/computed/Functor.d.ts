import { NumberField } from "../field-types/useNumberField";
export declare abstract class Functor<T> {
    abstract get type(): string;
    static map<X, Y>(fn: (...args: X[]) => Y): (...args: Functor<X>[]) => Functor<Y>;
}
export declare class Blank<T> extends Functor<T> {
    type: string;
}
export declare class Invalid<T> extends Functor<T> {
    type: string;
}
export declare class Valid<T> extends Functor<T> {
    type: string;
    readonly value: T;
    constructor(value: T);
}
export declare function asFunctor(field: NumberField): Functor<number>;
