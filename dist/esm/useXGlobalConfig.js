import React from "react";
import merge from "lodash/merge";
import { defaultXFormConfig, XFormContext } from "./XFormContext";
export function useXGlobalConfig() {
  const context = React.useContext(XFormContext);
  return merge({}, defaultXFormConfig, context);
}
//# sourceMappingURL=useXGlobalConfig.js.map