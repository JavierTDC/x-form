import { DetailedHTMLProps, InputHTMLAttributes, ReactElement } from "react";
import { HtmlInputField, HtmlInputFieldConfig } from "./useHtmlInput";
export declare type InputProps = DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;
export interface TextFieldConfig extends HtmlInputFieldConfig {
    render?(field: TextField): ReactElement;
}
export interface TextField extends HtmlInputField {
    readonly config: TextFieldConfig;
    readonly inputProps: InputProps;
}
export declare function useTextField(config: TextFieldConfig): TextField;
