"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useNumberField = useNumberField;

var _useXGlobalConfig = require("../useXGlobalConfig");

var _useHtmlInput = require("./useHtmlInput");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function useNumberField(config) {
  var _config$xType, _config$defaultValue, _config$inputProps$st, _config$decimals;

  var xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
  var innerField = (0, _useHtmlInput.useHtmlInput)(_objectSpread(_objectSpread({}, config), {}, {
    xType: (_config$xType = config.xType) !== null && _config$xType !== void 0 ? _config$xType : "Number",
    defaultValue: (_config$defaultValue = config.defaultValue) === null || _config$defaultValue === void 0 ? void 0 : _config$defaultValue.toString(),
    shouldValidate: config.shouldValidate,
    render: config.render,
    inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
      // type: "number",
      step: (_config$inputProps$st = config.inputProps.step) !== null && _config$inputProps$st !== void 0 ? _config$inputProps$st : Math.pow(10, -((_config$decimals = config.decimals) !== null && _config$decimals !== void 0 ? _config$decimals : 0))
    }),
    validate: function validate(value) {
      var _match$, _config$decimals2, _config$validate, _config$validate2;

      value = value.trim();
      var match = /^([0-9]|-[1-9])[0-9]*(\.([0-9]*))?$/.exec(value);

      if (!match) {
        return xGlobalConfig.strings.invalidNumber;
      }

      var decimalPart = (_match$ = match[3]) !== null && _match$ !== void 0 ? _match$ : "";

      if (decimalPart && !config.decimals) {
        return xGlobalConfig.strings.shouldBeInteger;
      }

      if (decimalPart.length > ((_config$decimals2 = config.decimals) !== null && _config$decimals2 !== void 0 ? _config$decimals2 : 0)) {
        return xGlobalConfig.strings.tooManyDecimals;
      }

      var x = Number(value);

      if (x < 0 && config.inputProps.min >= 0) {
        return xGlobalConfig.strings.negativeNumber;
      }

      if (x === 0 && config.nonZero) {
        return xGlobalConfig.strings.cantBeZero;
      }

      if (x < config.inputProps.min) {
        return xGlobalConfig.strings.numberTooSmall(config.inputProps.min);
      }

      if (x > config.inputProps.max) {
        return xGlobalConfig.strings.numberTooBig(config.inputProps.max);
      }

      return (_config$validate = (_config$validate2 = config.validate) === null || _config$validate2 === void 0 ? void 0 : _config$validate2.call(config, x)) !== null && _config$validate !== void 0 ? _config$validate : "OK";
    }
  }), {
    resetExtraState: function resetExtraState() {}
  });
  return _objectSpread(_objectSpread({}, innerField), {}, {
    config: config,
    innerField: innerField,
    initialValue: Number(innerField.initialValue),
    value: Number(innerField.value),
    reactiveValue: Number(innerField.reactiveValue),
    setValue: function setValue(action) {
      if (typeof action === "function") {
        innerField.setValue(function (prev) {
          return action(Number(prev)).toFixed(config.decimals);
        });
      } else {
        innerField.setValue(action.toFixed(config.decimals));
      }
    },
    getLatestValue: function getLatestValue() {
      return Number(innerField.getLatestValue());
    },
    validate: function validate(value) {
      return innerField.validate(value.toString());
    }
  });
}
//# sourceMappingURL=useNumberField.js.map