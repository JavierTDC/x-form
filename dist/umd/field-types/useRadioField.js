(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "../useField", "../useXState"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("../useField"), require("../useXState"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.useField, global.useXState);
    global.undefined = mod.exports;
  }
})(this, function (exports, _useField, _useXState) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useRadioField = useRadioField;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function useRadioField(config) {
    const state = (0, _useXState.useXState)(config.defaultValue ?? "");
    let field = (0, _useField.useField)(_objectSpread(_objectSpread({}, config), {}, {
      xType: config.xType ?? "Radio"
    }), _objectSpread(_objectSpread({}, state), {}, {
      resetExtraState: () => {},
      isBlank: value => value === ""
    }));

    function onChange(event) {
      state.setValue(event.target.value);
      config.inputProps?.onChange?.(event);
    }

    const optionId = config.optionId ?? defaultOptionId;

    function defaultOptionId(option, field) {
      return `${field.config.name}_${option.value}`;
    }

    return Object.assign(field, {
      config,
      options: config.options.map(option => _objectSpread(_objectSpread({}, option), {}, {
        inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
          type: 'radio',

          get id() {
            return optionId(option, field);
          },

          name: config.inputProps?.name ?? config.name,
          checked: state.value === option.value,
          value: option.value,
          onChange
        })
      }))
    });
  }
});
//# sourceMappingURL=useRadioField.js.map