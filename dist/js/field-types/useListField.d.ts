import { Field, FieldConfig } from "../useField";
import { List } from "immutable";
import { Form } from "../useForm";
import { ButtonHTMLAttributes, DetailedHTMLProps } from "react";
export declare type ButtonProps = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;
export interface ListFieldConfig<T> extends FieldConfig<List<T>> {
    readonly form: Form<T>;
    readonly buttonProps?: Partial<ButtonProps>;
    readonly emptyMessage?: string;
}
export interface ListField<T> extends Field<List<T>> {
    readonly config: ListFieldConfig<T>;
    readonly buttonProps: ButtonProps;
    add(item: T): void;
    update(index: number, newValues: T): void;
    remove(index: number): void;
}
export declare function useListField<T>(config: ListFieldConfig<T>): ListField<T>;
