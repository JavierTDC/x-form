(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "./FormTheme", "./useXGlobalConfig"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("./FormTheme"), require("./useXGlobalConfig"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.FormTheme, global.useXGlobalConfig);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _FormTheme, _useXGlobalConfig) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useField = useField;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function validateFieldConfig(config) {
    if (config.ignored != null && config.removed != null) {
      throw new Error(`'ignored' and 'removed' are specified at the same time in the field` + ` "${config.name}". Removed fields are already ignored by definition,` + ` so this configuration is at best redundant and at worst wrong.`);
    }

    if (config.hidden != null && config.removed != null) {
      throw new Error(`'hidden' and 'removed' are specified at the same time in the field` + ` "${config.name}". Removed fields are already hidden by definition,` + ` so this configuration is at best redundant and at worst wrong.`);
    }
  }

  function useField(config, internal) {
    validateFieldConfig(config);
    const xGlobalConfig = (0, _useXGlobalConfig.useXGlobalConfig)();
    const ref = (0, _react.useRef)(null); // submit

    const [hasBeenSubmitted, setHasBeenSubmitted] = (0, _react.useState)(false);
    const submitter = {
      hasBeenSubmitted,

      onSubmit() {
        setHasBeenSubmitted(true);
      }

    }; // reset
    // eslint-disable-next-line react-hooks/exhaustive-deps

    function reset() {
      setHasBeenSubmitted(false);
      internal.resetValue();
      internal.resetExtraState();
    }

    const partialField = _objectSpread(_objectSpread(_objectSpread({
      config,
      ref
    }, internal), submitter), {}, {
      reset,
      xType: config.xType ?? "Field"
    });
    /* VALIDATOR
     * --------- */


    const requiredFieldMessage = config.requiredFieldMessage ?? xGlobalConfig.strings.requiredField;
    const customValidator = config.validate; // minimize calls to validate function

    const validate = (0, _react.useCallback)(value => {
      const isBlank = internal.isBlank(value);

      if (config.optional && isBlank) {
        return 'OK';
      }

      if (isBlank) {
        return requiredFieldMessage;
      }

      if (customValidator) {
        return customValidator(value);
      }

      return "OK";
    }, [config, internal.isBlank]);
    const validationResult = (0, _react.useMemo)(() => {
      return validate(internal.value);
    }, [validate, internal.value]);
    const valid = validationResult === "OK";
    const shouldValidate = config.shouldValidate?.(partialField) ?? hasBeenSubmitted;
    const shouldShowError = shouldValidate && !valid;
    const validator = {
      validationResult,
      valid,
      shouldValidate,
      shouldShowError,
      validate
    }; // Removed fields are hidden by definition

    const hidden = config.hidden || config.removed; // render

    function render() {
      // Hidden fields are not rendered by definition
      if (hidden) {
        return null;
      }

      return config.render?.(field) ?? /*#__PURE__*/_react2.default.createElement(_FormTheme.FormThemeContext.Consumer, null, formTheme => formTheme.render[field.xType]?.(field) ?? /*#__PURE__*/_react2.default.createElement(MissingThemeRender, {
        field: field
      }));
    } // those methods depend on the field instance to already exist


    const field = Object.assign(partialField, _objectSpread(_objectSpread({}, validator), {}, {
      render
    }));
    return field;
  }

  const MissingThemeRender = props => {
    return /*#__PURE__*/_react2.default.createElement("p", null, "Missing render function for field of type ", props.field.xType, " in FormTheme implementation");
  };
});
//# sourceMappingURL=useField.js.map