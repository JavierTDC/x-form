(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "immutable"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("immutable"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.immutable);
    global.undefined = mod.exports;
  }
})(this, function (exports, _immutable) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Graph = undefined;

  class Graph {
    constructor(adj) {
      this.adj = adj;
    }

    static empty() {
      return new Graph((0, _immutable.Map)());
    }

    addEdge(u, v) {
      this.adj = this.adj.update(u, (0, _immutable.List)(), _ => _.push(v));
    }

    nodes() {
      return (0, _immutable.List)(this.adj.keys());
    }

    neighbors(u) {
      return this.adj.get(u, (0, _immutable.List)());
    }

    edges() {
      return this.nodes().flatMap(u => this.neighbors(u).map(v => [u, v]));
    }

    withReversedEdges() {
      let reversedGraph = Graph.empty();
      this.edges().forEach(([u, v]) => {
        reversedGraph.addEdge(v, u);
      });
      return reversedGraph;
    }

  }

  exports.Graph = Graph;
});
//# sourceMappingURL=Graph.js.map