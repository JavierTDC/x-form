(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "react", "../utils", "../useField", "../useXState"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("react"), require("../utils"), require("../useField"), require("../useXState"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.react, global.utils, global.useField, global.useXState);
    global.undefined = mod.exports;
  }
})(this, function (exports, _react, _utils, _useField, _useXState) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.useHtmlInput = useHtmlInput;

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function useHtmlInput(config, internal) {
    const inputRef = (0, _react.useRef)(null);
    const state = (0, _useXState.useXState)(config.defaultValue ?? "");
    const [isFocused, setIsFocused] = (0, _react.useState)(false);
    const [hasBeenBlurred, setHasBeenBlurred] = (0, _react.useState)(false);

    function shouldValidate(field) {
      return hasBeenBlurred || field.hasBeenSubmitted;
    }

    let field = (0, _useField.useField)(_objectSpread(_objectSpread({}, config), {}, {
      shouldValidate
    }), _objectSpread(_objectSpread({}, state), {}, {
      resetExtraState() {
        setHasBeenBlurred(false);
        internal.resetExtraState();
      },

      isBlank: _utils.isBlank
    }));
    return Object.assign(field, {
      inputProps: _objectSpread(_objectSpread({}, config.inputProps), {}, {
        id: config.inputProps?.id ?? config.name,
        name: config.inputProps?.name ?? config.name,
        value: state.value,

        onChange(event) {
          state.setValue(event.target.value);
          config.inputProps?.onChange?.(event);
        },

        onFocus(event) {
          setIsFocused(true);
          config.inputProps?.onFocus?.(event);
        },

        onBlur(event) {
          setIsFocused(false);
          setHasBeenBlurred(true);
          config.inputProps?.onBlur?.(event);
        }

      }),
      inputRef,
      isFocused,
      isBlank: (0, _utils.isBlank)(state.value)
    });
  }
});
//# sourceMappingURL=useHtmlInput.js.map