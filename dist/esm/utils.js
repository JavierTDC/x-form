import React, { useMemo } from "react";
export function isBlank(s) {
  return s.trim().length === 0;
}
export function useConstant(factory) {
  return useMemo(factory, []);
}
export function scrollToField(field) {
  if (field.ref.current != null) {
    field.ref.current.scrollIntoView({
      behavior: "smooth"
    });
  } else {
    // defensive behavior
    scrollToTop();
    console.error(`I tried to scroll to the field named ${field.config.name}, ` + `but it doesn't have a ref to scroll to`);
  }
}
export function scrollToTop() {
  window.scrollTo({
    top: 0,
    behavior: "smooth"
  });
}
//# sourceMappingURL=utils.js.map